﻿using Imow.WebService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imow.WebServer.Services
{
    public class DatabaseConnectionFactoryService : IDatabaseConnectionFactoryService
    {
        public DatabaseConnectionFactoryService()
        {
        }

        public IDatabaseService CreateConnection()
        {
            return new DatabaseService();
        }
    }
}
