﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Imow.WebService.Models;
using Imow.WebServer.Models;
using System;

namespace Imow.WebService.Services
{
    public interface IDatabaseService : IDisposable
    {
        Task<bool> CreateUser(UserModel user);
        Task<UserModel> CheckLogin(string email, string password);
        Task<bool> UpdateUser(UserModel user);
        Task<List<ApartmentModel>> GetApartments();
        Task<List<ApartmentModel>> GetApartmentsInRange(double lat1, double lat2, double lng1, double lng2, string clientId);
        Task<List<ApartmentModel>> GetApartmentsInRange(double lat1, double lat2, double lng1, double lng2);
        Task<List<UserModel>> GetAgents();
        Task<UserModel> GetClient(string clientId);
        Task<ApartmentModel> GetApartmentById(string apartmentId);
        Task<List<string>> GetNeighborhoodsByCity(string city, string state);
        Task<List<string>> GetPropertyTypes();
        Task<bool> AddRating(string agentId, string clientId, int rating, string observation = null);
        Task<bool> AddApartmentClick(string clientId, string apartmentId);
        Task<bool> CreateConnection(string userId, string version, string deviceId, string connectionId, int type);
        Task<List<LeadModel>> GetLeads();
        Task<bool> CreateLead(LeadModel lead);
        Task<bool> UpdateLead(LeadModel lead);
        Task<bool> ConfirmAttendance(string clientId, string apartmentId, string brokerId);
        Task<MatchModel> GetMatches(string clientId);
        Task<MatchModel> GetBrokerMatches(string brokerId, DateTime timestamp);
        Task<List<string>> GetApartmentPhotos(Guid id);
    }
}
