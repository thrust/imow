﻿using Imow.WebService.Services;

namespace Imow.WebServer.Services
{
    public interface IDatabaseConnectionFactoryService
    {
        IDatabaseService CreateConnection();
    }
}
