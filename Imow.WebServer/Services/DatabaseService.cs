﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Imow.WebService.Models;
using Imow.WebServer;
using Imow.WebServer.Models;
using System.Linq;

namespace Imow.WebService.Services
{
    public class DatabaseService : IDatabaseService
    {
        private NpgsqlConnection Connection => _lazyConnection.Value.Result;
        private readonly AsyncLazy<NpgsqlConnection> _lazyConnection;

        public DatabaseService()
        {
            _lazyConnection = new AsyncLazy<NpgsqlConnection>(async () =>
            {
                var databaseConnectionString = Startup.Configuration.GetSection("Database")["ConnectionString"];
                var connection = new NpgsqlConnection(databaseConnectionString);
                await connection.OpenAsync().ConfigureAwait(false);

                return connection;
            });
        }

        #region Connection
        public ConnectionState GetConnectionState()
        {
            return Connection.State;
        }

        public async Task<bool> ConnectAsync()
        {
            if (Connection == null)
            {
                return false;
            }

            await Connection.OpenAsync().ConfigureAwait(false);
            return true;
        }

        public void Disconnect()
        {
            try
            {
                Connection?.Close();
            }
            catch (Exception e)
            {
                var a = e;
            }
        }

        public void Dispose()
        {
            Disconnect();
        }

        protected void CloseDataReaderAndRollback(DbDataReader dataReader, NpgsqlTransaction transaction)
        {
            if (dataReader != null && !dataReader.IsClosed)
            {
                dataReader.Close();
            }
            if (transaction != null)
            {
                transaction.Rollback();
                transaction.Dispose();
                transaction = null;
            }
        }
        #endregion

        #region User
        public async Task<bool> CreateUser(UserModel user)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.create_user ( :id, :firstname, :lastname, :email, :password, :phone, :is_agent)";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("firstname", NpgsqlDbType.Text);
                    command.Parameters.Add("lastname", NpgsqlDbType.Text);
                    command.Parameters.Add("email", NpgsqlDbType.Text);
                    command.Parameters.Add("password", NpgsqlDbType.Text);
                    command.Parameters.Add("phone", NpgsqlDbType.Text);
                    command.Parameters.Add("is_agent", NpgsqlDbType.Boolean);
                    command.Parameters[0].Value = user.Id;
                    command.Parameters[1].Value = user.FirstName;
                    command.Parameters[2].Value = user.LastName;
                    command.Parameters[3].Value = user.Email;
                    command.Parameters[4].Value = user.Password;
                    command.Parameters[5].Value = user.Phone;
                    command.Parameters[6].Value = user.IsAgent;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        public async Task<UserModel> CheckLogin(string email, string password)
        {
            var transaction = Connection.BeginTransaction();
            DbDataReader dataReader = null;
            UserModel user = null;

            try
            {
                const string sql = "SELECT imow.check_login ( :email, :password)";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("email", NpgsqlDbType.Text);
                    storedCommand.Parameters.Add("password", NpgsqlDbType.Text);
                    storedCommand.Parameters[0].Value = email;
                    storedCommand.Parameters[1].Value = password;

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);

                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[5].ToString(), out double lat);
                        double.TryParse(dataReader[6].ToString(), out double lng);
                        bool.TryParse(dataReader[7].ToString(), out bool isAgent);

                        user = new UserModel
                        {
                            Id = id,
                            Phone = dataReader[1].ToString(),
                            FirstName = dataReader[3].ToString(),
                            LastName = dataReader[2].ToString(),
                            Email = dataReader[4].ToString(),
                            Lat = lat,
                            Lng = lng,
                            IsAgent = isAgent
                        };
                    }
                    dataReader.Close();
                    transaction.Commit();
                }
                return user;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<bool> UpdateUser(UserModel user)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.update_user ( :id, :firstname, :lastname, :email, :password, :phone, :is_agent)";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("firstname", NpgsqlDbType.Text);
                    command.Parameters.Add("lastname", NpgsqlDbType.Text);
                    command.Parameters.Add("email", NpgsqlDbType.Text);
                    command.Parameters.Add("password", NpgsqlDbType.Text);
                    command.Parameters.Add("phone", NpgsqlDbType.Text);
                    command.Parameters.Add("is_agent", NpgsqlDbType.Boolean);
                    command.Parameters[0].Value = user.Id;
                    command.Parameters[1].Value = user.FirstName;
                    command.Parameters[2].Value = user.LastName;
                    command.Parameters[3].Value = user.Email;
                    command.Parameters[4].Value = user.Password;
                    command.Parameters[5].Value = user.Phone;
                    command.Parameters[6].Value = user.IsAgent;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        public async Task<List<UserModel>> GetAgents()
        {
            var transaction = Connection.BeginTransaction();
            var agents = new List<UserModel>();

            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_agents ()";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[6].ToString(), out double lat);
                        double.TryParse(dataReader[7].ToString(), out double lng);

                        var agent = new UserModel
                        {
                            Id = id,
                            Phone = dataReader[1].ToString(),
                            LastName = dataReader[3].ToString(),
                            FirstName = dataReader[4].ToString(),
                            Lat = lat,
                            Lng = lng,
                            IsAgent = true
                        };
                        agents.Add(agent);
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return agents;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<UserModel> GetClient(string userId)
        {
            var transaction = Connection.BeginTransaction();
            UserModel client = null;
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_client (:client_id)";
             
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("client_id", NpgsqlDbType.Uuid);
                    storedCommand.Parameters[0].Value = userId;
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[6].ToString(), out double lat);
                        double.TryParse(dataReader[7].ToString(), out double lng);
                        bool.TryParse(dataReader[8].ToString(), out bool isAgent);

                        client = new UserModel
                        {
                            Id = id,
                            Phone = dataReader[1].ToString(),
                            LastName = dataReader[3].ToString(),
                            FirstName = dataReader[4].ToString(),
                            Lat = lat,
                            Lng = lng,
                            IsAgent = isAgent
                        };
                       
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return client;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<bool> AddRating(string agentId, string clientId, int rating, string observation = null)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.insert_rating ( :id, :agent_id, :client_id, :rating, :observation )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("agent_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("client_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("rating", NpgsqlDbType.Integer);
                    command.Parameters.Add("observation", NpgsqlDbType.Text);
                    command.Parameters[0].Value = Guid.NewGuid();
                    command.Parameters[1].Value = agentId;
                    command.Parameters[2].Value = clientId;
                    command.Parameters[3].Value = rating;
                    command.Parameters[4].Value = observation;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        public async Task<bool> CreateConnection(string userId, string version, string deviceId, string connectionId, int type)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.insert_connection ( :user_id, :version, :device_id, :connection_id, :type, :free )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("user_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("version", NpgsqlDbType.Text);
                    command.Parameters.Add("device_id", NpgsqlDbType.Text);
                    command.Parameters.Add("connection_id", NpgsqlDbType.Text);
                    command.Parameters.Add("type", NpgsqlDbType.Smallint);
                    command.Parameters.Add("free", NpgsqlDbType.Boolean);
                    command.Parameters[0].Value = userId;
                    command.Parameters[1].Value = version;
                    command.Parameters[2].Value = deviceId;
                    command.Parameters[3].Value = connectionId;
                    command.Parameters[4].Value = type;
                    command.Parameters[5].Value = true;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }
        #endregion

        #region Apartment
        public async Task<List<ApartmentModel>> GetApartments()
        {
            var transaction = Connection.BeginTransaction();
            var apartments = new List<ApartmentModel>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_apartments ()";
       
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[3].ToString(), out double lat);
                        double.TryParse(dataReader[4].ToString(), out double lng);

                        var apartment = new ApartmentModel
                        {
                            Id = dataReader[0].ToString(),
                            Name = dataReader[1].ToString(),
                            Description = dataReader[2].ToString(),
                            Lat = lat,
                            Lng = lng,
                            Neighbourhood = dataReader[5].ToString(),
                            AgentStatus = dataReader[6].ToString(),
                            PropertyType = dataReader[7].ToString(),
                            HabitationOptions = dataReader[8].ToString()

                        };
                        apartments.Add(apartment);
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return apartments;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<List<ApartmentModel>> GetApartmentsInRange(double lat1, double lat2, double lng1, double lng2, string clientId)
        {
   
            var transaction = Connection.BeginTransaction();
            var apartments = new List<ApartmentModel>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_apartments_in_range ( :lat1, :lat2, :lng1, :lng2, :clientId)";
           
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("lat1", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lat2", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lng1", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lng2", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("clientId", NpgsqlDbType.Uuid);
                    storedCommand.Parameters[0].Value = lat1;
                    storedCommand.Parameters[1].Value = lat2;
                    storedCommand.Parameters[2].Value = lng1;
                    storedCommand.Parameters[3].Value = lng2;
                    storedCommand.Parameters[4].Value = Guid.Parse(clientId);

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[3].ToString(), out double lat);
                        double.TryParse(dataReader[4].ToString(), out double lng);

                        var apartment = new ApartmentModel
                        {
                            Id = dataReader[0].ToString(),
                            Name = dataReader[1].ToString(),
                            Description = dataReader[2].ToString(),
                            Lat = lat,
                            Lng = lng,
                            Neighbourhood = dataReader[5].ToString(),
                            AgentStatus = dataReader[6].ToString(),
                            PropertyType = dataReader[7].ToString(),
                            HabitationOptions = dataReader[8].ToString(),
                            AgentName = dataReader[9].ToString(),
                            AgentPhone = dataReader[10].ToString(),
                            Icon = dataReader[11].ToString(),
                            Rooms = int.Parse(dataReader[12].ToString()),
                            Suites = int.Parse(dataReader[13].ToString()),
                            Vacancies = int.Parse(dataReader[14].ToString()),
                            Area = int.Parse(dataReader[15].ToString()),
                            Address = dataReader[16].ToString(),
                            Enterprise = dataReader[17].ToString(),
                            Iptu = float.Parse(dataReader[18].ToString()),
                            Price = float.Parse(dataReader[19].ToString()),
                            Condominium = float.Parse(dataReader[20].ToString())
                        };
                        apartments.Add(apartment);
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return apartments;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<List<ApartmentModel>> GetApartmentsInRange(double lat1, double lat2, double lng1, double lng2)
        {

            var transaction = Connection.BeginTransaction();
            var apartments = new List<ApartmentModel>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_apartments_in_range ( :lat1, :lat2, :lng1, :lng2)";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("lat1", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lat2", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lng1", NpgsqlDbType.Numeric);
                    storedCommand.Parameters.Add("lng2", NpgsqlDbType.Numeric);
                    storedCommand.Parameters[0].Value = lat1;
                    storedCommand.Parameters[1].Value = lat2;
                    storedCommand.Parameters[2].Value = lng1;
                    storedCommand.Parameters[3].Value = lng2;

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[3].ToString(), out double lat);
                        double.TryParse(dataReader[4].ToString(), out double lng);

                        var apartment = new ApartmentModel
                        {
                            Id = dataReader[0].ToString(),
                            Name = dataReader[1].ToString(),
                            Description = dataReader[2].ToString(),
                            Lat = lat,
                            Lng = lng,
                            Neighbourhood = dataReader[5].ToString(),
                            AgentStatus = dataReader[6].ToString(),
                            PropertyType = dataReader[7].ToString(),
                            HabitationOptions = dataReader[8].ToString(),
                            AgentName = dataReader[9].ToString(),
                            AgentPhone = dataReader[10].ToString(),
                            Icon = dataReader[11].ToString(),
                            Rooms = int.Parse(dataReader[12].ToString()),
                            Suites = int.Parse(dataReader[13].ToString()),
                            Vacancies = int.Parse(dataReader[14].ToString()),
                            Area = int.Parse(dataReader[15].ToString()),
                            Address = dataReader[16].ToString(),
                            Enterprise = dataReader[17].ToString()
                        };
                        apartments.Add(apartment);
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return apartments;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<ApartmentModel> GetApartmentById(string apartmentId)
        {
            var transaction = Connection.BeginTransaction();
            var apartment = new ApartmentModel();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_apartment_by_id ( :apartmentId )";
                    
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("apartmentId", NpgsqlDbType.Uuid);
                    storedCommand.Parameters[0].Value = apartmentId;

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        Guid.TryParse(dataReader[0].ToString(), out Guid id);
                        double.TryParse(dataReader[3].ToString(), out double lat);
                        double.TryParse(dataReader[4].ToString(), out double lng);

                        apartment = new ApartmentModel
                        {
                            Id = dataReader[0].ToString(),
                            Name = dataReader[1].ToString(),
                            Description = dataReader[2].ToString(),
                            Lat = lat,
                            Lng = lng,
                            Neighbourhood = dataReader[5].ToString(),
                            AgentStatus = dataReader[6].ToString(),
                            PropertyType = dataReader[7].ToString(),
                            HabitationOptions = dataReader[8].ToString(),
                            Icon = dataReader[9].ToString(),                             
                        };
                    }
                    dataReader.Close();
                    transaction.Commit();

                }
                return apartment;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<List<string>> GetNeighborhoodsByCity(string city, string state)
        {
            var transaction = Connection.BeginTransaction();
            var neighborhoods = new List<string>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_neighborhoods_by_city ( :city, :state )";
        
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("city", NpgsqlDbType.Text);
                    storedCommand.Parameters.Add("state", NpgsqlDbType.Text);
                    storedCommand.Parameters[0].Value = city;
                    storedCommand.Parameters[1].Value = state;

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        neighborhoods.Add(dataReader[0].ToString());
                    }
                }

                dataReader.Close();
                transaction.Commit();

                return neighborhoods;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<List<string>> GetApartmentPhotos(Guid id)
        {
            var transaction = Connection.BeginTransaction();
            var photos = new List<string>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_apartment_photos ( :id )";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    storedCommand.Parameters.Add("id", NpgsqlDbType.Uuid);
                    storedCommand.Parameters[0].Value = id;

                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        photos.Add(dataReader[0].ToString());
                    }
                }

                dataReader.Close();
                transaction.Commit();

                return photos;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<List<string>> GetPropertyTypes()
        {
            var transaction = Connection.BeginTransaction();
            var types = new List<string>();
            DbDataReader dataReader = null;
            try
            {
                const string sql = "SELECT imow.get_property_types ()";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        types.Add(dataReader[0].ToString());
                    }

                }

                dataReader.Close();
                transaction.Commit();
                return types;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<bool> AddApartmentClick(string clientId, string apartmentId)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.insert_apartment_click ( :client_id, :apartment_id )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("client_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("apartment_id", NpgsqlDbType.Uuid);
                    command.Parameters[0].Value = clientId;
                    command.Parameters[1].Value = apartmentId;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }


        public async Task<bool> ConfirmAttendance(string clientId, string apartmentId, string brokerId)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.confirm_attendance ( :client_id, :apartment_id, :broker_id )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("client_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("apartment_id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("broker_id", NpgsqlDbType.Uuid);
                    command.Parameters[0].Value = clientId;
                    command.Parameters[1].Value = apartmentId;
                    command.Parameters[2].Value = brokerId;
                    var result = await command.ExecuteScalarAsync().ConfigureAwait(false);
                    transaction.Commit();

                    return bool.Parse(result.ToString());
                }
                
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        public async Task<MatchModel> GetMatches(string clientId)
        {
            var transaction = Connection.BeginTransaction();
          
            var matchModel = new MatchModel();
            matchModel.MatchDictionary = new Dictionary<string, UserModel>();
            DbDataReader dataReader = null;

            try
            {
                const string sql = "SELECT imow.get_matches (:client_id)";
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {

                    storedCommand.Parameters.Add("client_id", NpgsqlDbType.Uuid);
                    storedCommand.Parameters[0].Value = clientId;
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        matchModel.MatchDictionary.Add(dataReader[4].ToString(), new UserModel
                        {
                            Id = Guid.Parse(dataReader[0].ToString()),
                            FirstName = dataReader[1].ToString(),
                            LastName = dataReader[2].ToString(),
                            Phone = dataReader[3].ToString(),
                            IsAgent = true,
                        });
                    }
                }

                dataReader.Close();
                transaction.Commit();
                return matchModel;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<MatchModel> GetBrokerMatches(string brokerId, DateTime timestamp)
        {
            var transaction = Connection.BeginTransaction();

            var matchModel = new MatchModel();
            matchModel.BrokerMatchDictionary = new Dictionary<string, IEnumerable<UserModel>>();
            DbDataReader dataReader = null;

            try
            {
                const string sql = "SELECT imow.get_broker_matches (:broker_id, :timestamp)";
                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {

                    storedCommand.Parameters.Add("broker_id", NpgsqlDbType.Uuid);
                    storedCommand.Parameters.Add("timestamp", NpgsqlDbType.Timestamp);
                    storedCommand.Parameters[0].Value = brokerId;
                    storedCommand.Parameters[1].Value = timestamp;
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        var leadBrokerId = dataReader[6].ToString();
                        var userModel = string.IsNullOrEmpty(leadBrokerId) ? new UserModel
                        {
                            Id = Guid.Parse(dataReader[0].ToString()),
                            Timestamp = DateTime.Parse(dataReader[7].ToString()),
                            MatchId = Guid.Parse(dataReader[9].ToString()),

                        } : new UserModel
                        {
                            Id = Guid.Parse(dataReader[0].ToString()),
                            FirstName = dataReader[1].ToString(),
                            LastName = dataReader[2].ToString(),
                            Phone = dataReader[3].ToString(),
                            Timestamp = DateTime.Parse(dataReader[7].ToString()),
                            MatchId = Guid.Parse(dataReader[9].ToString()),
                            IsAgent = false,
                        };

                        if(!string.IsNullOrEmpty(dataReader[8].ToString()))
                        {
                            userModel.MatchTimestamp = DateTime.Parse(dataReader[8].ToString());
                        }

                        var apartmentId = dataReader[4].ToString();

                        if (matchModel.BrokerMatchDictionary.ContainsKey(apartmentId))
                        {
                            var userList = matchModel.BrokerMatchDictionary[apartmentId].ToList();
                            userList.Add(userModel);
                            matchModel.BrokerMatchDictionary[apartmentId] = userList;
                        }
                        else
                        {
                            matchModel.BrokerMatchDictionary.Add(apartmentId, new List<UserModel>{ userModel });
                        }

                      
                    }
                }

                dataReader.Close();
                transaction.Commit();
                return matchModel;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        #endregion

        #region Leads
        public async Task<List<LeadModel>> GetLeads()
        {
            var transaction = Connection.BeginTransaction();
            var leads = new List<LeadModel>();
            DbDataReader dataReader = null;

            try
            {
                const string sql = "SELECT imow.get_leads ()";

                using (var storedCommand = new NpgsqlCommand(sql, Connection))
                {
                    var cursorName = await storedCommand.ExecuteScalarAsync().ConfigureAwait(false);
                    var getCursorSql = "FETCH ALL IN\"" + cursorName + "\"";
                    var command = new NpgsqlCommand(getCursorSql, Connection);
                    dataReader = await command.ExecuteReaderAsync().ConfigureAwait(false);
                    while (await dataReader.ReadAsync().ConfigureAwait(false))
                    {
                        DateTime.TryParse(dataReader[3].ToString(), out DateTime time);

                        var lead = new LeadModel
                        {
                            Id = dataReader[0].ToString(),
                            ClientName = dataReader[1].ToString(),
                            ApartmentChosen = dataReader[2].ToString(),
                            TimeChosen = time,
                            Classification = dataReader[4].ToString()
                        };
                        leads.Add(lead);
                    }
                }

                dataReader.Close();
                transaction.Commit();
                return leads;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(dataReader, transaction);
                return null;
            }
        }

        public async Task<bool> UpdateLead(LeadModel lead)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.update_lead ( :id, :clientname, :apartment, :timechosen, :classification )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("clientname", NpgsqlDbType.Text);
                    command.Parameters.Add("apartment", NpgsqlDbType.Text);
                    command.Parameters.Add("timechosen", NpgsqlDbType.Timestamp);
                    command.Parameters.Add("classification", NpgsqlDbType.Text);
                    command.Parameters[0].Value = lead.Id;
                    command.Parameters[1].Value = lead.ClientName;
                    command.Parameters[2].Value = lead.ApartmentChosen;
                    command.Parameters[3].Value = lead.TimeChosen;
                    command.Parameters[4].Value = lead.Classification;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        public async Task<bool> CreateLead(LeadModel lead)
        {
            var transaction = Connection.BeginTransaction();
            try
            {
                const string sql = "SELECT imow.insert_lead ( :id, :clientname, :apartment, :timechosen, :classification )";
                using (var command = new NpgsqlCommand(sql, Connection))
                {
                    command.Parameters.Add("id", NpgsqlDbType.Uuid);
                    command.Parameters.Add("clientname", NpgsqlDbType.Text);
                    command.Parameters.Add("apartment", NpgsqlDbType.Text);
                    command.Parameters.Add("timechosen", NpgsqlDbType.Time);
                    command.Parameters.Add("classification", NpgsqlDbType.Text);
                    command.Parameters[0].Value = lead.Id;
                    command.Parameters[1].Value = lead.ClientName;
                    command.Parameters[2].Value = lead.ApartmentChosen;
                    command.Parameters[3].Value = lead.TimeChosen;
                    command.Parameters[4].Value = lead.Classification;

                    await command.ExecuteNonQueryAsync().ConfigureAwait(false);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                CloseDataReaderAndRollback(null, transaction);
                return false;
            }
        }

        #endregion
    }

    public class AsyncLazy<T> : Lazy<Task<T>>
    {
        public AsyncLazy(Func<T> valueFactory) : base(() => Task.Factory.StartNew(valueFactory)) { }
        
        public AsyncLazy(Func<Task<T>> taskFactory) : base(() => Task.Factory.StartNew(taskFactory).Unwrap()) { }
        
        public TaskAwaiter<T> GetAwaiter() { return Value.GetAwaiter(); }
    }
}
