﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Imow.WebService.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using Imow.WebServer.Models;
using Imow.WebServer.Services;
using Newtonsoft.Json;
using Imow.WebService.Models;

namespace Imow.WebService.Controllers
{
    [Produces("application/json")]
    [Route("api/Mobile")]
    public class ApartmentController : Controller
    {
        readonly IDatabaseConnectionFactoryService _databaseFactoryService;

        private string AppId = "9f322cd9-cdce-47cd-bf7f-1357d59a427f";
        public ApartmentController(IDatabaseConnectionFactoryService databaseFactoryService)
        {
            _databaseFactoryService = databaseFactoryService;
        }

        [Route("~/api/Apartment/Health")]
        public async Task<string> Health()
        {
            return await Task.FromResult("Live");
        }

        [HttpPost]
        [Route("~/api/Apartment/GetApartments")]
        public async Task<ActionResult> GetApartments()
        {
            try
            {
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var result = await connection.GetApartments();
                    return Json(result);
                }
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/Apartment/GetApartmentsInRange")]
        public async Task<ActionResult> GetApartmentsInRange([FromBody] JObject body)
        {
            try
            {
                double.TryParse(body["lat1"]?.ToString(), out double lat1);
                double.TryParse(body["lat2"]?.ToString(), out double lat2);
                double.TryParse(body["lng1"]?.ToString(), out double lng1);
                double.TryParse(body["lng2"]?.ToString(), out double lng2);

                var id = body["clientId"]?.ToString();

                var random = new Random();
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    return Ok((string.IsNullOrEmpty(id) ? (await connection.GetApartmentsInRange(lat1, lat2, lng1, lng2).ConfigureAwait(false)) : (await connection.GetApartmentsInRange(lat1, lat2, lng1, lng2, id).ConfigureAwait(false))));
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [Route("~/api/Apartment/GetApartmentSearchOptions")]
        public async Task<ActionResult> GetApartmentSearchOptions()
        {
            try
            {
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var types = await connection.GetPropertyTypes().ConfigureAwait(false);
                    var neighborhoods = await connection.GetNeighborhoodsByCity("Rio de Janeiro", "RJ").ConfigureAwait(false);
                    var result = new Dictionary<string, List<string>>
            {
                { "Alugar | Comprar", new List<string> { "Alugar", "Comprar" } },
                { "Tipo", types },
                { "Onde", neighborhoods }
            };
                    return Json(result);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        
        [HttpPost]
        [Route("~/api/Apartment/GetPhotos")]
        public async Task<ActionResult> GetPhotos([FromBody] JObject data)
        {
            try
            {
                var id = Guid.Parse(data["apartmentId"].ToString());
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var photos = await connection.GetApartmentPhotos(id).ConfigureAwait(false);      
                    return Json(photos);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        [HttpPost]
        [Route("~/api/Apartment/RequestMatchBroker")]
        public async Task<ActionResult> RequestMatchBroker([FromBody] JObject body)
        {
            try
            {
                var clientId = body["clientId"]?.ToString();
                var apartmentId = body["apartmentId"]?.ToString();

               using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var addOk = await connection.AddApartmentClick(clientId, apartmentId);
                    if (addOk)
                    {
 
                        var httpClient = new HttpClient();
                        httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic NTc1ZGExYzEtNGMzMi00ZWNmLTk0MGUtY2FhYzljZjJiZTc0");
                        var data = new
                        {
                            app_id = AppId,
                            contents = new { en = "A Customer wants to see an apartment" },
                            included_segments = new string[] { "Brokers" }
                        };

                        var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                        var response = await httpClient.PostAsync("https://onesignal.com/api/v1/notifications", content);

                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/Apartment/ConfirmAttendance")]
        public async Task<ActionResult> ConfirmAttendance([FromBody] JObject body)
        {
            try
            {
                var clientId = body["clientId"]?.ToString();
                var apartmentId = body["apartmentId"]?.ToString();
                var brokerId = body["brokerId"]?.ToString();

                using (var connection = _databaseFactoryService.CreateConnection())
                {

                    var checkAttendance = await connection.ConfirmAttendance(clientId, apartmentId, brokerId);
                    if (checkAttendance)
                    {
                        var httpClient = new HttpClient();
                        httpClient.DefaultRequestHeaders.Add("Authorization", $"Basic NTc1ZGExYzEtNGMzMi00ZWNmLTk0MGUtY2FhYzljZjJiZTc0");
                        var data = new
                        {
                            app_id = AppId,
                            contents = new { en = "We found one broker to show the apartment to you!" },
                            filters = new object[] { new { field = "tag", key = "ClientId", value = clientId } },
                        };

                        var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                        var response = await httpClient.PostAsync("https://onesignal.com/api/v1/notifications", content);

                        var client = await connection.GetClient(clientId);


                        return Ok(client);
                    };


                    return Ok(null);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/Apartment/GetMatches")]
        public async Task<ActionResult> GetMatches([FromBody] JObject body)
        {
            try
            {
               var clientId = body["clientId"]?.ToString();
               using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var matches = await connection.GetMatches(clientId);
                    return Ok(matches.MatchDictionary);       
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        [HttpPost]
        [Route("~/api/Apartment/GetBrokerMatches")]
        public async Task<ActionResult> GetBrokerMatches([FromBody] JObject body)
        {
            try
            {
                var brokerId = body["brokerId"]?.ToString();
                var timestamp = body["lastTimestamp"]?.ToString();
                var dateTime = DateTime.Parse(timestamp);
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var matches = await connection.GetBrokerMatches(brokerId, dateTime);
                    return Ok(matches.BrokerMatchDictionary);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/Apartment/Get")]
        public async Task<ActionResult> Get([FromBody] JObject body)
        {
            try
            {
                var apartmentId = body["apartmentId"]?.ToString();
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var apartment = await connection.GetApartmentById(apartmentId);
                    return Ok(apartment);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}