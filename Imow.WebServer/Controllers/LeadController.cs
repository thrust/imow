﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Imow.WebService.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Imow.WebServer.Models;
using Imow.WebServer.Services;

namespace Imow.WebService.Controllers
{
    [Produces("application/json")]
    [Route("api/Mobile")]
    public class LeadController : Controller
    {
        readonly IDatabaseConnectionFactoryService _databaseFactoryService;

        public LeadController(IDatabaseConnectionFactoryService databaseFactoryService)
        {
            _databaseFactoryService = databaseFactoryService;
        }

        [Route("~/api/Lead/Health")]
        public async Task<string> Health()
        {
            return await Task.FromResult("Live");
        }

        [HttpPost]
        [Route("~/api/Lead/GetLeads")]
        public async Task<ActionResult> GetLeads()
        {
            try
            {
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var result = await connection.GetLeads().ConfigureAwait(false);
                    return Json(result);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/Lead/CreateLead")]
        public async Task<ActionResult> CreateLead([FromBody] JObject body)
        {
            try
            {
                DateTime.TryParse(body["TimeChosen"]?.ToString(), out DateTime time);

                var id = body["Id"]?.ToString();
                var clientName = body["ClientName"]?.ToString();
                var apartment = body["ApartmentChosen"]?.ToString();
                var classification = body["Classification"]?.ToString();
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var updateOk = await connection.CreateLead(new LeadModel
                    {
                        Id = id,
                        ClientName = clientName,
                        ApartmentChosen = apartment,
                        TimeChosen = time,
                        Classification = classification
                    });

                    if (updateOk)
                    {
                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
            
        }

        [HttpPost]
        [Route("~/api/Lead/UpdateLead")]
        public async Task<ActionResult> UpdateLead([FromBody] JObject body)
        {
            try
            {
                DateTime.TryParse(body["TimeChosen"]?.ToString(), out DateTime time);
                int.TryParse(body["Classification"]?.ToString(), out int classification);

                var id = body["Id"]?.ToString();
                var clientName = body["ClientName"]?.ToString();
                var apartment = body["ApartmentChosen"]?.ToString();
                //  var classification = body["Classification"]?.ToString();
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var updateOk = await connection.UpdateLead(new LeadModel
                    {
                        Id = id,
                        ClientName = clientName,
                        ApartmentChosen = apartment,
                        TimeChosen = time,
                        Classification = (classification == 0) ? "Hot" : (classification == 1) ? "Medium" : "Cold"
                    });

                    if (updateOk)
                    {
                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}