﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Imow.WebService.Services;
using Imow.WebService.Models;
using Imow.WebServer.Services;

namespace Imow.WebService.Controllers
{
	[Produces("application/json")]
	[Route("api/User")]
    public class UserController : Controller
    {
        readonly IDatabaseConnectionFactoryService _databaseFactoryService;

        public UserController(IDatabaseConnectionFactoryService databaseFactoryService)
        {
            _databaseFactoryService = databaseFactoryService;
        }

		[HttpPost]
		[Route("~/api/User/CheckLogin")]
        public async Task<ActionResult> CheckLogin([FromBody] JObject body)
        {
            try
            {
                var email = body["email"]?.ToString();
                var pass = body["password"]?.ToString();
                var version = body["version"]?.Value<string>();
                var deviceId = body["deviceid"]?.Value<string>();
                var connectionId = body["connectionid"]?.Value<string>();
                using (var connection = _databaseFactoryService.CreateConnection())
                {

                    int.TryParse(body["type"]?.Value<string>(), out int type);
                    var user = await connection.CheckLogin(email, pass);
                    var connectionOk = await connection.CreateConnection(user.Id.ToString(), version, deviceId, connectionId, type);

                    if (user != null)
                    {
                        return Json(user);
                    }

                    return Unauthorized();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

		[HttpPost]
		[Route("~/api/User/RegisterUser")]
        public async Task<ActionResult> RegisterUser([FromBody] JObject body)
        {
            try
            {
                bool.TryParse(body["IsAgent"]?.ToString(), out bool agent);

                var fName = body["FirstName"]?.ToString();
                var lName = body["LastName"]?.ToString();
                var email = body["Email"]?.ToString();
                var password = body["Password"]?.ToString();
                var phone = body["Phone"]?.ToString();
                var isAgent = agent;

                if (!string.IsNullOrEmpty(fName) && !string.IsNullOrEmpty(lName) && !string.IsNullOrEmpty(email)
                    && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(phone))
                {
                    using (var connection = _databaseFactoryService.CreateConnection())
                    {
                        var createOk = await connection.CreateUser(new UserModel
                        {
                            Id = Guid.NewGuid(),
                            FirstName = fName,
                            LastName = lName,
                            Email = email,
                            Password = password,
                            Phone = phone,
                            IsAgent = isAgent
                        });
                        if (createOk)
                        {
                            return Ok();
                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/User/UpdateUser")]
        public async Task<ActionResult> UpdateUser([FromBody] JObject body)
        {
            try
            {
                bool.TryParse(body["IsAgent"]?.ToString(), out bool agent);

                var fName = body["FirstName"]?.ToString();
                var lName = body["LastName"]?.ToString();
                var email = body["Email"]?.ToString();
                var password = body["Password"]?.ToString();
                var phone = body["Phone"]?.ToString();
                var isAgent = agent;

                Guid.TryParse(body["Id"]?.ToString(), out Guid id);

                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var updateOk = await connection.UpdateUser(new UserModel
                    {
                        Id = id,
                        FirstName = fName,
                        LastName = lName,
                        Email = email,
                        Password = password,
                        Phone = phone,
                        IsAgent = isAgent
                    });

                    if (updateOk)
                    {
                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
		[Route("~/api/User/GetAgentsNearby")]
        public async Task<ActionResult> GetAgentsNearby([FromBody] JObject body)
        {
            try
            {
                var apartmentId = body["apartmentId"]?.ToString();
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var agents = await connection.GetAgents();
                    var apartment = await connection.GetApartmentById(apartmentId);

                    var result = CheckAgentDistance(agents, apartment);
                    //Get apartment, get lat long and check agents that are close using their lat long
                    return Json(result);
                }
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        [Route("~/api/User/AddRating")]
        public async Task<ActionResult> AddRating([FromBody] JObject body)
        {
            try
            {
                var agentId = body["agentId"]?.ToString();
                var clientId = body["clientId"]?.ToString();
                var observation = body["observation"]?.ToString();

                int.TryParse(body["rating"]?.ToString(), out int rating);
                using (var connection = _databaseFactoryService.CreateConnection())
                {
                    var addOk = await connection.AddRating(agentId, clientId, rating, observation);
                    if (addOk)
                    {
                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        private ActionResult CheckAgentDistance(List<UserModel> agents, ApartmentModel apartment)
        {
            try
            {
                var availableAgents = new List<UserModel>();

                const double earthRadius = 6378.16;

                var lat2 = apartment.Lat;
                var long2 = apartment.Lng;

                foreach (var agent in agents)
                {
                    var lat1 = agent.Lat;
                    var long1 = agent.Lng;
                    var dlon = Radians(long2 - long1);
                    var dlat = Radians(lat2 - lat1);
                    var a = (Math.Sin(dlat / 2) * Math.Sin(dlat / 2)) + Math.Cos(Radians(lat1)) * Math.Cos(Radians(lat2)) * (Math.Sin(dlon / 2) * Math.Sin(dlon / 2));
                    var angle = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

                    if ((angle * earthRadius) <= 5) //TODO: Check which distance is considered close, middle and far
                    {
                        availableAgents.Add(agent);
                    }
                }

                return Json(availableAgents);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        private static double Radians(double x)
        {
            const double pi = 3.141592653589793;
            return x * pi / 180;
        }
    }
}