﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Imow.WebServer
{
    public class Program
    {
        public static IConfigurationRoot HostingConfiguration;
        public static void Main(string[] args)
        {
            HostingConfiguration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json", optional: false)
                .Build();

            BuildWebHost(args)
                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) => WebHost.CreateDefaultBuilder(args).UseUrls(HostingConfiguration["ServerUrls"]).UseStartup<Startup>().Build();
    }
}
