﻿using System;

namespace Imow.WebServer.Models
{
    public class LeadModel
    {
        public string Id { get; set; }
        public string ClientName { get; set; }
        public string Phone { get; set; }
        public string ApartmentChosen { get; set; }
        public DateTime TimeChosen { get; set; }
        public string Classification { get; set; }
    }
}
