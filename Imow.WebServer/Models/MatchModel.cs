﻿using Imow.WebService.Models;
using System;
using System.Collections.Generic;

namespace Imow.WebServer.Models
{
    public class MatchModel
    {
        public Dictionary<string, UserModel> MatchDictionary { get; set; }
        public Dictionary<string, IEnumerable<UserModel>> BrokerMatchDictionary { get; set; }
    }
}
