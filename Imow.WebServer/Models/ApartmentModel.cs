﻿namespace Imow.WebService.Models
{
    public class ApartmentModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rooms { get; set; }
        public int Suites { get; set; }
        public int Vacancies { get; set; }
        public int Area { get; set; }
        public string Address { get; set; }
        public string Enterprise { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Neighbourhood { get; set; }
        public string AgentStatus { get; set; }
        public string PropertyType { get; set; }
        public string HabitationOptions { get; set; }
        public string AgentName { get; set; }
        public string AgentPhone { get; set; }
        public string Icon { get; set; }
        public float Price { get; set; }
        public float Iptu { get; set; }
        public float Condominium { get; set; }
    }
}
