﻿using System;

namespace Imow.WebService.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public Guid MatchId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public bool IsAgent { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DateTime? Timestamp { get; set; }
        public DateTime? MatchTimestamp { get; set; }
    }
}
