// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
    [Register ("DetailsView")]
    partial class DetailsView
    {
        [Outlet]
        UIKit.UILabel areaLabel { get; set; }


        [Outlet]
        UIKit.UIButton ChatButton { get; set; }


        [Outlet]
        UIKit.UICollectionView collectionView { get; set; }


        [Outlet]
        UIKit.UILabel condominiumLabel { get; set; }


        [Outlet]
        UIKit.UITextView descriptionTextView { get; set; }


        [Outlet]
        UIKit.UILabel garageLabel { get; set; }


        [Outlet]
        UIKit.UILabel iptuLabel { get; set; }


        [Outlet]
        UIKit.UILabel optionsLabel { get; set; }


        [Outlet]
        UIKit.UILabel priceLabel { get; set; }


        [Outlet]
        UIKit.UILabel roomLabel { get; set; }


        [Outlet]
        UIKit.UILabel suitesLabel { get; set; }


        [Outlet]
        UIKit.UILabel TitleLabel { get; set; }


        [Outlet]
        UIKit.UILabel typeLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (areaLabel != null) {
                areaLabel.Dispose ();
                areaLabel = null;
            }

            if (ChatButton != null) {
                ChatButton.Dispose ();
                ChatButton = null;
            }

            if (collectionView != null) {
                collectionView.Dispose ();
                collectionView = null;
            }

            if (condominiumLabel != null) {
                condominiumLabel.Dispose ();
                condominiumLabel = null;
            }

            if (descriptionTextView != null) {
                descriptionTextView.Dispose ();
                descriptionTextView = null;
            }

            if (garageLabel != null) {
                garageLabel.Dispose ();
                garageLabel = null;
            }

            if (iptuLabel != null) {
                iptuLabel.Dispose ();
                iptuLabel = null;
            }

            if (optionsLabel != null) {
                optionsLabel.Dispose ();
                optionsLabel = null;
            }

            if (priceLabel != null) {
                priceLabel.Dispose ();
                priceLabel = null;
            }

            if (roomLabel != null) {
                roomLabel.Dispose ();
                roomLabel = null;
            }

            if (suitesLabel != null) {
                suitesLabel.Dispose ();
                suitesLabel = null;
            }

            if (TitleLabel != null) {
                TitleLabel.Dispose ();
                TitleLabel = null;
            }

            if (typeLabel != null) {
                typeLabel.Dispose ();
                typeLabel = null;
            }
        }
    }
}