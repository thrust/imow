﻿using System.Collections.Generic;
using System.Linq;
using Imow.Core.Models.Lists;
using Imow.Core.ViewModels;
using Imow.Touch.Utils;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;

namespace Imow.Touch
{
    [MvxTabPresentation(WrapInNavigationController = true)]
    public partial class HomeView : MvxViewController
	{
        public new HomeViewModel ViewModel => base.ViewModel as HomeViewModel;
        private ExpandableTableSource source;

        private IEnumerable<FilterSection> _filters;
        public IEnumerable<FilterSection> Filters
        {
            get
            {
                return _filters;
            }
            set
            {
                _filters = value;
                if(source != null)
                {
                    source.ExpandableTableModel = value;
                }
            }
        }

        private string _currentBannerImage;
        public string CurrentBannerImage
        {
            get
            {
                return _currentBannerImage;
            }
            set
            {
                if(_currentBannerImage != value)
                {
                    _currentBannerImage = value;
                    BannerImageView.Image = UIImage.FromBundle($"Images/{value}");
                }
            }
        }

		public HomeView () : base ("HomeView", null)
		{
		}

        public UIColor NavigationBarColor = TouchConstants.GreyColorCCC;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            EdgesForExtendedLayout = UIRectEdge.None;

            SearchOptionsTableView.BackgroundColor = UIColor.Clear;
            View.BackgroundColor = UIColor.Black;

            source = new ExpandableTableSource(ViewModel?.Filters, SearchOptionsTableView)
            {
                Command = ViewModel.AddFilterCommand
            };
            SearchOptionsTableView.Source = source;
            SearchOptionsTableView.SeparatorColor = UIColor.Clear;
            SearchOptionsTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            if(FilterButton != null)
            {
                FilterButton.TouchUpInside += (sender, e) =>
                {
                    ViewModel.FilterCommand.Execute(null);
                    TabBarController.SelectedIndex = 1;
                };

                FilterButton.Layer.CornerRadius = 10.0f;
                FilterButton.ClipsToBounds = true;
                FilterButton.BackgroundColor = TouchConstants.OrangeColor;
            }

			//this.SetNavigationBarAppearance(NavigationBarColor);
			//SetNeedsStatusBarAppearanceUpdate();

            Title = "Home";

			var set = this.CreateBindingSet<HomeView, HomeViewModel> ();
            set.Bind(this).For(t => t.Filters).To(vm => vm.Filters);
            set.Bind(this).For(t => t.CurrentBannerImage).To(vm => vm.CurrentBanner);
			//set.Bind (FilterButton).To (vm => vm.FilterCommand);
			set.Apply ();
		}
	}
}

