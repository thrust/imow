// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
    [Register ("LeadDetailsView")]
    partial class LeadDetailsView
    {
        [Outlet]
        UIKit.UILabel ApartmentLabel { get; set; }


        [Outlet]
        UIKit.UIPickerView ClassificationPicker { get; set; }


        [Outlet]
        UIKit.UILabel ClientNameLabel { get; set; }


        [Outlet]
        UIKit.UIButton DoneButton { get; set; }


        [Outlet]
        UIKit.UILabel TimeLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ApartmentLabel != null) {
                ApartmentLabel.Dispose ();
                ApartmentLabel = null;
            }

            if (ClassificationPicker != null) {
                ClassificationPicker.Dispose ();
                ClassificationPicker = null;
            }

            if (ClientNameLabel != null) {
                ClientNameLabel.Dispose ();
                ClientNameLabel = null;
            }

            if (DoneButton != null) {
                DoneButton.Dispose ();
                DoneButton = null;
            }

            if (TimeLabel != null) {
                TimeLabel.Dispose ();
                TimeLabel = null;
            }
        }
    }
}