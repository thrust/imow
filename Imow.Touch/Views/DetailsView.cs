﻿using System;
using System.Linq;
using CoreGraphics;
using Foundation;
using Imow.Core.ViewModels;
using Imow.Touch.Cells;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;

namespace Imow.Touch
{
    public partial class DetailsView : MvxViewController
	{
        public new DetailsViewModel ViewModel => base.ViewModel as DetailsViewModel;

		public DetailsView () : base ("DetailsView", null)
		{
			ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
		}

        private bool _isMatchChange;
        public bool IsMatchChange
        {
            get
            {
                return _isMatchChange;
            }
            set
            {
                if(_isMatchChange != value)
                {
                    _isMatchChange = value;
                    if(value)
                    {
                        EnableWhatsappButton();
                    }
                    else
                    {
                        NavigationItem.SetRightBarButtonItem(null, true);
                    }

                }
            }
        }

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            var collectionSource = new MagazineFlowLayout(collectionView, this, DetailsViewImageCell.Nib, DetailsViewImageCell.Key);
            collectionView.Source = collectionSource;
            collectionView.PagingEnabled = true;

            ChatButton.Hidden = true;

			var set = this.CreateBindingSet<DetailsView, DetailsViewModel> ();
			set.Bind (ChatButton).To (vm => vm.ChatWithAgentCommand);
			set.Bind (TitleLabel).For (t => t.Text).To (vm => vm.Name);
            set.Bind (descriptionTextView).For (t => t.Text).To (vm => vm.Description);
            set.Bind (optionsLabel).For(t => t.Text).To(vm => vm.RentOrBuy);
            set.Bind (priceLabel).For(t => t.Text).To(vm => vm.Price);
            set.Bind (condominiumLabel).For(t => t.Text).To(vm => vm.Condominium);
            set.Bind (iptuLabel).For(t => t.Text).To(vm => vm.Iptu);
            set.Bind (typeLabel).For(t => t.Text).To(vm => vm.Type);
            set.Bind (roomLabel).For(t => t.Text).To(vm => vm.Rooms);
            set.Bind (suitesLabel).For(t => t.Text).To(vm => vm.Suites);
            set.Bind (garageLabel).For(t => t.Text).To(vm => vm.Garage);
            set.Bind (areaLabel).For(t => t.Text).To(vm => vm.Area);
            set.Bind (collectionSource).To(vm => vm.Images);
            set.Bind (this).For(t => t.IsMatchChange).To(vm => vm.IsMatch);
			set.Apply ();

            EdgesForExtendedLayout = UIRectEdge.None;

            if(ViewModel.IsMatch)
            {
                EnableWhatsappButton();
            }
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

        private void EnableWhatsappButton()
        {
            InvokeOnMainThread(() => NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIImage.FromBundle("Images/whatsapp.png"), UIBarButtonItemStyle.Plain, (sender, e) => ViewModel.ChatWithAgentCommand.Execute(null)), true));
        }

        public class MagazineFlowLayout : MvxCollectionViewSource, IUICollectionViewDelegateFlowLayout
        {
            private readonly DetailsView View;
            private readonly string _resuseIdentifier;

            public MagazineFlowLayout(UICollectionView collectionView, DetailsView magazineView, UINib nibName, NSString reuseIdentifier)
                : base(collectionView, reuseIdentifier)
            {
                _resuseIdentifier = reuseIdentifier;
                CollectionView.RegisterNibForCell(nibName, reuseIdentifier);
                CollectionView.ShowsHorizontalScrollIndicator = false;
                View = magazineView;
                collectionView.PagingEnabled = true;
            }

            [Export("collectionView:layout:sizeForItemAtIndexPath:")]
            public CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
            {
                return new CGSize(collectionView.Frame.Width, collectionView.Frame.Height - collectionView.ContentInset.Top);
            }

            protected override UICollectionViewCell GetOrCreateCellFor(UICollectionView collectionView, NSIndexPath indexPath, object item)
            {
                var cell = (DetailsViewImageCell)collectionView.DequeueReusableCell(_resuseIdentifier, indexPath) ?? DetailsViewImageCell.Create();

                if((ItemsSource?.Count() ?? 0) == 0)
                {
                    cell.TotalItems = 1;
                    cell.ItemIndex = 0;
                    cell.IsLocal = true;
                }
                else
                {
                    cell.TotalItems = ItemsSource?.Count() ?? 0;
                    cell.ItemIndex = indexPath.Row;
                    cell.IsLocal = false;
                }

                cell.DataContext = item;
                return cell;
            }

            [Export("collectionView:layout:minimumInteritemSpacingForSectionAtIndex:")]
            public nfloat GetMinimumInteritemSpacingForSection(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
            {
                return 0.0f;
            }

            [Export("collectionView:layout:minimumLineSpacingForSectionAtIndex:")]
            public nfloat GetMinimumLineSpacingForSection(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
            {
                return 0.0f;
            }

            [Export("collectionView:layout:insetForSectionAtIndex:")]
            public UIEdgeInsets GetInsetForSection(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
            {
                return UIEdgeInsets.Zero;
            }

			public override nint GetItemsCount(UICollectionView collectionView, nint section)
			{
                return Math.Max(ItemsSource?.Count() ?? 0, 1);
			}
		}
	}
}

