// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
    [Register ("AccountView")]
    partial class AccountView
    {
        [Outlet]
        UIKit.UIButton CancelButton { get; set; }


        [Outlet]
        UIKit.UIButton ConfirmButton { get; set; }


        [Outlet]
        UIKit.UITextField EmailTextField { get; set; }


        [Outlet]
        UIKit.UITextField FirstNameTextField { get; set; }


        [Outlet]
        UIKit.UITextField LastNameTextField { get; set; }


        [Outlet]
        UIKit.UITextField PasswordTextField { get; set; }


        [Outlet]
        UIKit.UITextField PhoneTextField { get; set; }


        [Outlet]
        UIKit.UITextField RepeatPasswordTextField { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CancelButton != null) {
                CancelButton.Dispose ();
                CancelButton = null;
            }

            if (ConfirmButton != null) {
                ConfirmButton.Dispose ();
                ConfirmButton = null;
            }

            if (EmailTextField != null) {
                EmailTextField.Dispose ();
                EmailTextField = null;
            }

            if (FirstNameTextField != null) {
                FirstNameTextField.Dispose ();
                FirstNameTextField = null;
            }

            if (LastNameTextField != null) {
                LastNameTextField.Dispose ();
                LastNameTextField = null;
            }

            if (PasswordTextField != null) {
                PasswordTextField.Dispose ();
                PasswordTextField = null;
            }

            if (PhoneTextField != null) {
                PhoneTextField.Dispose ();
                PhoneTextField = null;
            }

            if (RepeatPasswordTextField != null) {
                RepeatPasswordTextField.Dispose ();
                RepeatPasswordTextField = null;
            }
        }
    }
}