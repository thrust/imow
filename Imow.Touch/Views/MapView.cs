﻿using System.Linq;
using CoreGraphics;
using CoreLocation;
using Foundation;
using Imow.Core.Models;
using Imow.Core.Utils;
using Imow.Core.ViewModels;
using Imow.Touch.Utils;
using MapKit;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;

namespace Imow.Touch
{
    [MvxTabPresentation(WrapInNavigationController = true)]
    public partial class MapView : MvxViewController, IMKMapViewDelegate
	{
		public new MapViewModel ViewModel => base.ViewModel as MapViewModel;

        private MvxFluentBindingDescriptionSet<MapView, MapViewModel> bindingSet;
        private MvxImageViewLoader ImageViewLoader;

        private MvxObservableCollection<Apartment> _filteredApartments;
        public MvxObservableCollection<Apartment> FilteredApartments
        {
            get
            {
                return _filteredApartments;
            }
            set
            {
                if (value != null && (!_filteredApartments?.SequenceEqual(value) ?? true))
                {
					_filteredApartments = value;

                    mapView.RemoveAnnotations(mapView.Annotations);
					foreach (var apartment in _filteredApartments)
					{
						var annotation = new ApartmentPinAnnotation
						{
							Coordinate = new CLLocationCoordinate2D(apartment.Lat, apartment.Lng),
							Title = apartment.Name,
							Subtitle = apartment.Description,
							Apartment = apartment
						};

						mapView?.AddAnnotation(annotation);
					}
                }
            }
        }

		public MapView () : base ("MapView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            apartmentView.Hidden = true;
            mapView.Delegate = this;

            EdgesForExtendedLayout = UIRectEdge.None;

            var locationManager = new CLLocationManager();
            locationManager.RequestWhenInUseAuthorization();
            var location = mapView.UserLocation?.Coordinate;

            mapView.SetRegion(new MKCoordinateRegion(
                new CLLocationCoordinate2D(Constants.DefaultLatitude, Constants.DefaultLongitude),
                new MKCoordinateSpan(0.2, 0.2)), false);

            //apartmentContactButton.BackgroundColor = TouchConstants.OrangeColor;
            apartmentContactButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            apartmentContactButton.TitleLabel.TextAlignment = UITextAlignment.Center;
            apartmentContactButton.Layer.CornerRadius = 10.0f;
            apartmentContactButton.ClipsToBounds = true;
            //apartmentContactButton.SetTitle(apartmentContactButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

            //moreInfoButton.BackgroundColor = TouchConstants.OrangeColor;
            moreInfoButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            moreInfoButton.TitleLabel.TextAlignment = UITextAlignment.Center;
            moreInfoButton.Layer.CornerRadius = 10.0f;
            moreInfoButton.ClipsToBounds = true;
            //moreInfoButton.SetTitle(moreInfoButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

            ImageViewLoader = new MvxImageViewLoader(() => apartmentImage)
            {
                DefaultImagePath = "res:" + NSBundle.MainBundle.PathForResource("Images/building1", "png")
            };

            bindingSet = this.CreateBindingSet<MapView, MapViewModel>();
            bindingSet.Bind(this).For(t => t.FilteredApartments).To(vm => vm.FilteredApartments);
            bindingSet.Bind(apartmentContactButton).To(vm => vm.ContactBrokerCommand);
            bindingSet.Bind(ImageViewLoader).To(vm => vm.SelectedApartment.Icon);
            bindingSet.Bind(moreInfoButton).To(vm => vm.ShowDetailsCommand);
            bindingSet.Apply();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
		}

		[Export ("mapView:didSelectAnnotationView:")]
		public void DidSelectAnnotationView (MKMapView mapView, MKAnnotationView view)
		{
            var apartmentPin = view.Annotation as ApartmentPinAnnotation;
            mapView.DeselectAnnotation(apartmentPin, false);

            if(ViewModel.SelectedApartment == null || ViewModel.SelectedApartment.Id != apartmentPin.Apartment.Id)
            {
                ViewModel.SelectedApartment = apartmentPin.Apartment;
                apartmentView.Hidden = false;
                apartmentLabel.Text = ViewModel.SelectedApartment.Name;
                apartmentDescription.Text = ViewModel.SelectedApartment.Description;
                apartmentPrice.Text = $"R$ {ViewModel.SelectedApartment.Price.ToString("C")}";
                apartmentIptu.Text = $"IPTU: R$ {ViewModel.SelectedApartment.Iptu.ToString("C")}";
                apartmentCond.Text = $"Cond: R$ {ViewModel.SelectedApartment.Condominium.ToString("C")}";
                apartmentRoom.Text = ViewModel.SelectedApartment.Rooms;
                apartmentBath.Text = ViewModel.SelectedApartment.Suites;
                apartmentGarage.Text = ViewModel.SelectedApartment.Vacancies;
                apartmentArea.Text = ViewModel.SelectedApartment.Area;
            }
            else
            {
                ViewModel.SelectedApartment = null;
                apartmentView.Hidden = true;
                apartmentLabel.Text = string.Empty;
                apartmentDescription.Text = string.Empty;
                apartmentPrice.Text = string.Empty;
                apartmentIptu.Text = string.Empty;
                apartmentCond.Text = string.Empty;
                apartmentRoom.Text = string.Empty;
                apartmentBath.Text = string.Empty;
                apartmentGarage.Text = string.Empty;
                apartmentArea.Text = string.Empty;
            }
		}

		[Export ("mapView:viewForAnnotation:")]
		public MKAnnotationView GetViewForAnnotation (MKMapView mapView, IMKAnnotation annotation)
		{
			var pin = (MKPinAnnotationView)mapView.DequeueReusableAnnotation ("apartment");

			if (pin == null) 
            {
				pin = new MKPinAnnotationView (annotation, "apartment");
			} else 
            {
				pin.Annotation = annotation;
			}

			pin.CanShowCallout = true;
			pin.RightCalloutAccessoryView = new UIButton (UIButtonType.DetailDisclosure);

			var apartmentAnnotation = annotation as ApartmentPinAnnotation;

            if(string.IsNullOrEmpty(apartmentAnnotation?.Apartment?.AgentPhone))
            {
                switch (apartmentAnnotation?.Apartment.Status)
                {
                    case AgentStatus.Close:
                    {
                        pin.PinTintColor = TouchConstants.GreenColor;
                        break;
                    }
                    case AgentStatus.Middle:
                    {
                        pin.PinTintColor = TouchConstants.OrangeColor;
                        break;
                    }
                    case AgentStatus.Far:
                    {
                        pin.PinTintColor = TouchConstants.RedColor;
                        break;
                    }
                }
            }
            else
            {
                pin.PinTintColor = TouchConstants.BlueColor;
            }

			return pin;
		}

		[Export ("mapView:regionDidChangeAnimated:")]
		public void RegionChanged (MapKit.MKMapView mapView, bool animated)
		{
			mapView.RemoveAnnotations (mapView.Annotations);
			var topleft = mapView.ConvertPoint (new CGPoint (0, 0), mapView);
			var pointBottomRight = new CGPoint (mapView.Frame.Size.Width, mapView.Frame.Size.Height);
			var bottomright = mapView.ConvertPoint (pointBottomRight, mapView);

			var lat1 = (topleft.Latitude < bottomright.Latitude) ? topleft.Latitude : bottomright.Latitude;
			var lat2 = (topleft.Latitude > bottomright.Latitude) ? topleft.Latitude : bottomright.Latitude;

			var lng1 = (topleft.Longitude < bottomright.Longitude) ? topleft.Longitude : bottomright.Longitude;
			var lng2 = (topleft.Longitude > bottomright.Longitude) ? topleft.Longitude : bottomright.Longitude;

			ViewModel.Range = new ApartmentRange { Lat1 = lat1, Lat2 = lat2, Lng1 = lng1, Lng2 = lng2 };
			ViewModel.GetApartmentsInRangeCommand.Execute (null);
        }
	}

	public class ApartmentPinAnnotation : MKPointAnnotation
	{
		public double Lat
		{ 
			get 
            { 
                return Coordinate.Latitude; 
            }
			set 
			{
				WillChangeValue ("coordinate");
				SetCoordinate (new CLLocationCoordinate2D(value, Lng));
				DidChangeValue ("coordinate");
			}
		}

		public double Lng 
        {
			get 
            { 
                return Coordinate.Longitude; 
            }
			set 
            {
				WillChangeValue ("coordinate");
				SetCoordinate (new CLLocationCoordinate2D (Lat, value));
				DidChangeValue ("coordinate");
			}
		}

		private Apartment _apartment;
		public Apartment Apartment 
		{
			get 
            { 
                return _apartment; 
            }
			set 
            { 
                _apartment = value; 
            }
		}
	}
}

