﻿using Foundation;
using Imow.Core.ViewModels;
using Imow.Touch.Utils;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;

namespace Imow.Touch
{
	[MvxTabPresentation(WrapInNavigationController = true)]
    public partial class AccountView : MvxViewController, IUIGestureRecognizerDelegate
	{
		public AccountView () : base ("AccountView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            EdgesForExtendedLayout = UIRectEdge.None;

            ConfirmButton.Layer.CornerRadius = 10.0f;
            ConfirmButton.ClipsToBounds = true;
            ConfirmButton.BackgroundColor = TouchConstants.OrangeColor;
            ConfirmButton.SetTitle(ConfirmButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

			var set = this.CreateBindingSet<AccountView, AccountViewModel> ();
			set.Bind (FirstNameTextField).For (t => t.Text).To (vm => vm.FirstName);
			set.Bind (LastNameTextField).For (t => t.Text).To (vm => vm.LastName);
			set.Bind (EmailTextField).For (t => t.Text).To (vm => vm.Email);
            set.Bind (PhoneTextField).For (t => t.Text).To (vm => vm.Phone);
            set.Bind (PasswordTextField).For(t => t.Text).To(vm => vm.Password);
            set.Bind (CancelButton).To (vm => vm.LogoutCommand);
            set.Bind (ConfirmButton).To (vm => vm.ConfirmCommand);
			set.Apply ();

            FirstNameTextField.AttributedPlaceholder = new NSAttributedString(FirstNameTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            LastNameTextField.AttributedPlaceholder = new NSAttributedString(LastNameTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            EmailTextField.AttributedPlaceholder = new NSAttributedString(EmailTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            PhoneTextField.AttributedPlaceholder = new NSAttributedString(PhoneTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            PasswordTextField.AttributedPlaceholder = new NSAttributedString(PasswordTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));

            DismissKeyboardOnBackgroundTap();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
        }

        protected void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer
            {
                CancelsTouchesInView = false,
                Delegate = this,
            };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }

        [Export("gestureRecognizer:shouldReceiveTouch:")]
        public bool ShouldReceiveTouch(UIGestureRecognizer recognizer, UITouch touch)
        {
            var firstNameTextFieldPoint = touch.LocationInView(FirstNameTextField);
            var lastNameTextFieldPoint = touch.LocationInView(LastNameTextField);
            var emailTextFieldPoint = touch.LocationInView(EmailTextField);
            var passwordTextFieldPoint = touch.LocationInView(PasswordTextField);
            var phoneTextFieldPoint = touch.LocationInView(PhoneTextField);
            var cancelButtonPoint = touch.LocationInView(CancelButton);
            var confirmButtonPoint = touch.LocationInView(ConfirmButton);

            return FirstNameTextField.HitTest(firstNameTextFieldPoint, null) == null && LastNameTextField.HitTest(lastNameTextFieldPoint, null) == null && EmailTextField.HitTest(emailTextFieldPoint, null) == null && PasswordTextField.HitTest(passwordTextFieldPoint, null) == null && PhoneTextField.HitTest(phoneTextFieldPoint, null) == null && CancelButton.HitTest(cancelButtonPoint, null) == null && ConfirmButton.HitTest(confirmButtonPoint, null) == null;
        }
	}
}

