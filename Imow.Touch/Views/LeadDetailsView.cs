﻿using System;
using System.Collections.Generic;
using Imow.Core;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;

namespace Imow.Touch
{
	public partial class LeadDetailsView : MvxViewController
	{
		public new LeadDetailsViewModel ViewModel => base.ViewModel as LeadDetailsViewModel;

		public LeadDetailsView () : base ("LeadDetailsView", null)
		{
				
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			DoneButton.TouchUpInside += (sender, e) => 
			{
				//ViewModel.ConfirmCommand.Execute (null);
				NavigationController.PopViewController (true);
			};

			var model = new PickerDataModel ();

			//ViewModel.Classification = LeadClassification.Hot;

			//model.ValueChanged += (sender, e) => 
			//{
			//	ViewModel.Classification = (LeadClassification)model.SelectedIndex;
			//};

			ClassificationPicker.Model = model;
			//ClassificationPicker.Select ((int)ViewModel.Classification, 0, true);

			var set = this.CreateBindingSet<LeadDetailsView, LeadDetailsViewModel> ();
			set.Bind (ClientNameLabel).For (t => t.Text).To (vm => vm.ClientName);
			set.Bind (ApartmentLabel).For (t => t.Text).To (vm => vm.ApartmentChosen);
			set.Bind (TimeLabel).For (t => t.Text).To (vm => vm.TimeChosen);
			set.Apply ();

			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}

	public class PickerDataModel : UIPickerViewModel
	{
		public List<string> Classification = new List<string> { "Quente", "Médio", "Frio" };
		public event EventHandler ValueChanged;

		public string SelectedItem 
		{
			get { return Classification [selectedIndex]; }
		}

		public int SelectedIndex 
		{
			get { return selectedIndex; }
		}

		int selectedIndex = 0;

		public override nint GetRowsInComponent (UIPickerView pickerView, nint component)
		{
			return Classification.Count;
		}

		public override string GetTitle (UIPickerView pickerView, nint row, nint component)
		{
			return Classification [(int)row];
		}

		public override nint GetComponentCount (UIPickerView pickerView)
		{
			return 1;
		}

		public override void Selected (UIPickerView pickerView, nint row, nint component)
		{
			selectedIndex = (int)row;
            ValueChanged?.Invoke(this, new EventArgs()); //Same as: ValueChanged (this, new EventArgs ());
        }
	}
}

