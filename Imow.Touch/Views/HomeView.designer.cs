// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
	[Register ("HomeView")]
	partial class HomeView
	{
		[Outlet]
		UIKit.UIImageView BannerImageView { get; set; }

		[Outlet]
		UIKit.UIButton FilterButton { get; set; }

		[Outlet]
		UIKit.UITableView SearchOptionsTableView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (FilterButton != null) {
				FilterButton.Dispose ();
				FilterButton = null;
			}

			if (SearchOptionsTableView != null) {
				SearchOptionsTableView.Dispose ();
				SearchOptionsTableView = null;
			}

			if (BannerImageView != null) {
				BannerImageView.Dispose ();
				BannerImageView = null;
			}
		}
	}
}
