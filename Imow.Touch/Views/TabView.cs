﻿using System;
using System.Collections.Generic;
using Imow.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.Platform;
using MvvmCross.Platform.Core;
using UIKit;

namespace Imow.Touch
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public class TabView : MvxTabBarViewController<TabViewModel>
	{
		private int _createdSoFarCount = 0;
        public new TabViewModel ViewModel => base.ViewModel as TabViewModel;

        public TabView()
        {
            ViewDidLoad();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // first time around this will be null, second time it will be OK
            if (ViewModel == null)
            {
                return;
            }

            View.BackgroundColor = UIColor.White;
            Title = "Imow";

            ViewModel.SubscriptionTokenAction = loginMesssage =>
            {
                Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                {
                    try
                    {
                        ViewControllers = new UIViewController[] { };
                        ViewModel.InitTabs();
                        InitializeTabs();
                    }
                    catch (Exception e)
                    {

                    }
                });
            };

            InitializeTabs();
		}

		public override void ViewWillAppear(bool animated)
		{
            base.ViewWillAppear(animated);

            if (NavigationController?.NavigationBar != null)
            {
                NavigationController.NavigationBar.BackgroundColor = UIColor.Black;
                NavigationController.NavigationBar.BarTintColor = UIColor.Black;
                NavigationController.NavigationBar.TintColor = UIColor.White;
                NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes { ForegroundColor = UIColor.White };
            }
		}

		private UIViewController CreateTabFor (string title, string imageName, IMvxViewModel viewModel)
		{
			var controller = new UINavigationController ();
			var screen = this.CreateViewControllerFor (viewModel) as UIViewController;
			SetTitleAndTabBarItem (screen, title, imageName);

            if (controller?.NavigationBar != null)
            {
                controller.NavigationBar.BackgroundColor = UIColor.Black;
                controller.NavigationBar.BarTintColor = UIColor.Black;
                controller.NavigationBar.TintColor = UIColor.White;
                controller.NavigationBar.TitleTextAttributes = new UIStringAttributes { ForegroundColor = UIColor.White };
            }

			controller.PushViewController (screen, false);
			return controller;
		}

		private void SetTitleAndTabBarItem (UIViewController screen, string title, string imageName)
		{
			screen.Title = title;
            screen.TabBarItem = new UITabBarItem (title, UIImage.FromBundle ("Images/Tabs/" + imageName + ".png"), _createdSoFarCount);
			_createdSoFarCount++;
		}

        public override void ItemSelected(UITabBar tabbar, UITabBarItem item)
        {
            SelectItem((int)item.Tag);
        }

        public void InitializeTabs()
        {
            _createdSoFarCount = 0;
            try
            {
                var viewController = new List<UIViewController>();
                var isAgent = ViewModel.CurrentUser?.IsAgent ?? false;
                if (isAgent)
                {
                    viewController = new List<UIViewController>()
                    {
                        CreateTabFor("Leads", "file-list-tick-7", ViewModel?.LeadsViewModel),
                        //CreateTabFor("Settings", "gear-7", ViewModel?.SettingsViewModel)
                    };
                }
                else
                {
                    viewController = new List<UIViewController>()
                    {
                        CreateTabFor("Home", "house-7", ViewModel?.HomeViewModel),
                        CreateTabFor("Map", "pin-map-7", ViewModel?.MapViewModel),
                        //CreateTabFor("Settings", "gear-7", ViewModel?.SettingsViewModel)
                    };
                }

                if(ViewModel.CurrentUser != null)
                {
                    viewController.Add(CreateTabFor("Account", "circle-user-7", ViewModel?.AccountViewModel));
                }
                else
                {
                    viewController.Add(CreateTabFor("Login", "circle-user-7", ViewModel?.LoginViewModel));
                }

                ViewControllers = viewController.ToArray();
                CustomizableViewControllers = new UIViewController[] { };
                SelectItem(0);
            }
            catch (Exception e)
            {

            }
        }

        private void SelectItem(int index)
        {
            try
            {
                Title = ViewControllers?[index]?.Title ?? "Imow";
                NavigationController.NavigationBarHidden = true;//(Title?.ToLower() == "map") || (Title?.ToLower() == "login");
                SelectedViewController = ViewControllers?[index];
            }
            catch(Exception e)
            {
                var a = e;
            }
        }
	}
}

