﻿using Imow.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;

namespace Imow.Touch
{
    [MvxTabPresentation(WrapInNavigationController = true)]
    public partial class SettingsView : MvxViewController
	{
		public SettingsView () : base ("SettingsView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
            // Perform any additional setup after loading the view, typically from a nib.

            var set = this.CreateBindingSet<SettingsView, SettingsViewModel>();
            set.Bind(logoutButton).To(vm => vm.LogoutCommand);
            set.Apply();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

