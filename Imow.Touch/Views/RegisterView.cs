﻿using Foundation;
using Imow.Core.ViewModels;
using Imow.Touch.Utils;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using UIKit;

namespace Imow.Touch
{
    public partial class RegisterView : MvxViewController, IUITableViewDelegate, IUIGestureRecognizerDelegate
	{
		public new RegisterViewModel ViewModel => base.ViewModel as RegisterViewModel;

		public RegisterView () : base ("RegisterView", null)
		{
            ModalPresentationStyle = UIModalPresentationStyle.OverCurrentContext;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationController.SetNavigationBarHidden (false, true);
			NavigationItem.Title = "Cadastro Imow";

            EdgesForExtendedLayout = UIRectEdge.None;

			var source = new MvxSimpleTableViewSource (UserTypeTableView, UserTypeCell.Key, UserTypeCell.Key);
			source.ItemsSource = ViewModel.UserTypes;
			UserTypeTableView.RowHeight = 40;
			UserTypeTableView.Source = source;
			UserTypeTableView.Delegate = this;
			UserTypeTableView.AlwaysBounceVertical = false;
			UserTypeTableView.AllowsMultipleSelection = false;

            UserTypeTableView.Layer.CornerRadius = 10.0f;
            UserTypeTableView.BackgroundColor = UIColor.FromRGB(204, 204, 204);

            DoneButton.Layer.CornerRadius = 10.0f;
            DoneButton.ClipsToBounds = true;
            DoneButton.BackgroundColor = TouchConstants.OrangeColor;
            DoneButton.SetTitle(DoneButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

			var set = this.CreateBindingSet<RegisterView, RegisterViewModel> ();
			set.Bind (FirstNameTextField).For (t => t.Text).To (vm => vm.FirstName);
			set.Bind (LastNameTextField).For (t => t.Text).To (vm => vm.LastName);
			set.Bind (EmailTextField).For (t => t.Text).To (vm => vm.Email);
			set.Bind (PasswordTextField).For (t => t.Text).To (vm => vm.Password);
			set.Bind (PhoneTextField).For (t => t.Text).To (vm => vm.Phone);
			set.Bind (DoneButton).To (vm => vm.DoneCommand);
            set.Apply ();

            FirstNameTextField.AttributedPlaceholder = new NSAttributedString(FirstNameTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            LastNameTextField.AttributedPlaceholder = new NSAttributedString(LastNameTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            EmailTextField.AttributedPlaceholder = new NSAttributedString(EmailTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            PhoneTextField.AttributedPlaceholder = new NSAttributedString(PhoneTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            PasswordTextField.AttributedPlaceholder = new NSAttributedString(PasswordTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));

            UserTypeTableView.ReloadData ();

            DismissKeyboardOnBackgroundTap();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		[Export ("tableView:didSelectRowAtIndexPath:")]
		public void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.CellAt (indexPath);
			cell.Accessory = (indexPath.Row >= 0) ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;

			ViewModel.IsAgent = (indexPath.Row != 0);
		}

		[Export ("tableView:didDeselectRowAtIndexPath:")]
		public void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.CellAt (indexPath);
			cell.Accessory = UITableViewCellAccessory.None;
        }

        protected void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer
            {
                CancelsTouchesInView = false,
                Delegate = this,
            };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }

        [Export("gestureRecognizer:shouldReceiveTouch:")]
        public bool ShouldReceiveTouch(UIGestureRecognizer recognizer, UITouch touch)
        {
            var firstNameTextFieldPoint = touch.LocationInView(FirstNameTextField);
            var lastNameTextFieldPoint = touch.LocationInView(LastNameTextField);
            var emailTextFieldPoint = touch.LocationInView(EmailTextField);
            var passwordTextFieldPoint = touch.LocationInView(PasswordTextField);
            var phoneTextFieldPoint = touch.LocationInView(PhoneTextField);
            var buttonPoint = touch.LocationInView(DoneButton);

            return FirstNameTextField.HitTest(firstNameTextFieldPoint, null) == null && LastNameTextField.HitTest(lastNameTextFieldPoint, null) == null && EmailTextField.HitTest(emailTextFieldPoint, null) == null && PasswordTextField.HitTest(passwordTextFieldPoint, null) == null && PhoneTextField.HitTest(phoneTextFieldPoint, null) == null && DoneButton.HitTest(buttonPoint, null) == null;
        }
	}
}

