﻿using System;
using System.Collections.Generic;
using Foundation;
using Imow.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.Platform;
using UIKit;

namespace Imow.Touch
{
	[MvxTabPresentation (WrapInNavigationController = true)]
	public partial class LeadsView : MvxViewController
	{
		public new LeadsViewModel ViewModel => base.ViewModel as LeadsViewModel;

		public LeadsView () : base ("LeadsView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            var source = new MvxSimpleTableViewSource (LeadsTableView, LeadCell.Key);
			LeadsTableView.Source = source;
            LeadsTableView.ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;

            EdgesForExtendedLayout = UIRectEdge.None;

			var set = this.CreateBindingSet<LeadsView, LeadsViewModel> ();
            set.Bind (source).To (vm => vm.Leads);
            set.Bind (source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowDetailsCommand);
			set.Apply ();
		}
	}
}

