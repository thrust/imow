﻿using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using UIKit;
using Imow.Core.ViewModels;
using Imow.Touch.Utils;
using Foundation;
using MvvmCross.iOS.Views.Presenters.Attributes;

namespace Imow.Touch
{
    [MvxTabPresentation(WrapInNavigationController = true)]
    public partial class LoginView : MvxViewController, IUITextFieldDelegate, IUIGestureRecognizerDelegate
    {
        private new LoginViewModel ViewModel => base.ViewModel as LoginViewModel; 
		public LoginView () : base ("LoginView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            LoginButton.Layer.CornerRadius = 10.0f;
            LoginButton.ClipsToBounds = true;
            LoginButton.BackgroundColor = TouchConstants.OrangeColor;
            LoginButton.SetTitle(LoginButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

            RegisterButton.SetTitle(RegisterButton.Title(UIControlState.Normal).ToUpper(), UIControlState.Normal);

            EmailTextField.Delegate = this;
            PasswordTextField.Delegate = this;

			var set = this.CreateBindingSet<LoginView, LoginViewModel> ();
			set.Bind (LoginButton).To (vm => vm.LoginCommand);
			set.Bind (RegisterButton).To (vm => vm.RegisterCommand);
			set.Bind (EmailTextField).For (t => t.Text).To (vm => vm.Email);
			set.Bind (PasswordTextField).For (t => t.Text).To (vm => vm.Password);
            set.Apply ();

            EmailTextField.AttributedPlaceholder = new NSAttributedString(EmailTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));
            PasswordTextField.AttributedPlaceholder = new NSAttributedString(PasswordTextField.Placeholder, foregroundColor: UIColor.FromRGB(204, 204, 204));

            LogoImageView.Image = UIImage.FromFile("Images/logo.png");

            DismissKeyboardOnBackgroundTap();
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			NavigationController.SetNavigationBarHidden (true, true);
		}

        [Export("textFieldShouldReturn:")]
        public bool ShouldReturn(UITextField textField)
        {
            if(textField == EmailTextField)
            {
                EmailTextField?.ResignFirstResponder();
                PasswordTextField?.BecomeFirstResponder();
            }
            else if(textField == PasswordTextField)
            {
                View.EndEditing(true);
                ViewModel?.LoginCommand?.Execute(null);
            }
            return false;
        }

        protected void DismissKeyboardOnBackgroundTap()
        {
            // Add gesture recognizer to hide keyboard
            var tap = new UITapGestureRecognizer
            {
                CancelsTouchesInView = false,
                Delegate = this,
            };
            tap.AddTarget(() => View.EndEditing(true));
            View.AddGestureRecognizer(tap);
        }

        [Export("gestureRecognizer:shouldReceiveTouch:")]
        public bool ShouldReceiveTouch(UIGestureRecognizer recognizer, UITouch touch)
        {
            var emailTextFieldPoint = touch.LocationInView(EmailTextField);
            var passwordTextFieldPoint = touch.LocationInView(PasswordTextField);
            var buttonPoint = touch.LocationInView(LoginButton);

            return EmailTextField.HitTest(emailTextFieldPoint, null) == null && PasswordTextField.HitTest(passwordTextFieldPoint, null) == null && LoginButton.HitTest(buttonPoint, null) == null;
        }
	}
}

