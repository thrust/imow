// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
	[Register ("MapView")]
	partial class MapView
	{
		[Outlet]
		UIKit.UILabel apartmentArea { get; set; }

		[Outlet]
		UIKit.UILabel apartmentBath { get; set; }

		[Outlet]
		UIKit.UILabel apartmentCond { get; set; }

		[Outlet]
		UIKit.UIButton apartmentContactButton { get; set; }

		[Outlet]
		UIKit.UILabel apartmentDescription { get; set; }

		[Outlet]
		UIKit.UILabel apartmentGarage { get; set; }

		[Outlet]
		UIKit.UIImageView apartmentImage { get; set; }

		[Outlet]
		UIKit.UILabel apartmentIptu { get; set; }

		[Outlet]
		UIKit.UILabel apartmentLabel { get; set; }

		[Outlet]
		UIKit.UILabel apartmentPrice { get; set; }

		[Outlet]
		UIKit.UILabel apartmentRoom { get; set; }

		[Outlet]
		UIKit.UIView apartmentView { get; set; }

		[Outlet]
		MapKit.MKMapView mapView { get; set; }

		[Outlet]
		UIKit.UIButton moreInfoButton { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (apartmentArea != null) {
				apartmentArea.Dispose ();
				apartmentArea = null;
			}

			if (apartmentGarage != null) {
				apartmentGarage.Dispose ();
				apartmentGarage = null;
			}

			if (apartmentBath != null) {
				apartmentBath.Dispose ();
				apartmentBath = null;
			}

			if (apartmentRoom != null) {
				apartmentRoom.Dispose ();
				apartmentRoom = null;
			}

			if (apartmentContactButton != null) {
				apartmentContactButton.Dispose ();
				apartmentContactButton = null;
			}

			if (apartmentImage != null) {
				apartmentImage.Dispose ();
				apartmentImage = null;
			}

			if (apartmentLabel != null) {
				apartmentLabel.Dispose ();
				apartmentLabel = null;
			}

			if (apartmentView != null) {
				apartmentView.Dispose ();
				apartmentView = null;
			}

			if (mapView != null) {
				mapView.Dispose ();
				mapView = null;
			}

			if (moreInfoButton != null) {
				moreInfoButton.Dispose ();
				moreInfoButton = null;
			}

			if (apartmentDescription != null) {
				apartmentDescription.Dispose ();
				apartmentDescription = null;
			}

			if (apartmentPrice != null) {
				apartmentPrice.Dispose ();
				apartmentPrice = null;
			}

			if (apartmentIptu != null) {
				apartmentIptu.Dispose ();
				apartmentIptu = null;
			}

			if (apartmentCond != null) {
				apartmentCond.Dispose ();
				apartmentCond = null;
			}
		}
	}
}
