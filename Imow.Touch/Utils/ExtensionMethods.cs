﻿using System;
using System.Linq;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using Imow.Core.Models.Lists;
using Imow.Core.Utils;
using MvvmCross.iOS.Views;
using MvvmCross.Platform.UI;
using UIKit;

namespace Imow.Touch.Utils
{
    public static class TouchExtensionMethods
	{
		public static UIColor ToUiColor(this MvxColor color)
		{
			return UIColor.FromRGBA(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
		}

		public static MvxColor ToMvxColor(this UIColor color)
		{
			nfloat red, green, blue, alpha;
			color.GetRGBA(out red, out green, out blue, out alpha);
			return new MvxColor(Convert.ToInt32(red * 255.0f), Convert.ToInt32(green * 255.0f), Convert.ToInt32(blue * 255.0f), Convert.ToInt32(alpha * 255.0f));
		}

		public static UIColor GetContrastColor(this UIColor color)
		{
			return color.ToMvxColor().GetContrastColor().ToUiColor();
		}

		public static DateTime NSDateToDateTime(this NSDate date)
		{
			try
			{
				var reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
				return reference.AddSeconds(date.SecondsSinceReferenceDate);
			}
			catch (Exception)
			{
				return DateTime.UtcNow;
			}
		}

		public static NSDate DateTimeToNSDate(this DateTime date)
		{
			try
			{
				var reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
				return NSDate.FromTimeIntervalSinceReferenceDate((date - reference).TotalSeconds);
			}
			catch (Exception)
			{
				return NSDate.Now;
			}
		}

		public static void MakeDashedLine(this UIView view)
		{
			var layer = view.Layer;
			var subLayers = layer.Sublayers?.Where(l => l.Name == "DashedTopLine");

			if (subLayers?.Any() ?? false)
			{
				foreach (var sublayer in subLayers)
				{
					sublayer.RemoveFromSuperLayer();
				}
			}

			var color = view.BackgroundColor;
			view.BackgroundColor = UIColor.Clear;
			var cgColor = color.CGColor;

			var shapeLayer = new CAShapeLayer();
			var frameSize = view.Frame.Size;
			var shapeRect = new CGRect(0, 0, frameSize.Width, frameSize.Height);

			shapeLayer.Name = "DashedTopLine";
			shapeLayer.Bounds = shapeRect;
			shapeLayer.Position = new CGPoint(frameSize.Width / 2, frameSize.Height / 2);
			shapeLayer.FillColor = UIColor.Clear.CGColor;
			shapeLayer.StrokeColor = cgColor;
			shapeLayer.LineWidth = 1;
			shapeLayer.LineJoin = CAShapeLayer.JoinRound;
			shapeLayer.LineDashPattern = new NSNumber[] { 4, 4 };

			var path = new CGPath();
			path.MoveToPoint(0, 0);
			path.AddLineToPoint(frameSize.Width, 0);
			shapeLayer.Path = path;

			view.Layer.AddSublayer(shapeLayer);
		}

        public static void SetNavigationBarAppearance(this MvxViewController viewController, UIColor navBarColor)
        {
            var contrastColor = navBarColor.GetContrastColor();
            var navigationBar = viewController.NavigationController?.NavigationBar;
            if (navigationBar != null)
            {
                navigationBar.BackgroundColor = navBarColor;
                navigationBar.BarTintColor = navBarColor;

                navigationBar.TintColor = contrastColor;
                navigationBar.TitleTextAttributes = new UIStringAttributes { ForegroundColor = contrastColor };
            }
        }

        public static string GetLocalFilePath(string filename)
        {
            // Use the SpecialFolder enum to get the Personal folder on the iOS file system.
            // Then get or create the Library folder within this personal folder.
            // Storing the database here is a best practice.
            var docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libFolder = System.IO.Path.Combine(docFolder, "..", "Library");

            if (!System.IO.Directory.Exists(libFolder))
            {
                System.IO.Directory.CreateDirectory(libFolder);
            }

            return System.IO.Path.Combine(libFolder, filename);
        }

        public static UIColor GetFilterColor(this FilterChild filterChild, int index)
        {
            return filterChild.GetFilterMvxColor(index)?.ToUiColor() ?? UIColor.White;
        }
	}
}
