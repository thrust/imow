﻿using Imow.Core.Utils;
using UIKit;

namespace Imow.Touch.Utils
{
    public static class TouchConstants
	{
		public static UIColor BlackColor = Constants.BlackColor.ToUiColor();
        public static UIColor GreyColorCCC = Constants.GreyColorCCC.ToUiColor();
        public static UIColor GreenColor = Constants.GreenColor.ToUiColor();
        public static UIColor BlueColor = Constants.BlueColor.ToUiColor();
        public static UIColor RedColor = Constants.RedColor.ToUiColor();
        public static UIColor OrangeColor = Constants.OrangeColor.ToUiColor();
        public static UIColor DarkGreenColor = Constants.DarkGreenColor.ToUiColor();
        public static UIColor DarkBlueColor = Constants.DarkBlueColor.ToUiColor();
        public static UIColor DarkRedColor = Constants.DarkRedColor.ToUiColor();
        public static UIColor DarkOrangeColor = Constants.DarkOrangeColor.ToUiColor();
	}
}
