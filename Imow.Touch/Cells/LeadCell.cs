﻿using System;

using Foundation;
using Imow.Core;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Imow.Touch
{
	public partial class LeadCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("LeadCell");
        public static readonly UINib Nib = UINib.FromName("LeadCell", NSBundle.MainBundle);

        public static float Height = 40.0f;

		protected LeadCell (IntPtr handle) : base (handle)
		{
			Accessory = UITableViewCellAccessory.DisclosureIndicator;
            SelectionStyle = UITableViewCellSelectionStyle.None;

			// Note: this .ctor should not contain any initialization logic.
			this.DelayBind (() =>
            {
				var set = this.CreateBindingSet<LeadCell, Lead> ();
                set.Bind (ClientNameLabel).To (l => l.CustomerName);
                set.Bind (ApartmentLabel).To (l => l.ApartmentName);
                set.Bind (acceptButton).To(l => l.MatchCommand);
                set.Bind (rejectButton).To(l => l.RejectCommand);
                set.Bind (buttonsView).For("Visibility").To(l => l.Status).WithConversion("StatusVisibility");
				set.Apply ();
			});
		}
	}
}
