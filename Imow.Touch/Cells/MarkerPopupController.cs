﻿using System;
using System.Windows.Input;
using CoreGraphics;
using Foundation;
using MvvmCross.Core.ViewModels;
using UIKit;

namespace Imow.Touch.Cells
{
    public partial class MarkerPopupController : UIViewController, IUIPopoverPresentationControllerDelegate, IUIAdaptivePresentationControllerDelegate
    {
        public IMvxCommand ContactBrokerCommand;
        private string _apartmentName;

        public MarkerPopupController(string apartmentName) : base("MarkerPopupController", null)
        {
            _apartmentName = apartmentName;

            ModalPresentationStyle = UIModalPresentationStyle.Popover;
            PopoverPresentationController.Delegate = this;
            PreferredContentSize = new CGSize(200, 500);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            apartmentNameLabel.Text = _apartmentName;
            contactButton.TouchUpInside += (sender, e) => ContactBrokerCommand?.Execute(null);
        }

        [Export("adaptivePresentationStyleForPresentationController:")]
        public UIModalPresentationStyle GetAdaptivePresentationStyle(UIPresentationController forPresentationController)
        {
            return UIModalPresentationStyle.OverCurrentContext;
        }
    }
}

