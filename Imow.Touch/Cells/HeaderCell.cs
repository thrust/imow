﻿using System;
using Foundation;
using Imow.Touch.Utils;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Imow.Touch
{
    public partial class HeaderCell : MvxTableViewHeaderFooterView
	{
		public static readonly NSString Key = new NSString ("HeaderCell");
		public static readonly UINib Nib = UINib.FromName("HeaderCell", NSBundle.MainBundle);

        public const float Height = 50.0f;

		public UILabel CellLabel { get { return Label; } }
        public int Section;

		protected HeaderCell (IntPtr handle) : base (handle)
		{
			BindingContext.DataContextChanged += (sender, e) =>
			{
				this.ClearAllBindings();

                ContentView.BackgroundColor = UIColor.Black;

                backgroundView.Layer.CornerRadius = 10.0f;
                backgroundView.ClipsToBounds = true;

                Label.TextColor = UIColor.White;
                Label.Font = UIFont.BoldSystemFontOfSize(15.0f);
                Label.Text = Label.Text.ToUpper();

                switch (Section)
                {
                    case 0:
                    {
                        backgroundView.BackgroundColor = TouchConstants.GreenColor;
                        return;
                    }
                    case 1:
                    {
                        backgroundView.BackgroundColor = TouchConstants.BlueColor;
                        return;
                    }
                    case 2:
                    {
                        backgroundView.BackgroundColor = TouchConstants.RedColor;
                        return;
                    }
                    case 3:
                    {
                        backgroundView.BackgroundColor = TouchConstants.OrangeColor;
                        return;
                    }
                }
			};
		}

        public override void PrepareForReuse()
        {
            ContentView.BackgroundColor = UIColor.White;
            if(backgroundView != null)
            {
                backgroundView.BackgroundColor = UIColor.Clear;
            }
            base.PrepareForReuse();
        }
	}
}
