﻿using System;

using Foundation;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Imow.Touch
{
	public partial class TableViewCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("TableViewCell");
		public static readonly UINib Nib = UINib.FromName("TableViewCell", NSBundle.MainBundle);

		public UILabel CellLabel { get { return Label; } }

		protected TableViewCell (IntPtr handle) : base (handle)
		{
            SelectionStyle = UITableViewCellSelectionStyle.None;
            TintColor = UIColor.White;

			// Note: this .ctor should not contain any initialization logic.
		}
	}
}
