﻿using System;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Imow.Touch.Cells
{
    public partial class DetailsViewImageCell : MvxCollectionViewCell
    {
        public static readonly NSString Key = new NSString("DetailsViewImageCell");
        public static readonly UINib Nib = UINib.FromName("DetailsViewImageCell", NSBundle.MainBundle);

        public int TotalItems;
        public int ItemIndex;
        public bool IsLocal;

        protected DetailsViewImageCell(IntPtr handle) : base(handle)
        {
            BackgroundColor = UIColor.Clear;

            BindingContext.DataContextChanged += (sender, e) =>
            {
                this.ClearAllBindings();

                var dataContextString = DataContext as string;
                if (dataContextString != null)
                {
                    if(!IsLocal)
                    {
                        var _loader = new MvxImageViewLoader(() => imageView);

                        var set = this.CreateBindingSet<DetailsViewImageCell, string>();
                        set.Bind(_loader).To(vm => vm);
                        set.Apply();
                    }
                    else
                    {
                        imageView.Image = UIImage.FromBundle("Images/no_image.jpg");
                    }

                    beforeLabel.Hidden = (ItemIndex == 0);
                    nextLabel.Hidden = (ItemIndex == TotalItems - 1);

                    beforeLabel.Text = $"{ItemIndex} of {TotalItems}";
                    nextLabel.Text = $"{ItemIndex + 2} of {TotalItems}";
                    currentLabel.Text = $"{ItemIndex + 1} of {TotalItems}";
                }
            };
        }

        public override void PrepareForReuse()
        {
            base.PrepareForReuse();
            this.ClearAllBindings();
        }

        public static DetailsViewImageCell Create()
        {
            return (DetailsViewImageCell)Nib.Instantiate(null, null)[0];
        }
    }
}
