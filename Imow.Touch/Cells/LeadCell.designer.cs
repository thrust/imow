// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
    [Register ("LeadCell")]
    partial class LeadCell
    {
        [Outlet]
        UIKit.UIButton acceptButton { get; set; }


        [Outlet]
        UIKit.UILabel ApartmentLabel { get; set; }


        [Outlet]
        UIKit.UIView buttonsView { get; set; }


        [Outlet]
        UIKit.UIImageView ClientImageView { get; set; }


        [Outlet]
        UIKit.UILabel ClientNameLabel { get; set; }


        [Outlet]
        UIKit.UIButton rejectButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (acceptButton != null) {
                acceptButton.Dispose ();
                acceptButton = null;
            }

            if (ApartmentLabel != null) {
                ApartmentLabel.Dispose ();
                ApartmentLabel = null;
            }

            if (buttonsView != null) {
                buttonsView.Dispose ();
                buttonsView = null;
            }

            if (ClientImageView != null) {
                ClientImageView.Dispose ();
                ClientImageView = null;
            }

            if (ClientNameLabel != null) {
                ClientNameLabel.Dispose ();
                ClientNameLabel = null;
            }

            if (rejectButton != null) {
                rejectButton.Dispose ();
                rejectButton = null;
            }
        }
    }
}