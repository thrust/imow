// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch.Cells
{
    [Register ("MarkerPopupController")]
    partial class MarkerPopupController
    {
        [Outlet]
        UIKit.UIImageView apartmentImageView { get; set; }

        [Outlet]
        UIKit.UILabel apartmentNameLabel { get; set; }

        [Outlet]
        UIKit.UIButton contactButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (apartmentImageView != null) {
                apartmentImageView.Dispose ();
                apartmentImageView = null;
            }

            if (apartmentNameLabel != null) {
                apartmentNameLabel.Dispose ();
                apartmentNameLabel = null;
            }

            if (contactButton != null) {
                contactButton.Dispose ();
                contactButton = null;
            }
        }
    }
}