// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch
{
    [Register ("HeaderCell")]
    partial class HeaderCell
    {
        [Outlet]
        UIKit.UIView backgroundView { get; set; }


        [Outlet]
        UIKit.UILabel Label { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (backgroundView != null) {
                backgroundView.Dispose ();
                backgroundView = null;
            }

            if (Label != null) {
                Label.Dispose ();
                Label = null;
            }
        }
    }
}