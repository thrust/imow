// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Imow.Touch.Cells
{
    [Register ("DetailsViewImageCell")]
    partial class DetailsViewImageCell
    {
        [Outlet]
        UIKit.UILabel beforeLabel { get; set; }


        [Outlet]
        UIKit.UILabel currentLabel { get; set; }


        [Outlet]
        UIKit.UIImageView imageView { get; set; }


        [Outlet]
        UIKit.UILabel nextLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (beforeLabel != null) {
                beforeLabel.Dispose ();
                beforeLabel = null;
            }

            if (currentLabel != null) {
                currentLabel.Dispose ();
                currentLabel = null;
            }

            if (imageView != null) {
                imageView.Dispose ();
                imageView = null;
            }

            if (nextLabel != null) {
                nextLabel.Dispose ();
                nextLabel = null;
            }
        }
    }
}