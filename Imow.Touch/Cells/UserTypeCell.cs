﻿using System;
using Foundation;
using Imow.Core.Models;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace Imow.Touch
{
    public partial class UserTypeCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("UserTypeCell");
        public static readonly UINib Nib = UINib.FromName("UserTypeCell", NSBundle.MainBundle);

		public UserTypeCell (IntPtr handle) : base (handle)
		{
			ContentView.BackgroundColor = UIColor.Clear;

			this.DelayBind (() => 
            {
				var set = this.CreateBindingSet<UserTypeCell, UserType> ();
				set.Bind (UserTypeLabel).To (u => u.Name);
				set.Apply ();
			});
		}
	}
}
