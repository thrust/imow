﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using MvvmCross.Core.ViewModels;
using UIKit;
using Imow.Core.Models.Lists;
using System.Linq;

namespace Imow.Touch
{
    public class ExpandableTableSource : UITableViewSource
	{
		public IMvxCommand Command { get; set; }

        private IEnumerable<FilterSection> _expandableTableModel;
        public IEnumerable<FilterSection> ExpandableTableModel
        {
            get
            {
                return _expandableTableModel;
            }
            set
            {
                try
                {
                    _expandableTableModel = value;

                    if((value?.Count() ?? 0) > 0)
                    {
                        isOpen = new bool[value.Count()];
                    }

                    _tableView?.ReloadData();
                }
                catch(Exception e)
                {}
            }
        }

        private bool[] isOpen = null;

        private UITableView _tableView;

		private EventHandler _headerButtonCommand;

        public ExpandableTableSource (IEnumerable<FilterSection> items, UITableView tableView)
		{
            _tableView = tableView;
            ExpandableTableModel = items;

			_tableView.RegisterNibForCellReuse (UINib.FromName (TableViewCell.Key, NSBundle.MainBundle), TableViewCell.Key);
			_tableView.RegisterNibForHeaderFooterViewReuse (UINib.FromName (HeaderCell.Key, NSBundle.MainBundle), HeaderCell.Key);

			_headerButtonCommand = (sender, e) => 
            {
				var button = sender as UIButton;
				var section = button.Tag;

                if(isOpen != null && isOpen.Length > section)
                {
                    isOpen[section] = !isOpen[section];
                }

                // Animate the section cells
                _tableView.ReloadSections(NSIndexSet.FromIndex(section), UITableViewRowAnimation.Automatic);
			};
		}

		public override nint NumberOfSections (UITableView tableView)
		{
            return ExpandableTableModel?.Count() ?? 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
            try
            {
                return (isOpen != null && isOpen.Length > section && !isOpen[section]) ? (0) : (ExpandableTableModel?.ElementAt((int)section)?.Count ?? 0);
            }
            catch(Exception e)
            {
                return 0;
            }
		}

		public override nfloat GetHeightForHeader (UITableView tableView, nint section)
		{
            return HeaderCell.Height;
		}

		public override nfloat EstimatedHeightForHeader (UITableView tableView, nint section)
		{
            return HeaderCell.Height;
		}

        public override nfloat GetHeightForFooter(UITableView tableView, nint section)
        {
            return 0.0f;
        }
		

		public override UIView GetViewForHeader (UITableView tableView, nint section)
		{
			var header = _tableView.DequeueReusableHeaderFooterView (HeaderCell.Key) as HeaderCell;

            try
            {
                header.CellLabel.Text = ExpandableTableModel?.ElementAt((int)section)?.Title;
                header.Section = (int)section;
                foreach (var view in header.Subviews)
                {
                    if (view is HiddenHeaderButton)
                    {
                        view.RemoveFromSuperview();
                    }
                }
                header.DataContext = new object();
                var hiddenButton = CreateHiddenHeaderButton(header.Bounds, section);
                header.AddSubview(hiddenButton);
            }
            catch(Exception e)
            {
            }

            return header;
		}
		
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = _tableView.DequeueReusableCell (TableViewCell.Key, indexPath) as TableViewCell;
            try
            {
                var rowData = ExpandableTableModel?.ElementAt((int)indexPath.Section)?[indexPath.Row];
                if (rowData != null)
                {
                    cell.CellLabel.Text = rowData.Title;
                    cell.Accessory = (rowData.IsSelected) ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
                }
            }
            catch(Exception e)
            {
            }

			return cell;
		}
		
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = _tableView.CellAt (indexPath) as TableViewCell;
			var text = cell.CellLabel.Text;
            var section = indexPath.Section;
            var childPosition = indexPath.Row;

            try
            {
                var filterSection = ExpandableTableModel?.ElementAt(section);
                if (filterSection != null)
                {
                    var filterChild = filterSection[childPosition];
                    if (filterChild != null)
                    {
                        filterChild.IsSelected = !filterChild.IsSelected;
                    }

                    Command?.Execute(filterSection);
                }

            }
            catch(Exception e)
            {
                
            }
            finally
            {
                _tableView?.ReloadSections(NSIndexSet.FromIndex(indexPath.Section), UITableViewRowAnimation.Automatic);
            }
		}

		public override void RowDeselected (UITableView tableView, NSIndexPath indexPath)
		{
			//var cell = tableView.CellAt (indexPath);
			//cell.Accessory = UITableViewCellAccessory.None;
		}

		private HiddenHeaderButton CreateHiddenHeaderButton (CGRect frame, nint tag)
		{
			var button = new HiddenHeaderButton (frame);
			button.Tag = tag;
			button.TouchUpInside += _headerButtonCommand;
			return button;
		}
	}

	public class HiddenHeaderButton : UIButton
	{
		public HiddenHeaderButton (CGRect frame) : base (frame)
		{

		}
	}
}
