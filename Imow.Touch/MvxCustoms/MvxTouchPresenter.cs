﻿using Imow.Core.Models.PresentationHints;
using Imow.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform.Platform;
using UIKit;

namespace Imow.Touch.MvxCustoms
{
    public class MvxTouchPresenter : MvxIosViewPresenter
    {
        private UIViewController _currentModalViewController;

        public MvxTouchPresenter(IMvxApplicationDelegate appDelegate, UIWindow window)
            : base(appDelegate, window)
        {
        }

        public override void ChangePresentation(MvxPresentationHint hint)
        {
            if (hint is MvxRootPresentationHint rootHint)
            {
                if (MasterNavigationController == null)
                {
                    return;
                }

                MasterNavigationController.PopToRootViewController(true);
                MasterNavigationController = null;

                if (_currentModalViewController != null)
                {
                    CloseModalViewController(_currentModalViewController);
                }

                CloseSplitViewController();
                CloseTabBarViewController();
                CloseMasterNavigationController();

                base.Show(new MvxViewModelRequest(rootHint.PresentationType, rootHint.Body, null));
            }
            else
            {
                base.ChangePresentation(hint);
            }
        }

        public override void NativeModalViewControllerDisappearedOnItsOwn()
        {
            if (_currentModalViewController != null)
            {
                MvxTrace.Error("How did a modal disappear when we didn't have one showing?");
                return;
            }

            // clear our local reference to avoid back confusion
            _currentModalViewController = null;
        }
    }
}
