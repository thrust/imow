using MvvmCross.Platform.Plugins;

namespace Imow.Touch.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.DownloadCache.PluginLoader, MvvmCross.Plugins.DownloadCache.iOS.Plugin>
    {
    }
}