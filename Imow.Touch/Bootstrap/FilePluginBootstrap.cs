using MvvmCross.Platform.Plugins;

namespace Imow.Touch.Bootstrap
{
    public class FilePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader, MvvmCross.Plugins.File.iOS.Plugin>
    {
    }
}