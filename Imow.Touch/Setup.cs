using Imow.Core.Services.DatabaseServices;
using Imow.Touch.MvxCustoms;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using UIKit;
using static Imow.Touch.Utils.TouchExtensionMethods;

namespace Imow.Touch
{
    public class Setup : MvxIosSetup
    {
        public Setup(IMvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            var dbConn = GetLocalFilePath("imow.db3");
            Mvx.RegisterSingleton<IDatabaseService>(() =>
            {
                return new DatabaseService(dbConn);
            });
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxIosViewPresenter CreatePresenter()
        {
            var touchPresenter = new MvxTouchPresenter(ApplicationDelegate, Window);
            Mvx.RegisterSingleton<IMvxIosViewPresenter>(touchPresenter);
            return touchPresenter;
        }
    }
}
