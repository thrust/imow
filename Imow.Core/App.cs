﻿using Imow.Core.Services.LoggerServices;
using Imow.Core.Services.TokenServices;
using Imow.Core.Services.WebServices.Handlers;
using Imow.Core.Utils;
using Imow.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using System.Net.Http;
using System.Threading.Tasks;
using Imow.Core.Services.DatabaseServices;

namespace Imow.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .ExcludeInterfaces(typeof(IDatabaseService)) // it is created in specific project Setup.cs>CreateApp()
                .RegisterAsLazySingleton();
            
			Mvx.RegisterType<ITokenService, LocalTokenService>();
			Mvx.RegisterType<ILoggerService, ConsoleLoggerService>();
            Mvx.RegisterType<HttpMessageHandler>(() => new ImowApiHttpMessageHandler(Mvx.Resolve<ITokenService>(), false));
			Mvx.RegisterType<ImowApiHttpMessageHandler>(() => new ImowApiHttpMessageHandler(Mvx.Resolve<ITokenService>(), true));

            //RegisterAppStart<TabViewModel>();
            RegisterNavigationServiceAppStart<TabViewModel>();
            Task.Factory.StartNew(async () => await SharedPush.CheckMatch(this).ConfigureAwait(false));

            return;
        }
    }
}
