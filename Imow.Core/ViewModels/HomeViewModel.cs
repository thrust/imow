﻿using Imow.Core.Models.Lists;
using Imow.Core.Models.Messages;
using Imow.Core.Services.FilterServices;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Imow.Core.ViewModels
{
    public class HomeViewModel : MvxViewModel
	{
		private readonly IMvxMessenger _messenger;
		private readonly IFilterService _filterService;

        private IEnumerable<FilterSection> _filters;
		public IEnumerable<FilterSection> Filters
        {
            get
            {
                return _filters;
            }
            set
            {
                _filters = value;
                RaisePropertyChanged(() => Filters);
            }
        }

        private string _currentBanner;
        private int _currentBannerIndex = 1;
        public string CurrentBanner
        {
            get
            {
                return _currentBanner;
            }
            set
            {
                SetProperty(ref _currentBanner, value);
            }
        }

		public HomeViewModel (IMvxMessenger messenger, IFilterService filterService)
		{
			_messenger = messenger;
            _filterService = filterService;

            Filters = Enumerable.Empty<FilterSection>();

            Task.Factory.StartNew(async () => await Init().ConfigureAwait(false));
		}

        public async Task Init()
        {
            var filters = await _filterService.GetFiltersAsync().ConfigureAwait(false);
            InvokeOnMainThread(() => Filters = filters);

            CurrentBanner = "ad.png";
            Task.Factory.StartNew(async () => await BannerRotationAsync().ConfigureAwait(false));
        }

		public IMvxCommand FilterCommand => new MvxCommand (Filter);
		private void Filter ()
		{
			var message = new FilterApartmentsMessage (this);
			_messenger?.Publish (message);
		}

		public IMvxCommand AddFilterCommand => new MvxCommand<FilterSection> (AddFilter);
		private void AddFilter (FilterSection filterSection)
		{
            _filterService.UpdateFilter(filterSection);
		}

        private async Task BannerRotationAsync()
        {
            while(true)
            {
                await Task.Delay(5000).ConfigureAwait(false);
                if (++_currentBannerIndex >= 3)
                {
                    _currentBannerIndex = 1;
                }

                var suffix = (_currentBannerIndex <= 1) ? string.Empty : _currentBannerIndex.ToString();
                InvokeOnMainThread(() => CurrentBanner = $"ad{suffix}.png");
            }
        }
    }
}
