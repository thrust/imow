﻿using System.Threading.Tasks;
using Imow.Core.Services.UserApiServices;
using MvvmCross.Core.ViewModels;
using Imow.Core.Models.PresentationHints;

namespace Imow.Core.ViewModels
{
    public class SettingsViewModel : MvxViewModel
	{
        private readonly IUserApiService _userApiService;

        public SettingsViewModel (IUserApiService userApiService)
        {
            _userApiService = userApiService;
        }

		private string _text = "Settings";
		public string Text 
        {
			get 
            { 
                return _text;
            }
			set 
            { 
                _text = value; 
                RaisePropertyChanged (() => Text); 
            }
		}

		//Commands
        public IMvxCommand LogoutCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await Logout().ConfigureAwait(false)).ConfigureAwait(false));
		private async Task Logout()
		{
            await _userApiService.LogoutUserAsync(() =>
			{
                InvokeOnMainThread(() => ChangePresentation(new MvxRootPresentationHint(typeof(LoginViewModel))));
			});
		}
	}
}
