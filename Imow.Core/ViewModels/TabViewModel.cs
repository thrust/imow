﻿using System;
using System.Linq;
using Imow.Core.Models;
using Imow.Core.Models.Messages;
using Imow.Core.Services.DatabaseServices;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;

namespace Imow.Core.ViewModels
{
    public class TabViewModel : MvxViewModel
	{
        private readonly MvxSubscriptionToken loginSubscription;
        private readonly IDatabaseService _databaseService;
        public Action<LoginMessage> SubscriptionTokenAction = null;

		private AccountViewModel _accountViewModel;
		public AccountViewModel AccountViewModel 
        {
			get 
            { 
                return _accountViewModel; 
            }
			set 
            {
                SetProperty(ref _accountViewModel, value);
            }
		}

		private SettingsViewModel _settingsViewModel;
		public SettingsViewModel SettingsViewModel 
        {
			get 
            { 
                return _settingsViewModel; 
            }
			set
            {
                SetProperty(ref _settingsViewModel, value);
            }
		}

        private MapViewModel _mapViewModel;
		public MapViewModel MapViewModel
        {
			get 
            { 
                return _mapViewModel; 
            }
			set 
            { 
                SetProperty(ref _mapViewModel, value);
            }
        }

        private LoginViewModel _loginViewModel;
        public LoginViewModel LoginViewModel
        {
            get
            {
                return _loginViewModel;
            }
            set
            {
                SetProperty(ref _loginViewModel, value);
            }
        }

		private HomeViewModel _homeViewModel;
		public HomeViewModel HomeViewModel
        {
			get 
            { 
                return _homeViewModel; 
            }
			set 
            { 
                SetProperty(ref _homeViewModel, value);
            }
		}

		private LeadsViewModel _leadsViewModel;
		public LeadsViewModel LeadsViewModel 
        {
			get 
            {
				return _leadsViewModel;
			}
			set 
            {
                SetProperty(ref _leadsViewModel, value);
			}
		}

		private User _currentUser;
		public User CurrentUser 
        {
			get 
            {
				return _currentUser;
			}
			set 
            {
                SetProperty(ref _currentUser, value);
			}
		}

		public TabViewModel (IDatabaseService databaseService, IMvxMessenger messenger)
		{
            //SettingsViewModel = Mvx.IocConstruct<SettingsViewModel> ();
            _databaseService = databaseService;
            loginSubscription = messenger.Subscribe<LoginMessage>(loginMessage =>
            {
                CurrentUser = databaseService.GetUser().ToList().FirstOrDefault();
                SubscriptionTokenAction?.Invoke(loginMessage);
            });

            InitTabs();
		}

        public void InitTabs()
        {
            CurrentUser = _databaseService.GetUser().ToList().FirstOrDefault();
            if (CurrentUser == null) //has been logout
            {
                LoginViewModel = Mvx.IocConstruct<LoginViewModel>();
                AccountViewModel = null;
            }
            else
            {
                LoginViewModel = null;
                AccountViewModel = Mvx.IocConstruct<AccountViewModel>();
            }

            if (CurrentUser?.IsAgent ?? false)
            {
                LeadsViewModel = Mvx.IocConstruct<LeadsViewModel>();
                HomeViewModel = null;
                MapViewModel = null;
            }
            else
            {
                LeadsViewModel = null;
                MapViewModel = Mvx.IocConstruct<MapViewModel>();
                HomeViewModel = Mvx.IocConstruct<HomeViewModel>();
            }
        }

        public void ClearTabs()
        {
            LoginViewModel = null;
            AccountViewModel = null;
            LeadsViewModel = null;
            HomeViewModel = null;
            MapViewModel = null;
            SettingsViewModel = null;
        }
	}
}
