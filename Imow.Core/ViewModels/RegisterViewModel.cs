﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Models.PresentationHints;
using Imow.Core.Services.UserApiServices;
using Imow.Core.Utils;
using MvvmCross.Core.ViewModels;
using Thrust.Plugins.Dialogs;

namespace Imow.Core.ViewModels
{
    public class RegisterViewModel : MvxViewModel
    {
        private readonly IUserApiService _userApiService;
        private readonly IDialogService _dialogService;

        public RegisterViewModel(IUserApiService userApiService, IDialogService dialogService)
        {
            _userApiService = userApiService;
            _dialogService = dialogService;
        }

		public List<UserType> UserTypes = new List<UserType>
		{
            Constants.ClientUserType, 
            Constants.CorretorUserType
		};

        //Properties
        private string _firstName;
        public string FirstName
        {
            get 
            { 
                return _firstName; 
            }
            set 
            { 
                SetProperty(ref _firstName, value); 
            }
        }

        private string _lastName;
        public string LastName
        {
            get 
            { 
                return _lastName; 
            }
            set 
            { 
                SetProperty(ref _lastName, value); 
            }
        }

        private string _email;
        public string Email
        {
            get 
            { 
                return _email; 
            }
            set 
            { 
                SetProperty(ref _email, value); 
            }
        }

        private string _password;
        public string Password
        {
            get 
            { 
                return _password; 
            }
            set 
            { 
                SetProperty(ref _password, value); 
            }
        }

        private string _repeatPassword;
        public string RepeatPassword
        {
            get 
            { 
                return _repeatPassword; 
            }
            set 
            { 
                SetProperty(ref _repeatPassword, value); 
            }
        }

        private string _phone;
        public string Phone
        {
            get 
            { 
                return _phone; 
            }
            set 
            { 
                SetProperty(ref _phone, value); 
            }
        }

		private bool _isAgent;
		public bool IsAgent 
        {
			get 
            { 
                return _isAgent; 
            }
			set 
            { 
                SetProperty (ref _isAgent, value); 
            }
		}

		//Commands
		public IMvxCommand DoneCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await DoneAsync().ConfigureAwait(false)).ConfigureAwait(false));
		private async Task DoneAsync()
        {
            var user = new AuthenticatedUser
            {
                FirstName =  FirstName,
                LastName = LastName,
                Email = Email,
                Password =  Password,
                Phone =  Phone,
                IsAgent = IsAgent
            };

			await _userApiService.RegisterUserAsync(user, async () =>
            {
                await _dialogService.Neutral(new AlertNeutralConfig
                {
                    Button = SharedStrings.Ok,
                    Title = "Success",
                    Message = "Welcome to Imow. Your account has been created, please login with your credentials.",
                }).ConfigureAwait(false);

                InvokeOnMainThread(() =>
				{
                    Close(this);
				});
            }).ConfigureAwait(false);
        }
    }
}
