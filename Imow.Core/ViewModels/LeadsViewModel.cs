﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Imow.Core.Models.Messages;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Thrust.Plugins.Dialogs;
using Thrust.Plugins.Utils;

namespace Imow.Core.ViewModels
{
    public class LeadsViewModel : MvxViewModel
	{
        private readonly ILeadApiService _leadApiService;
        private readonly IDialogService _dialogService;
        private readonly IUtilsService _utilsService;
        private MvxSubscriptionToken _subscription;

        private bool isInitializing = true;

		private MvxObservableCollection<Lead> _leads;
		public MvxObservableCollection<Lead> Leads 
		{
			get 
			{
				return _leads;
			}
			set 
			{
				_leads = value;
				RaisePropertyChanged (() => Leads);
			}
		}

        public LeadsViewModel (ILeadApiService leadApiService, IMvxMessenger messenger, IDialogService dialogService, IUtilsService utilsService)
        {
            _leadApiService = leadApiService;
            _dialogService = dialogService;
            _utilsService = utilsService;
            _subscription = messenger.Subscribe<LeadsMessage>(async message => await Task.Factory.StartNew(() => LeadsReceived(message)).ConfigureAwait(false));

            Task.Factory.StartNew(async () => await Init().ConfigureAwait(false));
        }

		public async Task Init ()
		{
			try 
			{
                isInitializing = true;
				var leads = await _leadApiService.GetLeadsAsync().ConfigureAwait (false);
                leads = leads?.Select(l =>
                {
                    l.MatchCommand = new MvxCommand(() => MatchLeadCommand.Execute(l));
                    l.RejectCommand = new MvxCommand(() => RejectLeadCommand.Execute(l));
                    return l;
                });
				InvokeOnMainThread (() => 
				{
					Leads = new MvxObservableCollection<Lead> (leads);
                    isInitializing = false;
				});
			} 
			catch (Exception e) 
			{
				var a = e;
                isInitializing = false;
			}
		}

		//Commands
        public IMvxCommand ShowDetailsCommand => new MvxCommand<Lead> (async lead => await Task.Factory.StartNew(async () => await ShowDetails(lead).ConfigureAwait(false)).ConfigureAwait(false));
		private async Task ShowDetails (Lead lead)
		{
            if(lead.Status != LeadStatus.Match)
            {
                return;
            }

            //await _dialogService.Neutral(new AlertNeutralConfig
            //{
            //    Button = SharedStrings.Ok,
            //    Title = "Lead Info",
            //    Message = $"Hey, you lead's name is: {lead.CustomerName} and his phone is {lead.CustomerPhone}",
            //});

            OpenConversation(lead.CustomerPhone, lead.ApartmentName);
        }

        public IMvxCommand MatchLeadCommand => new MvxCommand<Lead>(async lead => await Task.Factory.StartNew(async () => await MatchLead(lead).ConfigureAwait(false)).ConfigureAwait(false));
        private async Task MatchLead(Lead lead)
        {
            try
            {
                var customer = await _leadApiService.UpdateLeadAsync(lead).ConfigureAwait(false);
                if(customer != null)
                {
                    await _dialogService.Neutral(new AlertNeutralConfig
                    {
                        Button = SharedStrings.Ok,
                        Title = "Lead Info",
                        Message = $"Hey, you lead's name is: {lead.CustomerName} and his phone is {lead.CustomerPhone}",
                    });
                }
                else
                {
                    await _dialogService.Neutral(new AlertNeutralConfig
                    {
                        Button = SharedStrings.Ok,
                        Title = "Not fast enough",
                        Message = $"Sorry, you did not get a match this time. Good luck next time",
                    });
                    InvokeOnMainThread(() => Leads.Remove(lead));
                }
            }
            catch(Exception e)
            {
                await _dialogService.Neutral(new AlertNeutralConfig
                {
                    Button = SharedStrings.Ok,
                    Title = SharedStrings.Error,
                    Message = $"Error matching with customer. Try again later.",
                });
            }
        }

        public IMvxCommand RejectLeadCommand => new MvxCommand<Lead>(async lead => await Task.Factory.StartNew(async () => await RejectLead(lead).ConfigureAwait(false)).ConfigureAwait(false));
        private async Task RejectLead(Lead lead)
        {
            try
            {
                lead.Status = LeadStatus.Rejected;
                var customer = await _leadApiService.UpdateLeadAsync(lead).ConfigureAwait(false);
                InvokeOnMainThread(() => Leads.Remove(lead));
            }
            catch (Exception e)
            {
                await _dialogService.Neutral(new AlertNeutralConfig
                {
                    Button = SharedStrings.Ok,
                    Title = SharedStrings.Error,
                    Message = $"Error rejecting customer. Try again later.",
                });
            }
        }

        private void LeadsReceived(LeadsMessage message)
        {
            while(isInitializing)
            {
                Task.Delay(200).Wait();
            }

            var leads = message.Leads?.Where(l => Leads?.All(lead => lead.Id != l.Id) ?? true);
            leads = leads?.Select(l =>
            {
                l.MatchCommand = new MvxCommand(() => MatchLeadCommand.Execute(l));
                l.RejectCommand = new MvxCommand(() => RejectLeadCommand.Execute(l));
                return l;
            });
            if(leads?.Any() ?? false)
            {
                InvokeOnMainThread(() =>
                {
                    if (Leads != null)
                    {
                        Leads.AddRange(leads);
                    }
                    else
                    {
                        Leads = new MvxObservableCollection<Lead>(leads);
                    }
                });
            }
        }

        private void OpenConversation(string phoneNumber, string apartmentName)
        {
            _utilsService.SendWhatsapp(phoneNumber, $"Olá, tenho vi que tem interesse no apartamento {apartmentName})! Posso agendar uma visita para você conhecê-lo?");
        }
    }
}
