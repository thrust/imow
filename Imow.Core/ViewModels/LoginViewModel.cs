﻿using System.Threading.Tasks;
using Imow.Core.Models.Messages;
using Imow.Core.Models.PresentationHints;
using Imow.Core.Services.ApartmentApiServices;
using Imow.Core.Services.FilterServices;
using Imow.Core.Services.RequestServices;
using Imow.Core.Services.UserApiServices;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace Imow.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IUserApiService _userApiService;
		private readonly IApartmentApiService _apartmentApiService;
		private readonly IFilterService _filterService;
		private readonly IRequestService _requestService;
        private readonly IMvxMessenger _messenger;

        public LoginViewModel(IUserApiService userApiService, IApartmentApiService apartmentApiService, IFilterService filterService, IRequestService requestService, IMvxMessenger messenger)
        {
            _userApiService = userApiService;
			_apartmentApiService = apartmentApiService;
			_filterService = filterService;
			_requestService = requestService;
            _messenger = messenger;
        }

        //Properties
        private string _email;
        public string Email
        {
            get 
            { 
                return _email; 
            }
            set 
            { 
                SetProperty(ref _email, value); 
            }
        }

        private string _password;
        public string Password
        {
            get 
            { 
                return _password; 
            }
            set 
            { 
                SetProperty(ref _password, value); 
            }
        }
        
        //Commands
        public IMvxCommand RegisterCommand => new MvxCommand(Register);
        private void Register()
        {
            ShowViewModel<RegisterViewModel>();
        }

		public IMvxCommand LoginCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await LoginAsync().ConfigureAwait(false)).ConfigureAwait(false));
		private async Task LoginAsync()
        {
            await _userApiService.AuthenticateUserAsync(Email, Password, () =>
            {
				InvokeOnMainThread(() =>
				{
                    _messenger.Publish(new LoginMessage(true, this));
                });
            }).ConfigureAwait(false);
        }
    }
}