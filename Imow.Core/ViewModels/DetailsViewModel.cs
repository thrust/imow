﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Services.ApartmentApiServices;
using Imow.Core.Services.UserApiServices;
using Imow.Core.Utils;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using Thrust.Plugins.Utils;

namespace Imow.Core.ViewModels
{
    public class DetailsViewModel : MvxViewModel
	{
        private readonly IUserApiService _userApiService;
        private readonly IApartmentApiService _apartmentService;
        private readonly IUtilsService _utilsService;

        public DetailsViewModel(IApartmentApiService apartmentApiService, IUtilsService utilsService)
        {
            _apartmentService = apartmentApiService;
            _utilsService = utilsService;
        }

        private string _id;
        public string Id
        {
            get 
            { 
                return _id; 
            }
            set 
            { 
                SetProperty(ref _id, value);
            }
        }

        private string _name;
		public string Name 
        {
			get 
            { 
                return _name; 
            }
			set 
            { 
                SetProperty(ref _name, value);
            }
		}

		private string _description;
		public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                SetProperty(ref _description, value);
            }
        }

		private IEnumerable<string> _images;
        public IEnumerable<string> Images 
        {
			get 
            { 
                return _images; 
            }
			set 
            {
                SetProperty(ref _images, value);
            }
		}

        public string Icon
        {
            get
            {
                return Images?.FirstOrDefault();
            }
            set
            {
                //do nothing
            }
        }

        private string _rentOrBuy;
        public string RentOrBuy
        {
            get
            {
                return _rentOrBuy;
            }
            set
            {
                SetProperty(ref _rentOrBuy, value);
            }
        }

        private string _price;
        public string Price
        {
            get
            {
                return _price;
            }
            set
            {
                SetProperty(ref _price, value);
            }
        }

        private string _condominium;
        public string Condominium
        {
            get
            {
                return _condominium;
            }
            set
            {
                SetProperty(ref _condominium, value);
            }
        }

        private string _iptu;
        public string Iptu
        {
            get
            {
                return _iptu;
            }
            set
            {
                SetProperty(ref _iptu, value);
            }
        }

        private string _type;
        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                SetProperty(ref _type, value);
            }
        }

        private string _rooms;
        public string Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                SetProperty(ref _rooms, value);
            }
        }

        private string _suites;
        public string Suites
        {
            get
            {
                return _suites;
            }
            set
            {
                SetProperty(ref _suites, value);
            }
        }

        private string _garage;
        public string Garage
        {
            get
            {
                return _garage;
            }
            set
            {
                SetProperty(ref _garage, value);
            }
        }

        private string _area;
        public string Area
        {
            get
            {
                return _area;
            }
            set
            {
                SetProperty(ref _area, value);
            }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                SetProperty(ref _phoneNumber, value);
            }
        }

        private bool _isMatch;
        public bool IsMatch
        {
            get
            {
                return _isMatch;
            }
            set
            {
                SetProperty(ref _isMatch, value);
            }
        }

		public class Nav
		{
			public string Id { get; set; }
		}

        public DetailsViewModel(IUserApiService userApiService)
		{
            _userApiService = userApiService;
		}

		public async Task Init (Nav nav)
		{
            Id = nav.Id;

            var apartment = await _apartmentService.GetApartment(Id, CancellationToken.None).ConfigureAwait(false);
            var images = await _apartmentService.GetApartmentPhotos(Id, CancellationToken.None).ConfigureAwait(false);
            var resizedImages = new List<string>();

            foreach(var image in images ?? Enumerable.Empty<string>())
            {
                var imageUrlString = image;
                try
                {
                    var imageUrl = new Uri(imageUrlString);

                    var uriBuilder = new UriBuilder(imageUrl);
                    uriBuilder.Host += ".rsz.io";
                    uriBuilder.Scheme = "http";
                    var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                    query["width"] = "400";
                    uriBuilder.Query = query.ToString();
                    imageUrlString = uriBuilder.Uri.OriginalString;
                }
                catch (Exception e)
                {
                }
                resizedImages.Add(imageUrlString);
            }

            InvokeOnMainThread(() =>
            {
                Name = apartment.Name;
                Description = apartment.Description;
                RentOrBuy = apartment.Options.ToString();
                Price = apartment.Price.ToMonetaryString();
                Condominium = apartment.Condominium.ToMonetaryString();
                Iptu = apartment.Iptu.ToMonetaryString();
                Type = apartment.Type.ToString();
                Rooms = apartment.Rooms;
                Suites = apartment.Suites;
                Garage = apartment.Vacancies;
                Area = apartment.Area;
                PhoneNumber = apartment.AgentPhone;

                IsMatch = !string.IsNullOrEmpty(apartment.AgentPhone);

                Images = resizedImages;
            });
		}

        public IMvxCommand ChatWithAgentCommand => new MvxCommand(async() => await Task.Factory.StartNew(async () => await ChatWithAgentAsync().ConfigureAwait(false)).ConfigureAwait(false));
        private async Task ChatWithAgentAsync()
        {
            var phoneNumber = PhoneNumber.Replace("+", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
            _utilsService.SendWhatsapp(phoneNumber, $"Olá, tenho interesse no apartamento {Name}! Posso agendar uma visita para conhecê-lo?");
            //var agents = await _userApiService.GetAgentsNearbyAsync(Id).ConfigureAwait(false); //get list of agents that are NEARBY and maybe return phones?
        }

        public async Task<byte[]> GetImageBytesAsync(int index)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    return await httpClient.GetByteArrayAsync(Images?.ElementAt(index)).ConfigureAwait(false);
                }
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
