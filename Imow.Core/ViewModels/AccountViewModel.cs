﻿using System.Linq;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Services.DatabaseServices;
using Imow.Core.Services.UserApiServices;
using MvvmCross.Core.ViewModels;
using Imow.Core.Models.PresentationHints;
using Thrust.Plugins.Dialogs;
using MvvmCross.Plugins.Messenger;
using Imow.Core.Models.Messages;

namespace Imow.Core.ViewModels
{

    public class AccountViewModel : MvxViewModel
	{
		private readonly IUserApiService _userApiService;
		private readonly IDialogService _dialogService;
        private readonly IMvxMessenger _messenger;
		private AuthenticatedUser _currentUser;

		public AccountViewModel (IUserApiService userApiService, IDialogService dialogService, IMvxMessenger messenger)
		{
			_userApiService = userApiService;
            _dialogService = dialogService;
            _messenger = messenger;

            Task.Factory.StartNew(async () => await Init().ConfigureAwait(false));
		}

        public async Task Init()
        {
            _currentUser = await _userApiService.GetCurrentUserAsync().ConfigureAwait(false);
            if (_currentUser != null)
            {
                FirstName = _currentUser.FirstName;
                LastName = _currentUser.LastName;
                Email = _currentUser.Email;
                Phone = _currentUser.Phone;
            }
        }

		private string _text = "Account";
		public string Text 
        {
			get 
            { 
                return _text; 
            }
			set 
            { _text = value; 
                RaisePropertyChanged (() => Text); 
            }
		}

		//Properties
		private string _firstName;
		public string FirstName 
		{
			get 
			{
				return _firstName;
			}
			set 
			{
				SetProperty (ref _firstName, value);
			}
		}

		private string _lastName;
		public string LastName 
		{
			get 
			{
				return _lastName;
			}
			set 
			{
				SetProperty (ref _lastName, value);
			}
		}

		private string _email;
		public string Email 
		{
			get 
			{
				return _email;
			}
			set 
			{
				SetProperty (ref _email, value);
			}
		}

		private string _password;
		public string Password 
		{
			get 
			{
				return _password;
			}
			set 
			{
				SetProperty (ref _password, value);
			}
		}

		private string _repeatPassword;
		public string RepeatPassword 
		{
			get 
			{
				return _repeatPassword;
			}
			set 
			{
				SetProperty (ref _repeatPassword, value);
			}
		}

		private string _phone;
		public string Phone 
		{
			get 
			{
				return _phone;
			}
			set 
			{
				SetProperty (ref _phone, value);
			}
		}

		//Commands
		public IMvxCommand ConfirmCommand => new MvxCommand (async () => await Task.Factory.StartNew (async () => await ConfirmAsync ().ConfigureAwait (false)).ConfigureAwait (false));
		private async Task ConfirmAsync ()
		{
			var user = new AuthenticatedUser
            {
				Id = _currentUser.Id,
				FirstName = !string.IsNullOrEmpty (FirstName) ? FirstName : _currentUser.FirstName,
				LastName = !string.IsNullOrEmpty (LastName) ? LastName : _currentUser.LastName,
				Email = !string.IsNullOrEmpty (Email) ? Email : _currentUser.Email,
				Password = !string.IsNullOrEmpty (Password) ? Password : _currentUser.Password,
				Phone = !string.IsNullOrEmpty (Phone) ? Phone : _currentUser.Phone,
				IsAgent = _currentUser.IsAgent
			};

			await _userApiService.UpdateUserAsync (user, async() =>
            {
                await _dialogService.Neutral(new AlertNeutralConfig
                {
                    Title = "Success",
                    Button = SharedStrings.Ok,
                    Message = "Your user information has been updated!",
                }).ConfigureAwait(false);
            });
		}

		//public IMvxCommand CancelCommand => new MvxCommand (Cancel);
		//private void Cancel ()
		//{
		//	FirstName = _currentUser.FirstName;
		//	LastName = _currentUser.LastName;
		//	Email = _currentUser.Email;
		//	Phone = _currentUser.Phone;
  //      }

        public IMvxCommand LogoutCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await Logout().ConfigureAwait(false)).ConfigureAwait(false));
        private async Task Logout()
        {
            await _userApiService.LogoutUserAsync(() =>
            {
                InvokeOnMainThread(() =>
                {
                    _messenger.Publish(new LoginMessage(false, this));
                });
            }).ConfigureAwait(false);
        }
    }
}
