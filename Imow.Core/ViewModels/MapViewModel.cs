﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Models.Messages;
using Imow.Core.Services.ApartmentApiServices;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Imow.Core.Services.FilterServices;
using Thrust.Plugins.Dialogs;
using Imow.Core.Services.DatabaseServices;
using System.Threading;
using Newtonsoft.Json;
using Imow.Core.Models.Lists;
using Thrust.Plugins.Utils;

namespace Imow.Core.ViewModels
{
	public class ApartmentRange
	{
		public double Lat1 { get; set; }
		public double Lat2 { get; set; }
		public double Lng1 { get; set; }
		public double Lng2 { get; set; }

        public double CenterLat { get; set; }
        public double CenterLng { get; set; }
    }

    public class MapViewModel : MvxViewModel
    {
		private readonly MvxSubscriptionToken _token;
        private readonly IApartmentApiService _apartmentService;
        private readonly IDialogService _dialogService;
        private readonly IFilterService _filterService;
        private readonly IDatabaseService _databaseService;
        private readonly IUtilsService _utilsService;

        public IMvxCommand NeedToUpdateMarker { get; set; }

        private MvxObservableCollection<Apartment> _apartments;
		public MvxObservableCollection<Apartment> Apartments 
		{
			get 
            { 
                return _apartments; 
            }
			set 
            {
                SetProperty(ref _apartments, value);
                RaisePropertyChanged(() => FilteredApartments);
			}
		}

        private MvxObservableCollection<FilterSection> _selectedFilters;
        public MvxObservableCollection<FilterSection> SelectedFilters
        {
            get
            {
                return _selectedFilters;
            }
            set
            {
                SetProperty(ref _selectedFilters, value);
                RaisePropertyChanged(() => FilteredApartments);
            }
        }

		private MvxObservableCollection<Apartment> _filteredApartments;
		public MvxObservableCollection<Apartment> FilteredApartments 
        {
			get 
            {
                if(!Apartments?.Any() ?? true)
                {
                    return new MvxObservableCollection<Apartment>(Enumerable.Empty<Apartment>());
                }

                var filteredApartments = Apartments as IEnumerable<Apartment>;
                foreach (var selectedFilter in SelectedFilters)
                {
                    filteredApartments = filteredApartments.Where(a => a.Filter(selectedFilter));
                }
                return new MvxObservableCollection<Apartment>(filteredApartments);
                //return new MvxObservableCollection<Apartment>(filteredApartments.Aggregate(Enumerable.Empty<Apartment>(), (seed, next) =>
                //{
                //    if (seed == null)
                //    {
                //        seed = Enumerable.Empty<Apartment>();
                //    }

                //    if (!next?.Any() ?? true)
                //    {
                //        return seed;
                //    }

                //    var seedList = seed?.ToList();
                //    seedList?.AddRange(next);
                //    return seedList;
                //}));
            }
			set 
            { 
                RaisePropertyChanged (() => FilteredApartments); 
            }
		}

		private Apartment _selectedApartment;
		public Apartment SelectedApartment 
        {
			get 
            { 
                return _selectedApartment; 
            }
			set 
            {
                SetProperty(ref _selectedApartment, value);
            }
		}

		private ApartmentRange _range;
		public ApartmentRange Range 
        {
			get 
            {
				return _range;
			}
            set 
            {
                SetProperty(ref _range, value);
			}
		}

		public MapViewModel (IMvxMessenger messenger, IApartmentApiService apartmentApiService, IFilterService filterService, IDialogService dialogService, IDatabaseService databaseService, IUtilsService utilsService) 
		{
			_token = messenger.Subscribe<FilterApartmentsMessage>(async message => await Task.Factory.StartNew(async() => await OnFilterApartmentsMessage(message).ConfigureAwait(false)).ConfigureAwait(false));
			_apartmentService = apartmentApiService;
            _filterService = filterService;
            _dialogService = dialogService;
            _databaseService = databaseService;
            _utilsService = utilsService;

            Task.Factory.StartNew(async () => await Init().ConfigureAwait(false));

		}

        public async Task Init()
        {
            Apartments = new MvxObservableCollection<Apartment>();
            Range = new ApartmentRange
            {
                Lat1 = -23,
                Lat2 = -21,
                Lng1 = -44,
                Lng2 = -41,
                CenterLat = -22,
                CenterLng = -42,
            };

            await LoadApartments().ConfigureAwait(false);
        }

        public IMvxCommand LoadApartmentsCommand => new MvxCommand(async ()=> await Task.Factory.StartNew(async ()=> await LoadApartments().ConfigureAwait(false)).ConfigureAwait(false));
        private async Task LoadApartments()
        {
            try
            {
                if(Range != null)
                {
                    await LoadFilters().ConfigureAwait(false);
                    var apartments = await _apartmentService.GetApartmentsInRangeAsync(Range.Lat1, Range.Lat2, Range.Lng1, Range.Lng2).ConfigureAwait(false);

                    if (apartments != null)
                    {
                        InvokeOnMainThread(() =>
                        {
                            Apartments = new MvxObservableCollection<Apartment>(apartments);
                        });
                    }
                }
			}
            catch(Exception e)
            {
                var a = e;
            }
        }

		//Commands
        public IMvxCommand ShowDetailsCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await ShowDetails().ConfigureAwait(false)).ConfigureAwait(false));
        private async Task ShowDetails ()
		{
            ShowViewModel<DetailsViewModel>(new DetailsViewModel.Nav { Id = SelectedApartment.Id });
		}

        public IMvxCommand ContactBrokerCommand => new MvxCommand(async () => await Task.Factory.StartNew(async () => await ContactBrokerAsync().ConfigureAwait(false)).ConfigureAwait(false));
        private async Task ContactBrokerAsync()
        {
            User broker;

            var isLoggedIn = false;
            try
            {
                isLoggedIn = SharedSettings.AppSettings.GetValue<bool>(SharedSettings.Keys.IsLoggedIn.ToString(), false);   
            }
            catch(Exception) {}

            if(!isLoggedIn)
            {
                await _dialogService.Neutral(new AlertNeutralConfig
                {
                    Message = "You have to be logged in to contact a broker, log in at the tab",
                    Button = SharedStrings.Ok,
                    Title = SharedStrings.Warning,
                }).ConfigureAwait(false);
                return;
            }

            if(string.IsNullOrEmpty(_selectedApartment.AgentPhone))
            {
                broker = await _apartmentService.MatchBroker(_selectedApartment.Id).ConfigureAwait(false);

                if (broker != null)
                {
                    _selectedApartment.AgentName = $"{broker.FirstName} {broker.LastName}";
                    _selectedApartment.AgentPhone = broker.Phone;
                    _databaseService.Update(_selectedApartment);

                    if(NeedToUpdateMarker != null)
                    {
                        InvokeOnMainThread(() => NeedToUpdateMarker?.Execute(_selectedApartment.Id));
                    }
                }
            }
            else
            {
                broker = new User { Phone = _selectedApartment.AgentPhone, FirstName = _selectedApartment.AgentName }; 
            }
            await _dialogService.Neutral(new AlertNeutralConfig
            {
                Button = SharedStrings.Ok,
                Title = "Broker Match Result",
                Message = (broker == null) ? "Could not find an available broker this time. Try again later." : $"The broker's name and phone is {broker.FirstName} ({broker.Phone}). Will open whatsapp for the conversation.",
            });

            if(broker != null)
            {
                OpenConversation(_selectedApartment.AgentPhone.Replace("+", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""), _selectedApartment.Name);
            }
        }

        private void OpenConversation(string phoneNumber, string apartmentName)
        {
                _utilsService.SendWhatsapp(phoneNumber, $"Olá, tenho interesse no apartamento {apartmentName}! Posso agendar uma visita para conhecê-lo?");
        }

        public IMvxCommand GetApartmentsInRangeCommand => new MvxCommand (async () => await Task.Factory.StartNew (async () => await GetApartmentsInRangeAsync ().ConfigureAwait (false)).ConfigureAwait (false));
		private async Task GetApartmentsInRangeAsync ()
		{
			var apartments = await _apartmentService.GetApartmentsInRangeAsync (Range.Lat1, Range.Lat2, Range.Lng1, Range.Lat2).ConfigureAwait (false);
            InvokeOnMainThread(() => Apartments = new MvxObservableCollection<Apartment>(apartments));
		}

        private async Task LoadFilters()
        {
            var filters = await _filterService.GetFiltersAsync().ConfigureAwait(false);
            var selectedFilters = filters.Where(f => f.Any(c => c.IsSelected)).Select(f => new FilterSection(f.Where(c => c.IsSelected))
            {
                Title = f.Title,
            });
            InvokeOnMainThread(() => SelectedFilters = new MvxObservableCollection<FilterSection>(selectedFilters));
        }

		public async Task OnFilterApartmentsMessage (FilterApartmentsMessage message)
		{
            await LoadFilters().ConfigureAwait(false);
		}
    }
}
