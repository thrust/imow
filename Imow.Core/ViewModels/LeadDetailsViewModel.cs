﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;

namespace Imow.Core
{
    public class LeadDetailsViewModel : MvxViewModel
    {
        private readonly ILeadApiService _leadApiService;

        public LeadDetailsViewModel(ILeadApiService leadApiService)
        {
            _leadApiService = leadApiService;
        }

        private string _id;
        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                RaisePropertyChanged(() => Id);
            }
        }

        private string _clientName;
        public string ClientName
        {
            get
            {
                return _clientName;
            }
            set
            {
                _clientName = value;
                RaisePropertyChanged(() => ClientName);
            }
        }

        private string _apartmentChosen;
        public string ApartmentChosen
        {
            get
            {
                return _apartmentChosen;
            }
            set
            {
                _apartmentChosen = value;
                RaisePropertyChanged(() => ApartmentChosen);
            }
        }

        private DateTime _timeChosen;
        public DateTime TimeChosen
        {
            get
            {
                return _timeChosen;
            }
            set
            {
                _timeChosen = value;
                RaisePropertyChanged(() => TimeChosen);
            }
        }

        //private LeadClassification _classification;
        //public LeadClassification Classification {
        //get {
        //	return _classification;
        //}
        //set {
        //	_classification = value;
        //	RaisePropertyChanged (() => Classification);
        //}
    }

    public class Nav
    {
        public string Id { get; set; }
        public string ClientName { get; set; }
        public string ApartmentChosen { get; set; }
        public DateTime TimeChosen { get; set; }
        //public LeadClassification Classification { get; set; }
    }

    //public void Init (Nav nav)
    //{
    //Id = nav.Id;
    //ClientName = nav.ClientName;
    //ApartmentChosen = nav.ApartmentChosen;
    //TimeChosen = nav.TimeChosen;
    //Classification = nav.Classification;
    //}

    //Commands
    //public IMvxCommand ConfirmCommand => new MvxCommand (async () => await Task.Factory.StartNew (async () => await ConfirmAsync ().ConfigureAwait (false)).ConfigureAwait (false));
    //private async Task ConfirmAsync ()
    //{
    //	var lead = new Lead 
    //	{
    //		Id = Id,
    //		ClientName = ClientName,
    //		ApartmentChosen = ApartmentChosen,
    //		TimeChosen = TimeChosen,
    //		Classification = Classification
    //	};

    //	await _leadApiService.UpdateLeadAsync (lead);
    //}
}
