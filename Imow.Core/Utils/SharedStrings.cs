﻿namespace Imow.Core
{
    public static class SharedStrings
	{
		public static string Loading { get { return "Loading"; } }
		public static string Ok { get { return "Ok"; } }
		public static string Error { get { return "Error"; } }
		public static string TokenExpireMessage { get { return "Your token has expired. You have to login again."; } }
		public static string RegisterError { get { return "Error registering user. Try again later."; } }
		public static string UpdateError { get { return "Error updating user. Try again later."; } }
		public static string LoginError { get { return "Error logging in user. Check your email and password and try again later."; } }
        public static string Warning { get { return "Warning"; } }
	}
}
