﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Imow.Core.Models.Messages;
using Imow.Core.Services.ApartmentApiServices;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Imow.Core.Services.DatabaseServices;
using Microsoft.AppCenter.Analytics;
using Newtonsoft.Json;

namespace Imow.Core.Utils
{
    public static class SharedPush
    {
        // Called on iOS and Android to initialize OneSignal
        public static void Initialize()
        {
            //OneSignal.Current.SetLogLevel(LOG_LEVEL.VERBOSE, LOG_LEVEL.WARN);

            OneSignal.Current.StartInit("9f322cd9-cdce-47cd-bf7f-1357d59a427f").Settings(new Dictionary<string, bool>()
            {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, true }
            })
              .InFocusDisplaying(OSInFocusDisplayOption.Notification)
              .HandleNotificationOpened((result) =>
            {
                Analytics.TrackEvent("OneSignal.Current.HandleNotificationOpened", new Dictionary<string, string>
                    {
                    {"result" , JsonConvert.SerializeObject(result) },
                    });
                  Task.Factory.StartNew(async () => await CheckMatch(result).ConfigureAwait(false));
              })
              .HandleNotificationReceived((notification) =>
            {
                Analytics.TrackEvent("OneSignal.Current.HandleNotificationReceived", new Dictionary<string, string>
                    {
                        {"notification" , JsonConvert.SerializeObject(notification) },
                    });
                  Task.Factory.StartNew(async () => await CheckMatch(notification).ConfigureAwait(false));
              })
              .EndInit();

            OneSignal.Current.IdsAvailable((playerID, pushToken) =>
            {
                Analytics.TrackEvent("OneSignal.Current.IdsAvailable", new Dictionary<string, string>
                {
                    {"playerID" , playerID }, 
                    {"pushToken" , pushToken }
                });
            });
        }

        // Just for iOS.
        // No effect on Android, device auto registers without prompting.
        public static void RegisterIOS()
        {
            OneSignal.Current.RegisterForPushNotifications();
        }

        public static async Task CheckMatch(object sender)
        {
            var databaseService = Mvx.Resolve<IDatabaseService>();
            var currentUser = databaseService.GetUser().FirstOrDefault();

            if(currentUser.IsAgent)
            {
                var leads = await Mvx.Resolve<ILeadApiService>().GetLeadsAsync(true, false).ConfigureAwait(false);

                if (!leads?.Any() ?? true)
                {
                    return;
                }

                var messenger = Mvx.Resolve<IMvxMessenger>();
                messenger.Publish(new LeadsMessage(leads, sender));
            }
            else
            {
                var matches = await Mvx.Resolve<IApartmentApiService>().GetMatches(false).ConfigureAwait(false);

                if (!matches?.Any() ?? true)
                {
                    return;
                }

                var messenger = Mvx.Resolve<IMvxMessenger>();
                foreach (var match in matches)
                {
                    messenger.Publish(new MatchMessage(match.Key, match.Value.FirstName + match.Value.LastName, match.Value.Phone, sender));
                }
            }
        }
    }
}
