﻿using Imow.Core.Models;
using MvvmCross.Platform.UI;

namespace Imow.Core.Utils
{
    public static class Constants
	{
		public static MvxColor BlackColor = new MvxColor(0, 0, 0);
		public static MvxColor GreyColorCCC = new MvxColor(204, 204, 204);
        public static MvxColor GreenColor = new MvxColor(161, 205, 58); //#a1cd3a
        public static MvxColor BlueColor = new MvxColor(0, 173, 231); //#00ade7
        public static MvxColor RedColor = new MvxColor(213, 33, 57); //#d52139
        public static MvxColor OrangeColor = new MvxColor(240, 135, 30); //#f0871e
        public static MvxColor DarkGreenColor = new MvxColor(0, 100, 0); //#006400
        public static MvxColor DarkBlueColor = new MvxColor(0, 0, 139); //#00008B
        public static MvxColor DarkRedColor = new MvxColor(139, 0, 0); //#8B0000
        public static MvxColor DarkOrangeColor = new MvxColor(255, 140, 0); //#FF8C00

        public static UserType ClientUserType = new UserType { Name = "Cliente" };
		public static UserType CorretorUserType = new UserType { Name = "Corretor" };

		public const string AuthorizationSchema = "Bearer";

        //public static string ServerAddress = "http://fd963c99.ngrok.io";
        public static string ServerAddress = "https://imow.azurewebsites.net";
        //public static string ServerAddress = "https://ea1699e3.ngrok.io";
        //public static string ServerAddress = "http://10.0.3.2:5000"; //genymotion localhost
      //public static string ServerAddress = "http://localhost:60208";
        public static string ServerAddressApi = $"{ServerAddress}/api";

        public static double DefaultLatitude = -22.95506436713819;
        public static double DefaultLongitude = -43.19088287651539;
        public static double DefaultZoom = 14.1;
	}
}
