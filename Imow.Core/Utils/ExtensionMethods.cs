﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Imow.Core.Models.Lists;
using MvvmCross.Platform;
using MvvmCross.Platform.UI;
using static Imow.Core.Utils.Constants;

namespace Imow.Core.Utils
{
    public static class ExtensionMethods
    {
		public static MvxColor GetContrastColor(this MvxColor color)
		{
			return (0.213f * color.R + 0.715 * color.G + 0.072 * color.B) < (255.0f * 0.5f) ? MvxColors.White : MvxColors.Black;
		}

        public static MvxColor GetFilterMvxColor(this FilterChild filterChild, int index)
        {
            switch (index)
            {
                case 0:
                {
                    return (filterChild?.IsSelected ?? false) ? DarkGreenColor : GreenColor;
                }
                case 1:
                {
                    return (filterChild?.IsSelected ?? false) ? DarkRedColor : RedColor;
                }
                case 2:
                {
                    return (filterChild?.IsSelected ?? false) ? DarkBlueColor : BlueColor;
                }
                default:
                {
                    return (filterChild?.IsSelected ?? false) ? DarkOrangeColor : OrangeColor;
                }
            }
        }

        public static void PopulateWithParentProperties<T>(this T toBePopulated, T parent)
        {
            if (toBePopulated == null || parent == null)
            {
                return;
            }

            var properties = typeof(T).GetProperties().Where(p => !p.GetIndexParameters().Any());
            foreach (var property in properties.Where(p => p.GetSetMethod(nonPublic: true) != null))
            {
                property.SetValue(toBePopulated, property.GetValue(parent));
            }
        }

        public static int Count<T>(this IEnumerable<T> source)
        {
            ICollection<T> c = source as ICollection<T>;
            if (c != null)
                return c.Count;

            int result = 0;
            using (IEnumerator<T> enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                    result++;
            }
            return result;
        }

        public static string ToMonetaryString(this float moneyValue)
        {
            var numberFormat = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            numberFormat.CurrencySymbol = "R$";
            numberFormat.NumberGroupSeparator = ".";
            numberFormat.NumberDecimalSeparator = ",";
            numberFormat.CurrencyGroupSeparator = ".";
            numberFormat.CurrencyDecimalSeparator = ",";
            return string.Format(numberFormat, "{0:C}", moneyValue);
        }
    }
}
