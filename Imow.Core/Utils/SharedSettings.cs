﻿using System;
using MvvmCross.Platform;
using Thrust.Plugins.Settings;

namespace Imow.Core
{
	public static class SharedSettings
	{
		public static ISettingsService AppSettings => Mvx.Resolve<ISettingsService> ();

		public enum Keys
		{
			IsLoggedIn,
			LoginResult,
			Domain,
			UserName,
            CurrentTimestamp,
		}
    }
}