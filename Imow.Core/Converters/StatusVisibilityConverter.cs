﻿using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Visibility;

namespace Imow.Core.Converters
{
    public class StatusVisibilityConverter : MvxBaseVisibilityValueConverter<LeadStatus>
    {
        protected override MvxVisibility Convert(LeadStatus value, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value == LeadStatus.Pending) ? MvxVisibility.Visible : MvxVisibility.Collapsed;
        }
    }
}
