﻿namespace Imow.Core.Models
{
    public class UserType
	{
		public string Name { get; set; }
		public string Icon { get; set; }
	}

}
