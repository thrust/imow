﻿using Imow.Core.Utils;

namespace Imow.Core.Models
{
    public class AuthenticatedUser : User
    {
        public AuthenticatedUser()
        {
        }

        public AuthenticatedUser(User user = null, string token = null)
        {
            this.PopulateWithParentProperties(user);
            Token = token;
        }

        public string Token { get; set; }
    }
}
