﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Imow.Core.Models.Lists
{
    public class ExpandableTableModel<T> : List<T>
    {
        [JsonProperty("Title")]
        public string Title { get; set; }
        public ExpandableTableModel() : base() { }
        public ExpandableTableModel(IEnumerable<T> collection) : base(collection) { }
    }

    public class FilterSection : ExpandableTableModel<FilterChild>
    {
        public FilterSection() : base() { }
        public FilterSection(IEnumerable<FilterChild> collection) : base(collection) { }
    }

    public class FilterChild
    {
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("IsSelected")]
        public bool IsSelected { get; set; }
    }

    public class FilterDatabase
    {
        [PrimaryKey]
        public string Title { get; set; }
        public string Values { get; set; }
    }

    public static class DatabaseExtensions
    {
        public static FilterDatabase ToDatabase(this FilterSection section)
        {
            var childs = section as IEnumerable<FilterChild>;
            return new FilterDatabase
            {
                Title = section?.Title,
                Values = JsonConvert.SerializeObject(childs),
            };
        }

        public static FilterDatabase ToDatabase(this KeyValuePair<string, IEnumerable<string>> dictionary)
        {
            var childs = string.Empty;
            try
            {
                if (dictionary.Value?.Any() ?? false)
                {
                    childs = JsonConvert.SerializeObject(dictionary.Value.Select(v => new FilterChild { IsSelected = false, Title = v }));
                }

                return new FilterDatabase
                {
                    Title = dictionary.Key,
                    Values = childs,
                };
            }
            catch (Exception e)
            {
                return new FilterDatabase();
            }
        }

        public static FilterSection ToSection(this FilterDatabase filterDatabase)
        {
            IEnumerable<FilterChild> childs = null;
            try
            {
                if (!string.IsNullOrEmpty(filterDatabase?.Values))
                {
                    childs = JsonConvert.DeserializeObject<IEnumerable<FilterChild>>(filterDatabase?.Values);
                }

                var filterSection = (childs != null) ? new FilterSection(childs) : new FilterSection();
                filterSection.Title = filterDatabase?.Title;
                return filterSection;
            }
            catch(Exception e)
            {
                return new FilterSection();
            }
        }

        public static FilterSection ToSection(this KeyValuePair<string, IEnumerable<string>> dictionary)
        {
            IEnumerable<FilterChild> childs = null;
            try
            {
                if (dictionary.Value?.Any() ?? false)
                {
                    childs = dictionary.Value.Select(v => new FilterChild { IsSelected = false, Title = v });
                }

                var filterSection = (childs != null) ? new FilterSection(childs) : new FilterSection();
                filterSection.Title = dictionary.Key;
                return filterSection;
            }
            catch (Exception e)
            {
                return new FilterSection();
            }
        }
    }
}
