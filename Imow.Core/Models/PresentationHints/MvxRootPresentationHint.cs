﻿using System;
using MvvmCross.Core.ViewModels;

namespace Imow.Core.Models.PresentationHints
{
    public class MvxRootPresentationHint : MvxPresentationHint
    {
        public Type PresentationType { get; set; }
        public MvxRootPresentationHint(Type type, MvxBundle bundle = null) : base(bundle)
        {
            PresentationType = type;
        }
    }
}
