﻿using System;
using System.Linq;
using Imow.Core.Models.Lists;
using MvvmCross.Core.ViewModels;
using Newtonsoft.Json;
using SQLite;

namespace Imow.Core.Models
{
    public class Apartment : MvxNotifyPropertyChanged
    { 
        [PrimaryKey]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonIgnore]
        private string _name;
        [JsonProperty("name")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        [JsonIgnore]
        private float _price;
        [JsonProperty("price")]
        public float Price
        {
            get
            {
                return _price;
            }
            set
            {
                SetProperty(ref _price, value);
            }
        }

        [JsonIgnore]
        private float _condominium;
        [JsonProperty("condominium")]
        public float Condominium
        {
            get
            {
                return _condominium;
            }
            set
            {
                SetProperty(ref _condominium, value);
            }
        }

        [JsonIgnore]
        private float _iptu;
        [JsonProperty("iptu")]
        public float Iptu
        {
            get
            {
                return _iptu;
            }
            set
            {
                SetProperty(ref _iptu, value);
            }
        }

        [JsonIgnore]
        private string _description;
        [JsonProperty("description")]
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                SetProperty(ref _description, value);
            }
        }

        [JsonIgnore]
        private string _rooms;
        [JsonProperty("rooms")]
        public string Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                SetProperty(ref _rooms, value);
            }
        }

        [JsonIgnore]
        private string _suites;
        [JsonProperty("suites")]
        public string Suites
        {
            get
            {
                return _suites;
            }
            set
            {
                SetProperty(ref _suites, value);
            }
        }

        [JsonIgnore]
        private string _vacancies;
        [JsonProperty("vacancies")]
        public string Vacancies
        {
            get
            {
                return _vacancies;
            }
            set
            {
                SetProperty(ref _vacancies, value);
            }
        }

        [JsonIgnore]
        private string _area;
        [JsonProperty("area")]
        public string Area
        {
            get
            {
                return _area;
            }
            set
            {
                SetProperty(ref _area, value);
            }
        }

        [JsonIgnore]
        private string _address;
        [JsonProperty("address")]
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                SetProperty(ref _address, value);
            }
        }

        [JsonIgnore]
        private string _enterprise;
        [JsonProperty("enterprise")]
        public string Enterprise
        {
            get
            {
                return _enterprise;
            }
            set
            {
                SetProperty(ref _enterprise, value);
            }
        }

        [JsonIgnore]
        private string _icon;
        [JsonProperty("icon")]
        public string Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                SetProperty(ref _icon, value);
            }
        }

        //[JsonIgnore]
        //private List<string> _photos;
        //public List<string> Photos
        //{
        //    get
        //    {
        //        return _photos;
        //    }
        //    set
        //    {
        //        _photos = value;
        //        RaisePropertyChanged(() => Photos);
        //    }
        //}


        [JsonIgnore]
        private double _lat;
        [JsonProperty("lat")]
        public double Lat
        {
            get
            {
                return _lat;
            }
            set
            {
                SetProperty(ref _lat, value);
            }
        }

        [JsonIgnore]
        private double _lng;
        [JsonProperty("lng")]
        public double Lng
        {
            get
            {
                return _lng;
            }
            set
            {
                SetProperty(ref _lng, value);
            }
        }

        [JsonIgnore]
        private string _neighbourhood;
        [JsonProperty("neighbourhood")]
        public string Neighbourhood
        {
            get
            {
                return _neighbourhood;
            }
            set
            {
                SetProperty(ref _neighbourhood, value);
            }
        }

        [JsonIgnore]
        private AgentStatus _status;
        [JsonProperty("agentStatus")]
        public AgentStatus Status 
        {
            get
            {
                return _status;
            }
            set
            {
                SetProperty(ref _status, value);
            }
        }

        [JsonIgnore]
        private PropertyType _type;
        [JsonProperty("propertyType")]
        public PropertyType Type
        {
            get
            {
                return _type;
            }
            set
            {
                SetProperty(ref _type, value);
            }
        }

        [JsonIgnore]
        private HabitationOptions _options;
        [JsonProperty("habitationOptions")]
        public HabitationOptions Options
        {
            get
            {
                return _options;
            }
            set
            {
                SetProperty(ref _options, value);
            }
        }

        [JsonIgnore]
        private string _agentName;
        [JsonProperty("agentName")]
        public string AgentName
        {
            get
            {
                return _agentName;
            }
            set
            {
                SetProperty(ref _agentName, value);
            }
        }

        [JsonIgnore]
        private string _agentPhone;
        [JsonProperty("agentPhone")]
        public string AgentPhone
        {
            get
            {
                return _agentPhone;
            }
            set
            {
                SetProperty(ref _agentPhone, value);
            }
        }
        
        public bool Filter(FilterSection filterSection)
        {
            var isFiltered = false;
            var selectedFilters = filterSection.Where(c => c.IsSelected);
            if (filterSection.Title == "Alugar | Comprar")
            {
                var options = selectedFilters.Select(c =>
                {
                    return (c.Title == "Comprar") ? HabitationOptions.Buy : HabitationOptions.Rent;
                });

                isFiltered = isFiltered || options.Contains(Options);
            }
            else if (filterSection.Title == "Tipo")
            {
                var options = selectedFilters.Select(c =>
                {
                    PropertyType type;
                    return (Enum.TryParse(c.Title, out type)) ? type : (PropertyType)(-1);
                });

                isFiltered = isFiltered || options.Contains(Type);
            }
            else if (filterSection.Title == "Onde")
            {
                var options = selectedFilters.Select(c => c.Title);

                isFiltered = isFiltered || options.Contains(Neighbourhood);
            }

            return isFiltered;
        }
    }

    public enum AgentStatus
    {
        Close,
        Middle,
        Far
    }

    public enum PropertyType
    {
        Apartment,
        House,
        Penthouse,
        Flat,
        Terrain,
        Box,
        Store
    }

    public enum HabitationOptions
    {
        Rent,
        Buy
    }
}