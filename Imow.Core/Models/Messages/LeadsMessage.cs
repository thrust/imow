﻿using System.Collections.Generic;
using MvvmCross.Plugins.Messenger;

namespace Imow.Core.Models.Messages
{
    public class LeadsMessage : MvxMessage
    {
        public readonly IEnumerable<Lead> Leads;
        public LeadsMessage(IEnumerable<Lead> leads, object sender) : base(sender)
		{
            Leads = leads;
		}
	}
}
