﻿using MvvmCross.Plugins.Messenger;

namespace Imow.Core.Models.Messages
{
    public class MatchMessage : MvxMessage
    {
        public readonly string ApartmentId;
        public readonly User User;
		public MatchMessage(string apartmentId, string name, string phone, object sender) : base(sender)
		{
            ApartmentId = apartmentId;
            User = new User
            {
                FirstName = name,
                Phone = phone,
            };
		}
	}
}
