﻿using System.Collections.Generic;
using MvvmCross.Plugins.Messenger;

namespace Imow.Core.Models.Messages
{
    public class LoginMessage : MvxMessage
    {
        public readonly bool Login;
        public LoginMessage(bool login, object sender) : base(sender)
        {
            Login = login;
        }
    }
}
