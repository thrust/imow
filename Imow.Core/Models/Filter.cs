﻿using SQLite;

namespace Imow.Core
{
    public class Filter
	{
		[PrimaryKey]
        public string Name { get; set; }
		public string Values { get; set; }
	}
}
