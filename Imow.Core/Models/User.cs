﻿using System;
using SQLite;

namespace Imow.Core.Models
{
    public class User
    {
		[PrimaryKey]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public bool IsAgent { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DateTime? Timestamp { get; set; }
        public DateTime? MatchTimestamp { get; set; }
        public string MatchId { get; set; }
    }
}
