﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Imow.Core.Models;
using MvvmCross.Core.ViewModels;
using SQLite;

namespace Imow.Core
{
    public enum LeadStatus
    {
        Pending,
        Match,
        Rejected,
    }

    public class Lead : MvxNotifyPropertyChanged
	{
        [PrimaryKey]
        public string Id { get; set; }

        private string _customerId;
        public string CustomerId 
        { 
            get
            {
                return _customerId;
            }
            set
            {
                SetProperty(ref _customerId, value);
            }
        }

        private string _customerPhone;
        public string CustomerPhone 
        {
            get
            {
                return _customerPhone;
            }
            set
            {
                SetProperty(ref _customerPhone, value);
            }
        }

        private DateTime? _timestamp;
        public DateTime? Timestamp
        {
            get
            {
                return _timestamp;
            }
            set
            {
                SetProperty(ref _timestamp, value);
            }
        }

        private DateTime? _matchTimestamp;
        public DateTime? MatchTimestamp
        {
            get
            {
                return _matchTimestamp;
            }
            set
            {
                SetProperty(ref _matchTimestamp, value);
            }
        }

        private string _customerName;
        public string CustomerName
        {
            get
            {
                return _customerName;
            }
            set
            {
                SetProperty(ref _customerName, value);
            }
        }

        private string _apartmentId;
        public string ApartmentId
        {
            get
            {
                return _apartmentId;
            }
            set
            {
                SetProperty(ref _apartmentId, value);
            }
        }

        private string _apartmentName;
        public string ApartmentName
        {
            get
            {
                return _apartmentName;
            }
            set
            {
                SetProperty(ref _apartmentName, value);
            }
        }

        private LeadStatus _status;
        public LeadStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                SetProperty(ref _status, value);
            }
        }

        [Ignore]
        public ICommand MatchCommand { get; set; }
        [Ignore]
        public ICommand RejectCommand { get; set; }
	}

    public class LeadApi : Dictionary<string, IEnumerable<User>>
    {
    }

    public static class LeadExtensionMethods
    {
        public static IEnumerable<Lead> ExtractLeads(this LeadApi leadApis)
        {
            return leadApis.SelectMany(leadApi => leadApi.Value.Select(leadUser => 
            {
                var status = (string.IsNullOrEmpty(leadUser?.Phone)) ? LeadStatus.Pending : LeadStatus.Match;
                var lead = new Lead
                {
                    CustomerName = "Pending lead...",
                    CustomerId = leadUser?.Id,
                    ApartmentId = leadApi.Key,
                    Status = status,
                    Timestamp = leadUser?.Timestamp,
                    MatchTimestamp = leadUser?.MatchTimestamp,
                    Id = leadUser?.MatchId,
                };

                if(status == LeadStatus.Match)
                {
                    lead.CustomerName = $"{leadUser?.FirstName} {leadUser?.LastName}";
                    lead.CustomerPhone = leadUser?.Phone;
                }

                return lead;
            }));
        }
    }
}
