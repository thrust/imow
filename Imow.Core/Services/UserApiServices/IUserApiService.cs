﻿using Imow.Core.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace Imow.Core.Services.UserApiServices
{
    public interface IUserApiService
    {
		Task<bool> AuthenticateUserAsync(string email, string password, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
		Task<bool> LogoutUserAsync(Action onSuccess, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> RegisterUserAsync(AuthenticatedUser user, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
		Task<bool> UpdateUserAsync (AuthenticatedUser user, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken));
        Task<IEnumerable<User>> GetAgentsNearbyAsync(string apartmentId, CancellationToken cancellationToken = default(CancellationToken));
        Task<AuthenticatedUser> GetCurrentUserAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
