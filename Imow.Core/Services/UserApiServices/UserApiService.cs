﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Services.DatabaseServices;
using Imow.Core.Services.RequestServices;
using Imow.Core.Services.TokenServices;
using Imow.Core.Services.WebServices;
using Imow.Core.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Thrust.Plugins.Dialogs;
using System.Linq;
using Com.OneSignal;

namespace Imow.Core.Services.UserApiServices
{
    public class UserApiService : IUserApiService
	{
        private readonly IWebService _webService;
        private readonly ILeadApiService _leadService;
		private readonly ITokenService _tokenService;
		private readonly IDialogService _dialogService;
        private readonly IRequestService _requestService;
		private readonly IDatabaseService _databaseService;

		private enum MethodName
		{
			CheckLogin,
			RegisterUser,
            GetAgentsNearby,
			UpdateUser
		}

        public UserApiService(IWebService webService, ITokenService tokenService, IDialogService dialogService, IRequestService requestService, IDatabaseService databaseService, ILeadApiService leadService)
        {
			_webService = webService;
			_tokenService = tokenService;
			_dialogService = dialogService;
            _requestService = requestService;
			_databaseService = databaseService;
            _leadService = leadService;
        }

		public async Task<bool> AuthenticateUserAsync (string email, string password, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
		{
            try
			{
                var result = await  _requestService.RunAsync(Task.Run(async () =>
                {
                    try
                    {
                        var url = $"{Constants.ServerAddressApi}/User/{MethodName.CheckLogin.ToString()}";
                        var data = JObject.FromObject(new
                        {
                            email,
                            password,
                        });

                        var response = await _webService.PostAsync(url, data.ToString(), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                        if (!response.IsSuccessStatusCode)
                        {
                            return false;
                        }
                        var user = JsonConvert.DeserializeObject<AuthenticatedUser>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

                        OneSignal.Current.SendTag("ClientId", user.Id);
                        OneSignal.Current.SendTag("UserType", (user.IsAgent) ? "Broker" : "Customer");

                        if (user != null)
                        {
                            _databaseService.Clear<AuthenticatedUser>();
                            _databaseService.Insert(user);
                            await _tokenService.UpdateAuthenticationAsync(user).ConfigureAwait(false);

                            if(user.IsAgent)
                            {
                                await _leadService.GetLeadsAsync(false).ConfigureAwait(false);
                            }
                            return true;
                        }

                        return false;
                    }
					catch(Exception e)
                    {
                        return false;
                    }
				}, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);

                if(result)
                {
                    onSuccess?.Invoke();
                    return result;
                }

				await _dialogService.Neutral(new AlertNeutralConfig
				{
					Title = SharedStrings.Error,
					Button = SharedStrings.Ok,
					Message = SharedStrings.LoginError,
				}).ConfigureAwait(false);

                return result;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		public async Task<bool> RegisterUserAsync (AuthenticatedUser user, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
		{
            var result = await _requestService.RunAsync(Task.Run(async () =>
            {
                var url = $"{Constants.ServerAddressApi}/User/{MethodName.RegisterUser.ToString()}";
                var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);

                return response.IsSuccessStatusCode;
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
            
            if(result)
            {
                onSuccess?.Invoke();
                return result;
            }

            await _dialogService.Neutral(new AlertNeutralConfig
            {
                Title = SharedStrings.Error,
                Button = SharedStrings.Ok,
                Message = SharedStrings.RegisterError,
            }).ConfigureAwait(false);

            return result;
		}

        public async Task<IEnumerable<User>> GetAgentsNearbyAsync(string apartmentId, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var url = $"{Constants.ServerAddressApi}/api/User/{MethodName.GetAgentsNearby.ToString()}";
                var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { apartmentId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                return JsonConvert.DeserializeObject<List<User>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<bool> LogoutUserAsync(Action onSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var innerResult = await _tokenService.LogoutAsync().ConfigureAwait(false);

                    _databaseService.Clear<AuthenticatedUser>();
                    _databaseService.Clear<Apartment>();
                    _databaseService.Clear<Filter>();
                    _databaseService.Clear<Lead>();

                    OneSignal.Current.DeleteTag("ClientId");
                    OneSignal.Current.DeleteTag("UserType");

                    return innerResult;
                }
                catch (Exception e)
                {
                    return false;
                }
            }, cancellationToken), true, SharedStrings.Loading, false).ConfigureAwait(false);

            if (result)
            {
                onSuccess?.Invoke();
                return result;
            }

            await _dialogService.Neutral(new AlertNeutralConfig
            {
                Title = SharedStrings.Error,
                Button = SharedStrings.Ok,
                Message = SharedStrings.UpdateError,
            }).ConfigureAwait(false);

            return result;
        }

		public async Task<bool> UpdateUserAsync (AuthenticatedUser user, Action onSuccess, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken))
		{
            var result = await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var url = $"{Constants.ServerAddressApi}/User/{MethodName.UpdateUser.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                    
                    _databaseService.Update(user);

                    return response.IsSuccessStatusCode;
                }
                catch(Exception e)
                {
                    return false;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);

            
			if (result) 
            {
				onSuccess?.Invoke ();
				return result;
			}

			await _dialogService.Neutral (new AlertNeutralConfig {
				Title = SharedStrings.Error,
				Button = SharedStrings.Ok,
				Message = SharedStrings.UpdateError,
			}).ConfigureAwait (false);

			return result;
		}

        public async Task<AuthenticatedUser> GetCurrentUserAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                return await Task.FromResult(_databaseService.GetUser().ToList().FirstOrDefault()).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                return null;
            }
        }
	}
}
