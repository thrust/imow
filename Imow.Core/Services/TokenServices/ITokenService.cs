﻿using System.Threading.Tasks;
using Imow.Core.Models;

namespace Imow.Core.Services.TokenServices
{
    public interface ITokenService
    {
        Task<AuthenticatedUser> GetAuthenticationAsync();
		Task<bool> UpdateAuthenticationAsync(AuthenticatedUser authenticatedUser);
		Task<bool> LogoutAsync();
        Task<string> GetAuthorizationSchemaAsync();
    }
}
