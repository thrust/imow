using System;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Utils;
using Newtonsoft.Json;

namespace Imow.Core.Services.TokenServices
{
    public class LocalTokenService : ITokenService
    {
        public async Task<AuthenticatedUser> GetAuthenticationAsync()
        {
            var authenticatedUserString = SharedSettings.AppSettings.GetValue(SharedSettings.Keys.LoginResult.ToString(), string.Empty);
            return await Task.FromResult((string.IsNullOrEmpty(authenticatedUserString)) ? null : JsonConvert.DeserializeObject<AuthenticatedUser>(authenticatedUserString));
        }

		public async Task<bool> UpdateAuthenticationAsync(AuthenticatedUser authenticatedUser)
		{
            try
			{
				var serializedUser = JsonConvert.SerializeObject(authenticatedUser);
				SharedSettings.AppSettings.AddOrUpdate(SharedSettings.Keys.LoginResult.ToString(), serializedUser);
                SharedSettings.AppSettings.AddOrUpdate(SharedSettings.Keys.IsLoggedIn.ToString(), true);
				return await Task.FromResult(true);
            }
            catch(Exception e)
            {
                return false;
            }
		}

		public async Task<bool> LogoutAsync()
		{
			try
			{
                SharedSettings.AppSettings.Remove(SharedSettings.Keys.LoginResult.ToString());
                SharedSettings.AppSettings.Remove(SharedSettings.Keys.IsLoggedIn.ToString());
                SharedSettings.AppSettings.Remove(SharedSettings.Keys.CurrentTimestamp.ToString());
                SharedSettings.AppSettings.Remove(SharedSettings.Keys.UserName.ToString());
				return await Task.FromResult(true);
			}
			catch (Exception)
			{
				return false;
			}
		}

        public async Task<string> GetAuthorizationSchemaAsync()
        {
            return await Task.FromResult(Constants.AuthorizationSchema).ConfigureAwait(false);
        }
    }
}