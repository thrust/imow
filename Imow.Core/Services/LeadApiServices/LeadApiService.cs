﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Services.DatabaseServices;
using Imow.Core.Services.RequestServices;
using Imow.Core.Services.WebServices;
using Imow.Core.Utils;
using MvvmCross.Platform;
using MvvmCross.Platform.Core;
using Newtonsoft.Json;

namespace Imow.Core
{
	public class LeadApiService : ILeadApiService
	{
		private readonly IWebService _webService;
		private readonly IDatabaseService _databaseService;
		private readonly IRequestService _requestService;

        private static object LeadFetchingLock = new object();

		private enum MethodName
		{
            Get,
		    GetBrokerMatches,
			ConfirmAttendance
		}

		public LeadApiService (IWebService webService, IDatabaseService databaseService, IRequestService requestService)
		{
			_webService = webService;
			_databaseService = databaseService;
			_requestService = requestService;
		}

		public async Task<IEnumerable<Lead>> GetLeadsAsync (bool forceRefresh = false, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    IEnumerable<Lead> leads = null;
                    var taskCompletionSource = new TaskCompletionSource<IEnumerable<Lead>>();
                    lock(LeadFetchingLock)
                    {
                        leads = forceRefresh ? null :_databaseService.GetLeads();
                        if (!leads?.Any() ?? true)
                        {
                            var user = _databaseService.GetUser()?.FirstOrDefault();
                            var brokerId = user.Id;
                            var lastTimestamp = SharedSettings.AppSettings.GetValue(SharedSettings.Keys.CurrentTimestamp.ToString(), DateTime.MinValue);

                            var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetBrokerMatches.ToString()}";
                            var response = _webService.PostAsync(url, JsonConvert.SerializeObject(new { brokerId, lastTimestamp }), Encoding.UTF8, "application/json", cancellationToken).Result;

                            if (!response.IsSuccessStatusCode)
                            {
                                return null;
                            }

                            var responseString = response.Content.ReadAsStringAsync().Result;
                            var leadApis = JsonConvert.DeserializeObject<LeadApi>(responseString);

                            leads = leadApis.ExtractLeads();
                            leads = leads?.Select(l =>
                            {
                                var getUrl = $"{Constants.ServerAddressApi}/Apartment/{MethodName.Get.ToString()}";
                                var apartmentId = l.ApartmentId;
                                var getResponse = _webService.PostAsync(getUrl, JsonConvert.SerializeObject(new { apartmentId }), Encoding.UTF8, "application/json", cancellationToken).Result;
                                var apartment = JsonConvert.DeserializeObject<Apartment>(getResponse.Content.ReadAsStringAsync().Result);
                                l.ApartmentName = $"{apartment?.Name} ({apartment?.Description})";
                                return l;
                            }).ToList();

                            if(leads?.Any() ?? false)
                            {
                                var timestampResponse = leads.Max(l => l.Timestamp);
                                if (lastTimestamp <= timestampResponse)
                                {
                                    SharedSettings.AppSettings.AddOrUpdate(SharedSettings.Keys.CurrentTimestamp.ToString(), timestampResponse);
                                }
                            }

                            _databaseService.InsertAll(leads);
                        }
                        leads = leads?.OrderByDescending(l => l.MatchTimestamp)?.ThenBy(l => l.Timestamp);
                        taskCompletionSource.SetResult(leads);
                    }

                    return await taskCompletionSource.Task.ConfigureAwait(false);
                }
            catch (Exception e)
            {
                return null;
            }}, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
		}

		public async Task<User> UpdateLeadAsync (Lead lead, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    if(lead.Status == LeadStatus.Rejected)
                    {
                        _databaseService.Delete(lead);
                        return null;
                    }

                    var user = _databaseService.GetUser()?.FirstOrDefault();
                    var brokerId = user.Id;
                    var clientId = lead.CustomerId;
                    var apartmentId = lead.ApartmentId;

                    var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.ConfirmAttendance.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { clientId, apartmentId, brokerId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception();
                    }

                    var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    if(string.IsNullOrEmpty(responseString))
                    {
                        _databaseService.Delete(lead);
                        return null;
                    }
                    var customer = JsonConvert.DeserializeObject<AuthenticatedUser>(responseString);

                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() => 
                    {
                        lead.Status = LeadStatus.Match;
                        lead.CustomerPhone = customer.Phone;
                        lead.CustomerName = $"{customer.FirstName} {customer.LastName}";
                    });
                    _databaseService.Update(lead);

                    return customer;
                }
                catch (Exception e)
                {
                    throw;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
		}
	}
}
