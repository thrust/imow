﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models;

namespace Imow.Core
{
    public interface ILeadApiService
	{
        Task<IEnumerable<Lead>> GetLeadsAsync(bool forceRefresh = false, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> UpdateLeadAsync(Lead lead, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
	}
}
