﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models;
using Imow.Core.Services.DatabaseServices;
using Imow.Core.Services.RequestServices;
using Imow.Core.Services.WebServices;
using Imow.Core.Utils;
using Newtonsoft.Json;
using MvvmCross.Plugins.Messenger;
using Imow.Core.Models.Messages;

namespace Imow.Core.Services.ApartmentApiServices
{
    public class ApartmentApiService : IApartmentApiService
    {
		private readonly IWebService _webService;
		private readonly IDatabaseService _databaseService;
        private readonly IRequestService _requestService;
        private readonly IMvxMessenger _messenger;

        private enum MethodName
        {
			GetApartments,
			GetApartmentSearchOptions,
			GetApartmentsInRange,
            RequestMatchBroker,
            GetMatches,
            GetPhotos
        }

		public ApartmentApiService (IWebService webService, IDatabaseService databaseService, IRequestService requestService, IMvxMessenger messenger) 
		{
			_webService = webService;
			_databaseService = databaseService;
			_requestService = requestService;
            _messenger = messenger;

        }

        public async Task<IEnumerable<Apartment>> GetApartmentsAsync(bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
		{
			return await _requestService.RunAsync(Task.Run(async () =>
			{
                try
                {
					var apartments = _databaseService.GetApartments();

					if (!apartments?.Any() ?? true)
					{
						apartments = await GetApartmentsOnServerAsync(cancellationToken).ConfigureAwait(false);
					}

					return apartments;
                }
    			catch(Exception e)
                {
                    return null;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
        }

		public async Task<IEnumerable<Apartment>> GetApartmentsInRangeAsync (double lat1, double lat2, double lng1, double lng2, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var currentUser = _databaseService.GetUser()?.FirstOrDefault();
                    var clientId = currentUser?.Id;

                    var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetApartmentsInRange.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { lat1, lat2, lng1, lng2, clientId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);

                    if(!response.IsSuccessStatusCode)
                    {
                        return _databaseService.GetApartments().Where(a => a.Lat >= lat1 && a.Lat <= lat2 && a.Lng >= lng1 && a.Lng <= lng2);
                    }
                    var apartments = JsonConvert.DeserializeObject<List<Apartment>>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

                    _databaseService.Clear<Apartment>();
                    _databaseService.InsertAll(apartments);

                    return apartments;
                }
                catch (Exception e)
                {
                    return null;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
        }

        public async Task<User> MatchBroker(string apartmentId, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                MvxSubscriptionToken subscriptionToken = null;
                try
                {
                    var currentUser = _databaseService.GetUser().FirstOrDefault();
                    var clientId = currentUser.Id;

                    var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.RequestMatchBroker.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { clientId, apartmentId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                    
                    if(!response.IsSuccessStatusCode)
                    {
                        throw new Exception();
                    }

                    var taskCompletionSource = new TaskCompletionSource<User>();
                    subscriptionToken = _messenger.Subscribe<MatchMessage>(async matchMessage =>
                    {
                        if(matchMessage.ApartmentId == apartmentId)
                        {
                            taskCompletionSource?.TrySetResult(matchMessage.User);
                            subscriptionToken?.Dispose();
                        }
                    });

                    Task.Factory.StartNew(async () =>
                    {
                        await Task.Delay(30000);

                        if (taskCompletionSource.Task.Status == TaskStatus.Running || taskCompletionSource.Task.Status == TaskStatus.WaitingForActivation || taskCompletionSource.Task.Status == TaskStatus.WaitingForChildrenToComplete || taskCompletionSource.Task.Status == TaskStatus.WaitingToRun)
                        {
                            var matches = await GetMatches(false).ConfigureAwait(false);
                            if (matches?.ContainsKey(apartmentId) ?? false)
                            {
                                var match = matches.FirstOrDefault(m => m.Key == apartmentId);
                                taskCompletionSource?.TrySetResult(match.Value);
                                subscriptionToken?.Dispose();
                                return;
                            }
                        }
                        taskCompletionSource?.TrySetCanceled();
                        subscriptionToken?.Dispose();
                    });

                    return await taskCompletionSource.Task.ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    return null;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
        }
        public async Task<Dictionary<string, User>> GetMatches(bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var currentUser = _databaseService.GetUser().FirstOrDefault();
                    var clientId = currentUser.Id;

                    var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetMatches.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { clientId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                    return JsonConvert.DeserializeObject<Dictionary<string, User>>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                }
                catch(Exception e)
                {
                    return null;
                }
            }, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
        }

        public async Task<Dictionary<string, IEnumerable<string>>> GetApartmentSearchOptionsAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetApartmentSearchOptions.ToString()}";
            var response = await _webService.PostAsync(url, string.Empty, Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
			return JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<string>>> (await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        private async Task<IEnumerable<Apartment>> GetApartmentsOnServerAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
			var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetApartments.ToString()}";
			var response = await _webService.PostAsync(url, string.Empty, Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
			var apartments = JsonConvert.DeserializeObject<List<Apartment>>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
            _databaseService.InsertAll(apartments);
            return apartments;
        }

        public async Task<Apartment> GetApartment(string apartmentId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    return _databaseService.GetApartment(apartmentId);
                }
                catch (Exception e)
                {
                    return null;
                }
            }), true);

        }

        public async Task<List<string>> GetApartmentPhotos(string apartmentId, CancellationToken cancellationToken = default(CancellationToken))
        {

            return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var url = $"{Constants.ServerAddressApi}/Apartment/{MethodName.GetPhotos.ToString()}";
                    var response = await _webService.PostAsync(url, JsonConvert.SerializeObject(new { apartmentId }), Encoding.UTF8, "application/json", cancellationToken).ConfigureAwait(false);
                    return JsonConvert.DeserializeObject<List<string>>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                }
                catch (Exception e)
                {
                    return null;
                }
            }), true);
        }
    }
}
