﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models;

namespace Imow.Core.Services.ApartmentApiServices
{
    public interface IApartmentApiService
    {

        Task<IEnumerable<Apartment>> GetApartmentsInRangeAsync (double lat1, double lat2, double lng1, double lng2, bool showLoading = true, CancellationToken cancellationToken = default (CancellationToken));
        Task<IEnumerable<Apartment>> GetApartmentsAsync(bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
        Task<Apartment> GetApartment(string apartmentId, CancellationToken cancellationToken = default(CancellationToken));
        Task<List<string>> GetApartmentPhotos(string apartmentId, CancellationToken cancellationToken = default(CancellationToken));
        Task<Dictionary<string, IEnumerable<string>>> GetApartmentSearchOptionsAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<User> MatchBroker(string apartmentId, bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
        Task<Dictionary<string, User>> GetMatches(bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
    }
}
