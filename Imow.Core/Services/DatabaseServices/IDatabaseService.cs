﻿using System.Collections.Generic;
using Imow.Core.Models;
using Imow.Core.Models.Lists;

namespace Imow.Core.Services.DatabaseServices
{
    public interface IDatabaseService
    {
        void Clear<T>();

        Apartment GetApartment(string pk);
        IEnumerable<Apartment> GetApartments();
		void Insert(Apartment apartment);
		void InsertAll(IEnumerable<Apartment> items);
        void Update(Apartment apartment);
        void Delete(Apartment apartment);

        IEnumerable<Lead> GetLeads();
        void Insert(Lead lead);
        void InsertAll(IEnumerable<Lead> items);
        void Update(Lead lead);
        void Delete(Lead lead);

        IEnumerable<FilterDatabase> GetFilters ();
		void Insert(FilterDatabase filter);
        void Update(FilterDatabase filter);
        void InsertAll(IEnumerable<FilterDatabase> items);

		IEnumerable<AuthenticatedUser> GetUser ();
		void Insert (AuthenticatedUser user);
		void Update (AuthenticatedUser user);
    }
}
