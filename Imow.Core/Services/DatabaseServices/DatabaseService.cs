﻿using System;
using System.Collections.Generic;
using Imow.Core.Models;
using Imow.Core.Models.Lists;
using SQLite;

namespace Imow.Core.Services.DatabaseServices
{
    public class DatabaseService : IDatabaseService
    {
        internal readonly SQLiteConnection _connection;
        public static object InsertLock = new object();

        public DatabaseService(string databasePath)
        {
            _connection = new SQLiteConnection(databasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex);

            _connection.CreateTable<FilterDatabase>();
            _connection.CreateTable<Apartment>();
            _connection.CreateTable<AuthenticatedUser>();
            _connection.CreateTable<Lead>();
        }

        public void Clear<T>()
        {
            try
            {
                _connection.DropTable<T>();
                _connection.CreateTable<T>();
            }
            catch (Exception e) { }
        }

        public Apartment GetApartment(string pk)
        {
            try
            {
                return _connection.Get<Apartment>(pk);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<Apartment> GetApartments()
		{
			try 
			{
				return _connection.Table<Apartment> ().OrderBy (x => x.Id);
			} 
			catch (Exception e) 
			{
				return new List<Apartment>();
			}
        }

        public void Insert(Apartment apartment)
        {
            InsertOrUpdate(apartment.Id, apartment);
		}

		public void InsertAll(IEnumerable<Apartment> items)
		{
            foreach(var item in items)
            {
                Insert(item);
            }
        }

        public void Update(Apartment apartment)
        {
            _connection.Update(apartment);
        }

        public void Delete(Apartment apartment)
        {
            DeleteIfExists<Apartment>(apartment.Id);
        }

		public IEnumerable<FilterDatabase> GetFilters ()
		{
			return _connection.Table<FilterDatabase>();
		}

		public void Insert (FilterDatabase filter)
		{
            InsertOrUpdate(filter.Title, filter);
        }

        public void Update(FilterDatabase filter)
        {
            _connection.Update(filter);
        }

        public void InsertAll(IEnumerable<FilterDatabase> items)
		{
			foreach (var item in items)
			{
				Insert(item);
			}
		}

		public IEnumerable<AuthenticatedUser> GetUser ()
		{
			try 
			{
				return _connection.Table<AuthenticatedUser> ().OrderBy (x => x.Id);
			} 
			catch (Exception e) 
			{
				return new List<AuthenticatedUser> ();
			}
		}

		public void Insert (AuthenticatedUser user)
		{
            InsertOrUpdate(user.Id, user);
		}

        public void Update (AuthenticatedUser user)
		{
			_connection.Update (user);
		}

        public IEnumerable<Lead> GetLeads()
        {
            try
            {
                return _connection.Table<Lead>();
            }
            catch (Exception e)
            {
                return new List<Lead>();
            }
        }

        public void Insert(Lead lead)
        {
            InsertOrUpdate(lead.Id, lead);
        }

        public void InsertAll(IEnumerable<Lead> items)
        {
            foreach (var item in items)
            {
                Insert(item);
            }
        }

        public void Update(Lead lead)
        {
            _connection.Update(lead);
        }

        public void Delete(Lead lead)
        {
            DeleteIfExists<Lead>(lead.Id);
        }

        private void InsertOrUpdate<T>(object pk, T item) where T : new()
        {
            try
            {
                lock(InsertLock)
                {
                    if (_connection.Find<T>(pk) != null)
                    {
                        _connection.Update(item);
                    }
                    else
                    {
                        _connection.Insert(item);
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
        }

        private void DeleteIfExists<T>(object pk) where T : new()
        {
            if (_connection.Find<T>(pk) != null)
            {
                _connection.Delete<T>(pk);
            }
        }
	}
}
