﻿namespace Imow.Core.Services.LoggerServices
{
    public interface ILoggerService
    {
        void Log(string str);
    }
}
