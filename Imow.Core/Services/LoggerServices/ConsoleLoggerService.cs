﻿namespace Imow.Core.Services.LoggerServices
{
    public class ConsoleLoggerService : ILoggerService
    {
        public void Log(string str)
        {
            System.Diagnostics.Debug.WriteLine(str);
        }
    }
}