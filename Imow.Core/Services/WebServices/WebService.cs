﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Services.LoggerServices;
using Imow.Core.Services.WebServices.Handlers;
using MvvmCross.Platform;
using Newtonsoft.Json;

namespace Imow.Core.Services.WebServices
{
    public class WebService : IWebService
    {
        public static readonly char[] InvalidChars = "#%&*+/?:<>".ToCharArray();
        private bool _redirectResponse = false;
        public bool RedirectResponse
        {
            get
            {
                return _redirectResponse;
            }
            set
            {
                _redirectResponse = value;
            }
        }

		private readonly HttpClient _notRedirectHttpClient;
		private readonly HttpClient _redirectHttpClient;
        private readonly ILoggerService _logger;

        public WebService(HttpMessageHandler notRedirectHandler, ImowApiHttpMessageHandler redirectHandler, ILoggerService logger = null)
        {
			_notRedirectHttpClient = new HttpClient(notRedirectHandler);
			_redirectHttpClient = new HttpClient(redirectHandler);
            _logger = logger;
        }

        public HttpClient HttpClient => RedirectResponse ? _redirectHttpClient : _notRedirectHttpClient;

        public async Task<TReturn> GetAsync<TReturn>(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            var responseMessage = await GetAsync(url, cancellationToken, onRequestReady).ConfigureAwait(false);

            if (typeof(string).IsAssignableFrom(typeof (TReturn)))
            {
                _logger?.Log($"[WebService.GetAsync<>]: Return type is string");
                var stringResult = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                _logger?.Log($"[WebService.GetAsync<>]: Response as string {stringResult}");
                return (TReturn)(object)stringResult;
            }

            if (typeof(Stream).IsAssignableFrom(typeof(TReturn)))
            {
                _logger?.Log($"[WebService.GetAsync<>]: Return type is stream");
                var streamResult = await responseMessage.Content.ReadAsStreamAsync().ConfigureAwait(false);

                _logger?.Log($"[WebService.GetAsync<>]: Response as stream {streamResult}");
                return (TReturn)(object)streamResult;
            }

            if (typeof(byte[]).IsAssignableFrom(typeof(TReturn)))
            {
                _logger?.Log($"[WebService.GetAsync<>]: Return type is byte[]");
                var byteArrayResult = await responseMessage.Content.ReadAsByteArrayAsync().ConfigureAwait(false);

                _logger?.Log($"[WebService.GetAsync<>]: Response as byte[] {byteArrayResult}");
                return (TReturn)(object)byteArrayResult;
            }

            _logger?.Log($"[WebService.GetAsync<>]: Return type is {typeof(TReturn)}");
            var resultString = await responseMessage.Content.ReadAsStringAsync();

            _logger?.Log($"[WebService.GetAsync<>]: Response as string {resultString}");
            var result = JsonConvert.DeserializeObject<TReturn>(resultString);

            _logger?.Log($"[WebService.GetAsync<>]: Response as {typeof(TReturn)} {result}");
            return result;
        }

        public async Task<HttpResponseMessage> GetAsync(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.GetAsync]: Will create request for {url}");
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            _logger?.Log($"[WebService.GetAsync]: Will send {request}");
            var response = await SendAsync(request, cancellationToken, onRequestReady).ConfigureAwait(false);
            
            _logger?.Log($"[WebService.GetAsync]: Response returned {response}");
            return response;
        }
        
        public async Task<HttpResponseMessage> PostAsync<TValue>(string url, TValue value, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.PostAsync<>]: Will create stringData with value {value}");
            var stringData = JsonConvert.SerializeObject(value, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            });

            var response = await PostAsync(url, stringData, Encoding.UTF8, "application/json", cancellationToken, onRequestReady).ConfigureAwait(false);
            _logger?.Log($"[WebService.PostAsync<>]: Response returned {response}");

            return response;
        }

        public async Task<HttpResponseMessage> PostAsync(string url, string body, Encoding encoding, string mediaType, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.PostAsync]: Will create content with data {body}, encoding {encoding} and mediaType {mediaType}");
            var content = new StringContent(body, encoding, mediaType);

            _logger?.Log($"[WebService.PostAsync]: Will create request for {url}");
            var request = new HttpRequestMessage(HttpMethod.Post, url);

            _logger?.Log($"[WebService.PostAsync]: Will attach content to request {request}");
            request.Content = content;

            _logger?.Log($"[WebService.PostAsync]: Will send {request}");
            var response = await SendAsync(request, cancellationToken, onRequestReady).ConfigureAwait(false);

            _logger?.Log($"[WebService.PostAsync]: Response returned {response}");
            return response;
        }

        public async Task<HttpResponseMessage> UploadPostAsync(string url, KeyValuePair<string, string>[] headers, string fileName, byte[] fileBytes, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.UploadPostAsync]: Will create request for {url} with headers {string.Join(", ", headers)} and filename {fileName} and file size of {fileBytes.Length}");
            using (var content = new MultipartFormDataContent())
            {
                var fileContent = new ByteArrayContent(fileBytes);
                fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileName,
                };

                _logger?.Log($"[WebService.UploadPostAsync]: Will add file content {fileContent}");
                content.Add(fileContent);

                _logger?.Log($"[WebService.UploadPostAsync]: Will create request with content {content}");
                var request = new HttpRequestMessage(HttpMethod.Post, url)
                {
                    Content = content,
                };

                _logger?.Log($"[WebService.UploadPostAsync]: Will add headers {headers}");
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        _logger?.Log($"[WebService.UploadPostAsync]: Will add header {header}");
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                _logger?.Log($"[WebService.UploadPostAsync]: Will send {request}");
                var response = await SendAsync(request, cancellationToken, onRequestReady).ConfigureAwait(false);

                _logger?.Log($"[WebService.UploadPostAsync]: Response returned {response}");
                return response;
            }
        }

        public async Task<HttpResponseMessage> PutAsync<TValue>(string url, TValue value, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.PutAsync<>]: Will create stringData with value {value}");
            var stringData = JsonConvert.SerializeObject(value, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            });

            var response = await PutAsync(url, stringData, Encoding.UTF8, "application/json", cancellationToken, onRequestReady).ConfigureAwait(false);
            _logger?.Log($"[WebService.PostAsync<>]: Response returned {response}");

            return response;
        }

        public async Task<HttpResponseMessage> PutAsync(string url, string body, Encoding encoding, string mediaType, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.PutAsync]: Will create content with data {body}, encoding {encoding} and mediaType {mediaType}");
            var content = new StringContent(body, encoding, mediaType);

            _logger?.Log($"[WebService.PutAsync]: Will create request for {url}");
            var request = new HttpRequestMessage(HttpMethod.Put, url);

            _logger?.Log($"[WebService.PutAsync]: Will attach content to request {request}");
            request.Content = content;

            _logger?.Log($"[WebService.PutAsync]: Will send {request}");
            var response = await SendAsync(request, cancellationToken, onRequestReady).ConfigureAwait(false);

            _logger?.Log($"[WebService.PutAsync]: Response returned {response}");
            return response;
        }

        public async Task<HttpResponseMessage> DeleteAsync(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.PutAsync]: Will create request for {url}");
            var request = new HttpRequestMessage(HttpMethod.Delete, url);

            _logger?.Log($"[WebService.PutAsync]: Will send {request}");
            var response = await SendAsync(request, cancellationToken, onRequestReady).ConfigureAwait(false);

            _logger?.Log($"[WebService.PutAsync]: Response returned {response}");
            return response;
        }

        public void Dispose()
        {
			_redirectHttpClient?.Dispose();
            _notRedirectHttpClient?.Dispose();
        }

        #region Private methods

        private async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null)
        {
            _logger?.Log($"[WebService.SendAsync]: Will call requestReady for prepared request {request}");
            onRequestReady?.Invoke(request);

            _logger?.Log($"[WebService.SendAsync]: Will call {request.RequestUri}");
            var responseMessage = await HttpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);

            _logger?.Log($"[WebService.SendAsync]: Response returned is {responseMessage}");
            return responseMessage;
        }


        #endregion
    }
}
