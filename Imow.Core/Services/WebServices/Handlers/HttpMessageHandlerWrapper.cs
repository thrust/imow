using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Models.PresentationHints;
using Imow.Core.Services.TokenServices;
using Imow.Core.Services.UserApiServices;
using Imow.Core.ViewModels;
using MvvmCross.Core.Views;
using MvvmCross.Platform;
using Thrust.Plugins.Dialogs;
using ThrustHttpClient;

namespace Imow.Core.Services.WebServices.Handlers
{
    public class HttpMessageHandlerWrapper : IHttpMessageHandler
	{
	    private readonly HttpMessageInvoker _invoker;
	    public HttpMessageHandler InnerHandler { get; }

	    public HttpMessageHandlerWrapper(HttpMessageHandler innerHandler)
	    {
	        InnerHandler = innerHandler;
	        _invoker = new HttpMessageInvoker(innerHandler);
	    }


	    public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
	    {
	        HttpResponseMessage response;
	        var handler = InnerHandler as IHttpMessageHandler;
	        if (handler != null)
	        {
	            response = await handler.SendAsync(request, cancellationToken).ConfigureAwait(false);
	        }
	        else
	        {
	            response = await _invoker.SendAsync(request, cancellationToken).ConfigureAwait(false);
	        }
	        await response.Content.LoadIntoBufferAsync().ConfigureAwait(false);
	        return response;
	    }
	}

	public class ImowApiHttpMessageHandler : HttpMessageHandler
	{
		private readonly IHttpMessageHandler _innerHandler;

		public ImowApiHttpMessageHandler(ITokenService tokenProvider, bool allowRedirect)
		{
            var webService = Mvx.Resolve<BaseWebService>();
            var nativeHandler = webService.GetNativeHandler();
            nativeHandler.AllowAutoRedirect = allowRedirect;
            //var nativeHandler = new HttpClientHandler();
            _innerHandler = new TokenHandler(tokenProvider, new CultureHandler(new HttpMessageHandlerWrapper(nativeHandler)));
		}

		public IHttpMessageHandler InnerHandler
		{
			get { return _innerHandler; }
		}

		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			var result = await InnerHandler.SendAsync(request, cancellationToken);

            if(result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                await Mvx.Resolve<IDialogService>().Neutral(new AlertNeutralConfig
                {
                    Button = SharedStrings.Ok,
                    Message = SharedStrings.TokenExpireMessage,
                    Title = SharedStrings.Error,
                });
                await Mvx.Resolve<IUserApiService>().LogoutUserAsync(() =>
                {
                    Mvx.Resolve<IMvxViewDispatcher>().ChangePresentation(new MvxRootPresentationHint(typeof(LoginViewModel)));
                });
            }

            return result;
		}
	}
}