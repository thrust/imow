using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Imow.Core.Services.WebServices.Handlers
{
    public interface IHttpMessageHandler
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken);
    }
}