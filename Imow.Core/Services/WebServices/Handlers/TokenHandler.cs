﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Imow.Core.Services.TokenServices;
using Imow.Core.Utils;

namespace Imow.Core.Services.WebServices.Handlers
{
    public class TokenHandler : IHttpMessageHandler
    {
        private readonly ITokenService _tokenService;
        public IHttpMessageHandler InnerHandler { get; }

        public TokenHandler(ITokenService tokenService, IHttpMessageHandler innerHandler)
        {
            _tokenService = tokenService;
            InnerHandler = innerHandler;
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var authenticatedUser = await _tokenService.GetAuthenticationAsync().ConfigureAwait(false);
            var tokenSchema = await _tokenService.GetAuthorizationSchemaAsync().ConfigureAwait(false);

            if (!string.IsNullOrEmpty(authenticatedUser?.Token) && request?.Headers != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue(string.IsNullOrEmpty(tokenSchema) ? Constants.AuthorizationSchema : tokenSchema, authenticatedUser?.Token);
            }

            return await InnerHandler.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
    }
}
