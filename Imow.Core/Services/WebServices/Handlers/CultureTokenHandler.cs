using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Imow.Core.Services.WebServices.Handlers
{
    public class CultureHandler : IHttpMessageHandler
    {
        public IHttpMessageHandler InnerHandler { get; }
        internal const string DefaultLanguage = "pt";

        public CultureHandler(IHttpMessageHandler innerHandler)
        {
            InnerHandler = innerHandler;
        }

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request?.Headers?.Add(HttpRequestHeader.AcceptLanguage.ToString(), DefaultLanguage);
            return await InnerHandler.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
    }
}