﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Imow.Core.Services.WebServices
{
    public interface IWebService : IDisposable
    {
        bool RedirectResponse { get; set; }
        Task<HttpResponseMessage> GetAsync(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        Task<TReturn> GetAsync<TReturn>(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        
        Task<HttpResponseMessage> PostAsync(string url, string body, Encoding encoding, string mediaType, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        Task<HttpResponseMessage> PostAsync<TValue>(string url, TValue value, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        Task<HttpResponseMessage> UploadPostAsync(string url, KeyValuePair<string, string>[] headers, string fileName, byte[] fileBytes, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        
        Task<HttpResponseMessage> PutAsync(string url, string body, Encoding encoding, string mediaType, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
        Task<HttpResponseMessage> PutAsync<TValue>(string url, TValue value, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);

        Task<HttpResponseMessage> DeleteAsync(string url, CancellationToken cancellationToken, Action<HttpRequestMessage> onRequestReady = null);
    }
}
