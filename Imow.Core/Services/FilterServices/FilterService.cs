﻿using Imow.Core.Models.Lists;
using Imow.Core.Services.ApartmentApiServices;
using Imow.Core.Services.DatabaseServices;
using Imow.Core.Services.RequestServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Imow.Core.Services.FilterServices
{
    public class FilterService : IFilterService
	{
		private readonly IDatabaseService _databaseService;
		private readonly IApartmentApiService _apartmentService;
		private readonly IRequestService _requestService;

        public FilterService (IDatabaseService databaseService, IApartmentApiService apartmentService, IRequestService requestService)
		{
			_databaseService = databaseService;
			_apartmentService = apartmentService;
			_requestService = requestService;
		}

        public async Task<IEnumerable<FilterSection>> GetFiltersAsync (bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken))
		{
			return await _requestService.RunAsync(Task.Run(async () =>
            {
                try
                {
                    var filters = _databaseService.GetFilters();
                    
                    if (!filters?.Any() ?? false)
                    {
                        var dictonary = await _apartmentService.GetApartmentSearchOptionsAsync().ConfigureAwait(false);
                        filters = dictonary.Select(k => k.ToDatabase());
                        _databaseService.InsertAll(filters);
                    }

                    return filters.Select(f => f.ToSection());
                }
                catch(Exception e)
                {
                    return null;
                }
			}, cancellationToken), showLoading, showLoading ? SharedStrings.Loading : string.Empty, true).ConfigureAwait(false);
		}

        public bool UpdateFilter(FilterSection filterSection)
        {
            try
            {
                _databaseService.Update(filterSection.ToDatabase());
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
