﻿using Imow.Core.Models.Lists;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Imow.Core.Services.FilterServices
{
    public interface IFilterService
	{
		Task<IEnumerable<FilterSection>> GetFiltersAsync (bool showLoading = true, CancellationToken cancellationToken = default(CancellationToken));
        bool UpdateFilter(FilterSection filterSection);
    }
}
