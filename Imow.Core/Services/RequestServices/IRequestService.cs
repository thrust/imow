﻿using System;
using System.Threading.Tasks;

namespace Imow.Core.Services.RequestServices
{
	public interface IRequestService
	{
		Task<T> RunAsync<T> (Task<T> task, bool showLoading, string loadingText = null, bool checkInternet = true);
		Task RunAsync (Func<Task> task, bool showLoading, string loadingText = null, bool checkInternet = true);
	}
}
