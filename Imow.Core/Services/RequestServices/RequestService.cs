﻿using System;
using System.Threading.Tasks;
using Imow.Core.Models.Exceptions;
using Thrust.Plugins.Dialogs;
using Thrust.Plugins.Network;

namespace Imow.Core.Services.RequestServices
{
	public class RequestService : IRequestService
	{
		private readonly INetworkService _networkService;
		private readonly IDialogService _dialogService;

		public RequestService (INetworkService networkService, IDialogService dialogService)
		{
			_networkService = networkService;
			_dialogService = dialogService;
		}

		public async Task<T> RunAsync<T> (Task<T> task, bool showLoading, string loadingText = null, bool checkInternet = true)
		{
			if (checkInternet && !_networkService.HasInternetConexion ()) {
				throw new NoInternetException ();
			}

			if (showLoading) {
				using (_dialogService.Load (new LoadingConfig {
					Title = loadingText ?? SharedStrings.Loading,
				})) {
					return await task.ConfigureAwait (false);
				}
			}
			return await task.ConfigureAwait (false);
		}

		public async Task RunAsync (Func<Task> task, bool showLoading, string loadingText = null, bool checkInternet = true)
		{
			if (checkInternet && !_networkService.HasInternetConexion ()) {
				throw new NoInternetException ();
			}

			if (showLoading) {
				using (_dialogService.Load (new LoadingConfig {
					Title = loadingText ?? SharedStrings.Loading,
				})) {
					await task ().ConfigureAwait (false);
					return;
				}
			}
			await task ().ConfigureAwait (false);
		}
	}
}
