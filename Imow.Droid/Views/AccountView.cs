﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using Imow.Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Views.Fragments;

namespace Imow.Droid.Views
{
    [Register("imow/droid/views/AccountView")]
    public class AccountView : MvxFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            return this.BindingInflate(Resource.Layout.AccountView, null);
        }

		public override void OnResume()
		{
            base.OnResume();
            (ViewModel as AccountViewModel).Init().Wait();
		}
	}
}