﻿using System.Collections.Generic;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Imow.Core;
using Imow.Core.ViewModels;
using Imow.Droid.MvxCustoms;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Views.Fragments;

namespace Imow.Droid.Views
{
    [Register("imow/droid/views/LeadsView")]
    public class LeadsView : MvxFragment
    {
        public new LeadsViewModel ViewModel => base.ViewModel as LeadsViewModel;

        private MvxListView _listView;
        private LeadsListAdapter _adapter;

        public IEnumerable<Lead> Leads
        {
            get
            {
                return ViewModel.Leads;
            }
            set
            {
                _adapter?.NotifyChange();
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(Resource.Layout.LeadsView, null);
            _listView = view.FindViewById<MvxListView>(Resource.Id.LeadsListView);

            _adapter = new LeadsListAdapter(ViewModel, Activity, (MvxAndroidBindingContext) BindingContext);
            _listView.Adapter = _adapter;
            _listView.ItemClick = ViewModel.ShowDetailsCommand;

            var set = this.CreateBindingSet<LeadsView, LeadsViewModel>();
            set.Bind(this).For(t => t.Leads).For(vm => vm.Leads);
            set.Apply();

            return view;
        }

		public override void OnResume()
		{
            base.OnResume();
		}
	}
}