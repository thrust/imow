﻿using System;

using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace Imow.Droid.Views
{
    //    Spinner spinner = (Spinner)findViewById(R.id.spinner);
    //    // Create an ArrayAdapter using the string array and a default spinner layout
    //    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    //            R.array.planets_array, android.R.layout.simple_spinner_item);
    //    // Specify the layout to use when the list of choices appears
    //    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    //// Apply the adapter to the spinner
    //spinner.setAdapter(adapter);

    [Register("imow/droid/views/LeadDetailsView")]
    [Activity(Label = "View for LeadDetailsViewModel")]
    public class LeadDetailsView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.LeadDetailsView);
            
            var spinner = FindViewById<Spinner>(Resource.Id.classification_spinner);
            string[] data = { "Quente", "Médio", "Frio" };

            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.classification_array, Resource.Layout.simple_spinner_item);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);//(Resource.Layout.simple_spinner_dropdown_item);
            spinner.Adapter = adapter;

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>((sender, e) =>
            {
                var a = ((Spinner)sender).GetItemAtPosition(e.Position);
            });
        }
    }
}