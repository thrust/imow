﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Threading.Tasks;
using Android.Gms.Common;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Imow.Core.Models;
using Imow.Core.Utils;
using Imow.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Views.Fragments;
using MvvmCross.Platform;
using MvvmCross.Platform.Core;
using static Android.Gms.Maps.GoogleMap;

namespace Imow.Droid.Views
{
    [Register("imow/droid/views/MapView")]
    public class MapView : MvxFragment, IOnMapReadyCallback
    {
        private new MapViewModel ViewModel => base.ViewModel as MapViewModel;

        private MvxObservableCollection<Apartment> _filteredApartments;
        public MvxObservableCollection<Apartment> FilteredApartments
        {
            get
            {
                return _filteredApartments;
            }
            set
            {
                if (value == null || _map == null)
                {
                    return;
                }

                _filteredApartments = value;

                if (ApartmentMarkers?.Any() ?? false)
                {
                    foreach (var marker in ApartmentMarkers)
                    {
                        marker.Value.Remove();
                    }
                }

                _map.MarkerClick -= _map_MarkerClick;

                var boundsBuilder = new LatLngBounds.Builder();
                ApartmentMarkers = new Dictionary<string, Marker>(value.Select(ap =>
                {
                    Marker marker = null;

                    var taskCompletionSource = new TaskCompletionSource<KeyValuePair<string, Marker>>();
                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                    {
                        if (string.IsNullOrEmpty(ap?.AgentPhone))
                        {
                            switch (ap.Status)
                            {
                                case AgentStatus.Close:
                                    {
                                        marker = _map.AddMarker(new MarkerOptions().SetPosition(new LatLng(ap.Lat, ap.Lng)).SetTitle(ap.Name)
                                            .SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueGreen)));
                                        break;
                                    }
                                case AgentStatus.Middle:
                                    {
                                        marker = _map.AddMarker(new MarkerOptions().SetPosition(new LatLng(ap.Lat, ap.Lng)).SetTitle(ap.Name)
                                                .SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueYellow)));
                                        break;
                                    }
                                default:
                                case AgentStatus.Far:
                                    {
                                        marker = _map.AddMarker(new MarkerOptions().SetPosition(new LatLng(ap.Lat, ap.Lng)).SetTitle(ap.Name)
                                                .SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueRed)));
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            marker = _map.AddMarker(new MarkerOptions().SetPosition(new LatLng(ap.Lat, ap.Lng)).SetTitle(ap.Name)
                                         .SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueBlue)));
                        }

                        boundsBuilder.Include(marker.Position);
                        taskCompletionSource.SetResult(new KeyValuePair<string, Marker>(ap.Id, marker));
                    });

                    return taskCompletionSource.Task.Result;
                }));

                _map.MarkerClick += _map_MarkerClick;

                //if (ApartmentMarkers?.Any() ?? false)
                //{
                //    var bounds = boundsBuilder.Build();
                //    if (bounds != null)
                //    {
                //        Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                //        {
                //            _map.AnimateCamera(CameraUpdateFactory.NewLatLngBounds(bounds, 0));
                //        });
                //    }
                //}
            }
        }

        public Dictionary<string, Marker> ApartmentMarkers;
        private Android.Gms.Maps.MapView _mapView;
        private GoogleMap _map;
        private CameraPosition cameraPosition;

        private LinearLayout _selectedApartmentView;
        private TextView _selectedApartmentTitle;
        private MvxImageView _selectedApartmentImage;
        private ImageButton _selectedApartmentButton;
        private ImageButton _selectedApartmentPhotosButton;
        private TextView _selectedApartmentDescription;
        private TextView _selectedApartmentPrice;
        private TextView _selectedApartmentIptu;
        private TextView _selectedApartmentCondominium;
        private TextView _selectedApartmentRoom;
        private TextView _selectedApartmentSuite;
        private TextView _selectedApartmentGarage;
        private TextView _selectedApartmentArea;

        public override void OnSaveInstanceState(Bundle outState)
        {
            try
            {
                _mapView?.OnSaveInstanceState(outState);
            }
            catch(Exception)
            {
                
            }
            base.OnSaveInstanceState(outState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.MapView, null);
            _mapView = view.FindViewById<Android.Gms.Maps.MapView>(Resource.Id.mapView);

            _selectedApartmentView = view.FindViewById<LinearLayout>(Resource.Id.apartmentDetailView);
            _selectedApartmentTitle = view.FindViewById<TextView>(Resource.Id.apartmentDetailTitle);
            _selectedApartmentImage = view.FindViewById<MvxImageView>(Resource.Id.apartmentDetailImage);
            _selectedApartmentDescription = view.FindViewById<TextView>(Resource.Id.apartmentDetailDescription);
            _selectedApartmentPrice = view.FindViewById<TextView>(Resource.Id.apartmentDetailPrice);
            _selectedApartmentIptu = view.FindViewById<TextView>(Resource.Id.apartmentDetailIptu);
            _selectedApartmentCondominium = view.FindViewById<TextView>(Resource.Id.apartmentDetailCondominium);
            _selectedApartmentRoom = view.FindViewById<TextView>(Resource.Id.apartmentRoom);
            _selectedApartmentSuite = view.FindViewById<TextView>(Resource.Id.apartmentSuites);
            _selectedApartmentGarage = view.FindViewById<TextView>(Resource.Id.apartmentGarage);
            _selectedApartmentArea = view.FindViewById<TextView>(Resource.Id.apartmentArea);

            _selectedApartmentImage.DefaultImagePath = "res:loading1";

            _selectedApartmentButton = view.FindViewById<ImageButton>(Resource.Id.apartmentDetailButton);
            _selectedApartmentPhotosButton = view.FindViewById<ImageButton>(Resource.Id.apartmentPhotosButton);
            _selectedApartmentButton.Click += _selectedApartmentButton_Click;
            _selectedApartmentPhotosButton.Click += _selectedApartmentPhotosButton_Click;
            _selectedApartmentView.Visibility = ViewStates.Gone;

            ViewModel.NeedToUpdateMarker = new MvxCommand<string>(apartmentId =>
            {
                if (ApartmentMarkers?.ContainsKey(apartmentId) ?? false)
                {
                    ApartmentMarkers[apartmentId].SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueBlue));
                }
            });

            var set = this.CreateBindingSet<MapView, MapViewModel>();
            set.Bind(this).For(t => t.FilteredApartments).To(vm => vm.FilteredApartments);
            set.Apply();

            return view;
        }

        private void _selectedApartmentButton_Click(object sender, System.EventArgs e)
        {
            ViewModel.ContactBrokerCommand.Execute(null);
        }
        private void _selectedApartmentPhotosButton_Click(object sender, System.EventArgs e)
        {
            ViewModel.ShowDetailsCommand.Execute();
        }

        public override void OnActivityCreated(Bundle p0)
        {
            base.OnActivityCreated(p0);
            _mapView?.OnCreate(p0);
            SetUpMapIfNeeded();
        }

        public override void OnPause()
        {
            _mapView?.OnPause();
            base.OnPause();

            cameraPosition = _map.CameraPosition;
            _map = null;
        }

        public override void OnResume()
        {
            base.OnResume();
            SetUpMapIfNeeded();
            _mapView?.OnResume();
            //FilteredApartments = ViewModel.FilteredApartments;\
        }

        public override void OnDestroyView()
        {
            _mapView?.OnDestroy();
            base.OnDestroyView();
        }

        public override void OnLowMemory()
        {
            _mapView?.OnLowMemory();
            base.OnLowMemory();
        }

        private void SetUpMapIfNeeded()
        {
            if (_map == null)
            {
                View.FindViewById<Android.Gms.Maps.MapView>(Resource.Id.mapView).GetMapAsync(this);
            }
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            _map = googleMap;

            try
            {
                MapsInitializer.Initialize(Activity);
            }
            catch (GooglePlayServicesNotAvailableException e)
            {
            }

            if (cameraPosition != null)
            {
                _map?.MoveCamera(CameraUpdateFactory.NewCameraPosition(cameraPosition));
                cameraPosition = null;
            }
            else
            {
                _map?.MoveCamera(CameraUpdateFactory.NewCameraPosition(CameraPosition.FromLatLngZoom(new LatLng(Constants.DefaultLatitude, Constants.DefaultLongitude), 14.2f)));
            }

            //_map.MyLocationEnabled = true;
            _map.CameraChange += CameraChange;
        }

        private void CameraChange(object sender, CameraChangeEventArgs e)
        {
            GetBoundaries();
        }

        private void _map_MarkerClick(object sender, MarkerClickEventArgs e)
        {
            var marker = e.Marker;
            var apartment = FilteredApartments.FirstOrDefault(a => a.Id == ApartmentMarkers?.FirstOrDefault(am => am.Value?.Id == marker?.Id).Key);
            if (apartment != null)
            {
                if (ViewModel?.SelectedApartment?.Id == apartment.Id)
                {
                    ViewModel.SelectedApartment = null;
                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                    {
                        _selectedApartmentView.Visibility = ViewStates.Gone;
                    });
                }
                else
                {
                    ViewModel.SelectedApartment = apartment;

                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                    {
                        _selectedApartmentView.Visibility = ViewStates.Visible;
                        _selectedApartmentTitle.Text = apartment.Name;
                        _selectedApartmentImage.ImageUrl = apartment.Icon;
                        _selectedApartmentDescription.Text = apartment.Description;
                        _selectedApartmentPrice.Text = $"R$ {apartment.Price.ToString("C")}";
                        _selectedApartmentIptu.Text = $"IPTU: R$ {apartment.Iptu.ToString("C")}";
                        _selectedApartmentCondominium.Text = $"Cond: R$ {apartment.Condominium.ToString("C")}";
                        _selectedApartmentRoom.Text = apartment.Rooms;
                        _selectedApartmentSuite.Text = apartment.Suites;
                        _selectedApartmentGarage.Text = apartment.Vacancies;
                        _selectedApartmentArea.Text = apartment.Area;
                    });
                }
            }
        }
        public void GetBoundaries()
        {
            var bounds = _map.Projection.VisibleRegion.LatLngBounds;

            var sCoord = new GeoCoordinate(bounds.Northeast.Latitude, bounds.Northeast.Longitude);
            var eCoord = new GeoCoordinate(bounds.Southwest.Latitude, bounds.Southwest.Longitude);

            var distance = sCoord.GetDistanceTo(eCoord);

            var oldDistanceCenter = new GeoCoordinate(ViewModel.Range.CenterLat, ViewModel.Range.CenterLng);
            var center = new GeoCoordinate(bounds.Center.Latitude, bounds.Center.Longitude);

            var lat1 = (bounds.Southwest.Latitude < bounds.Northeast.Latitude) ? bounds.Southwest.Latitude : bounds.Northeast.Latitude;
            var lat2 = (bounds.Southwest.Latitude > bounds.Northeast.Latitude) ? bounds.Southwest.Latitude : bounds.Northeast.Latitude;

            var lng1 = (bounds.Northeast.Longitude < bounds.Southwest.Longitude) ? bounds.Northeast.Longitude : bounds.Southwest.Longitude;
            var lng2 = (bounds.Northeast.Longitude > bounds.Southwest.Longitude) ? bounds.Northeast.Longitude : bounds.Southwest.Longitude;


            var deltaDistance = oldDistanceCenter.GetDistanceTo(center);

            ViewModel.Range = new ApartmentRange
            {
                Lat1 = lat1,
                Lng1 = lng1,
                Lat2 = lat2,
                Lng2 = lng2,
                CenterLat = bounds.Center.Latitude,
                CenterLng = bounds.Center.Longitude,
            };
            ViewModel.LoadApartmentsCommand.Execute();
        }
    }
}
