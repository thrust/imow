﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Imow.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Views;

namespace Imow.Droid.Views
{
    [Activity(Label = "View for MainViewModel")]
    public class RegisterView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.RegisterView);
            //ActionBar.Hide();

            var radioGroup = FindViewById<RadioGroup>(Resource.Id.radioGroup1);
            var registerViewModel = (RegisterViewModel)ViewModel;

            radioGroup.CheckedChange += (sender, args) =>
            {
                var id = radioGroup.CheckedRadioButtonId;
                var radioButton = radioGroup.FindViewById(id);
                var radioId = radioGroup.IndexOfChild(radioButton);
                var button = (RadioButton)radioGroup.GetChildAt(radioId);
                var selected = button?.Text;
                registerViewModel.IsAgent = !(selected == "Customer");
            };

            ActionBar?.SetDisplayHomeAsUpEnabled(true);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            Finish();
            return false;
        }
    }
}