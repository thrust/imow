﻿using System.Linq;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Imow.Core.ViewModels;
using Imow.Droid.MvxCustoms;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Views;

namespace Imow.Droid.Views
{
    [Activity]
    public class DetailsView : MvxActivity
    {
        private bool _isMatchChange;
        public bool IsMatchChange
        {
            get
            {
                return _isMatchChange;
            }
            set
            {
                if(_isMatchChange != value)
                {
                    _isMatchChange = value;
                    InvalidateOptionsMenu();
                }
            }
        }
        public new DetailsViewModel ViewModel => base.ViewModel as DetailsViewModel;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DetailsView);
            var viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            var adapter = new ImageSliderAdapter(this, ViewModel.Images?.ToList());

            viewPager.Adapter = adapter;

            var set = this.CreateBindingSet<DetailsView, DetailsViewModel>();
            set.Bind(adapter).For(a => a.ImageUrls).To(vm => vm.Images);
            set.Bind(this).For(t => t.IsMatchChange).To(vm => vm.IsMatch);
            set.Apply();

            ActionBar?.SetDisplayHomeAsUpEnabled(true);

          //  var chatButton = FindViewById<Button>(Resource.Id.chatButton);
        //    chatButton.Click += ChatButtonClick;
        }

		public override bool OnCreateOptionsMenu(IMenu menu)
        {
            if(ViewModel.IsMatch)
            {
                MenuInflater.Inflate(Resource.Menu.DetailsViewMenu, menu);
                menu.GetItem(0).SetTitle("Send Whatsapp");
            }

            return base.OnCreateOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if(item.TitleFormatted?.ToString() == "Send Whatsapp")
            {
                ViewModel.ChatWithAgentCommand.Execute(null);
                return false;
            }
            Finish();
            return false;
		}

		//private void ChatButtonClick(object sender, System.EventArgs e)
		//{
		//    var intent = new Intent(Intent.ActionSend);
		//    intent.SetData(Uri.Parse("mmsto:+5521998108583"));
		//    intent.SetType("text/plain");

		//    StartActivity(intent);

		//}
	}

}