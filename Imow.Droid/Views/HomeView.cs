﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Imow.Core.Models.Lists;
using Imow.Core.ViewModels;
using Imow.Droid.MvxCustoms;
using Imow.Droid.Utils;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Views.Fragments;

namespace Imow.Droid.Views
{
    [Register("imow/droid/views/HomeView")]
    public class HomeView : MvxFragment
    {
        public new HomeViewModel ViewModel => base.ViewModel as HomeViewModel;

        public ExpandableListAdapter ListAdapter;
        ExpandableListView _expListView;
        ImageView _bannerImage;

        private IEnumerable<FilterSection> _filters;
        public IEnumerable<FilterSection> Filters
        {
            get
            {
                return _filters;
            }
            set
            {
                _filters = value;
                if(ListAdapter != null)
                {
                    ListAdapter.ExpandableTableModel = value;
                }
            }
        }

        private string _currentBannerImage;
        public string CurrentBannerImage
        {
            get
            {
                return _currentBannerImage;
            }
            set
            {
                if (_currentBannerImage != value)
                {
                    _currentBannerImage = value;
                    var splittedValue = value?.Split('.');
                    var extension = splittedValue?.LastOrDefault() ?? string.Empty;
                    var fileName = value.Replace($".{extension}", string.Empty);

                    var bannerDrawable = ContextCompat.GetDrawable(this.Context, Resources.GetIdentifier(fileName, "drawable", this.Context.PackageName));
                    _bannerImage.SetImageDrawable(bannerDrawable);
                }
            }
        }

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(Resource.Layout.HomeView, null);
            var filterButton = view.FindViewById<Button>(Resource.Id.filterButton);
            _expListView = view.FindViewById<ExpandableListView>(Resource.Id.lvExp);
            _bannerImage = view.FindViewById<ImageView>(Resource.Id.bannerImageView);

            ListAdapter = new ExpandableListAdapter(Activity, ViewModel?.Filters);
            _expListView.SetAdapter(ListAdapter);

            MakeListAsExpandable();

            filterButton.SetBackgroundColor(DroidContants.OrangeColor);
            filterButton.Click += FilterButton_Click;

            var set = this.CreateBindingSet<HomeView, HomeViewModel>();
            set.Bind(this).For(t => t.Filters).To(vm => vm.Filters);
            set.Bind(this).For(t => t.CurrentBannerImage).To(vm => vm.CurrentBanner);
            set.Apply();

            return view;
        }

        private void FilterButton_Click(object sender, EventArgs e)
        {
            ViewModel.FilterCommand.Execute(null);
            var tabHost = Activity?.FindViewById<TabHost>(Android.Resource.Id.TabHost);
            tabHost?.SetCurrentTabByTag(TabView.MapTabTag);
        }

        void MakeListAsExpandable()
        {
            //Listening to child item selection
            _expListView.ChildClick += delegate (object sender, ExpandableListView.ChildClickEventArgs e) 
            {
                var filterSection = ListAdapter.ExpandableTableModel?.ElementAt(e.GroupPosition);
                if(filterSection != null)
                {
                    var filterChild = filterSection[e.ChildPosition];

                    if (filterChild != null)
                    {
                        filterChild.IsSelected = !filterChild.IsSelected;
                    }
                    var background = e.ClickedView.Background;
                    e.ClickedView.SetBackgroundColor(filterChild.GetFilterColor(e.GroupPosition));

                    ViewModel.AddFilterCommand.Execute(filterSection);
                }
            };
        }
    }
}