﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Gms.Common;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.Content;
using Android.Views;
using Android.Views.InputMethods;
using Imow.Core;
using Imow.Core.ViewModels;
using Imow.Droid.MvxCustoms;
using MvvmCross.Platform;
using MvvmCross.Platform.Core;
using Thrust.Plugins.Dialogs;

namespace Imow.Droid.Views
{
    [Activity(Label = "TabView")]
    public class TabView : MvxCustomTabsFragmentActivity
    {
        public static int TabCounter = 0;

        public string CurrentTab;
        public static string HomeTabTag => $"HomeTab_{TabCounter}";
        public static string MapTabTag => $"MapTab_{TabCounter}";
        public static string LoginTabTag => $"LoginTab_{TabCounter}";
        public static string AccountTabTag => $"AccountTab_{TabCounter}";
        //public static string SettingsTabTag => $"SettingsTab_{TabCounter}";
        public static string LeadsTabTag => $"LeadsTab_{TabCounter}";

         private new TabViewModel ViewModel => base.ViewModel as TabViewModel;

        public TabView() : base(Resource.Layout.TabView, Resource.Id.actualtabcontent)
        {
            CurrentTab = HomeTabTag;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);

                if(ActionBar != null)
                {
                    ActionBar.Title = HomeTabTag.Remove(HomeTabTag.IndexOf("Tab", StringComparison.InvariantCultureIgnoreCase));
                    ActionBar.SetBackgroundDrawable(new Android.Graphics.Drawables.ColorDrawable(Color.Black));
                }

                _tabHost.SetCurrentTabByTag(HomeTabTag);
                ViewModel.SubscriptionTokenAction = loginMesssage =>
                {
                    Mvx.Resolve<IMvxMainThreadDispatcher>().RequestMainThreadAction(() =>
                    {
                        try
                        {
                            _tabHost.ClearAllTabs();
                            TabCounter++;
                            ViewModel.InitTabs();
                            InitializeTabs();
                        }
                        catch(Exception e)
                        {
                            
                        }
                    });
                };
            }
            catch(Exception e)
            {
                var a = e;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            if (CurrentTab == MapTabTag)
            {
                MenuInflater.Inflate(Resource.Menu.MapViewMenu, menu);
                CreateMenuForMapView(ref menu);
            }

            return base.OnCreateOptionsMenu(menu);
        }

        protected override void AddTabs(Bundle args)
        {
            InitializeTabs(args);
        }

        public void InitializeTabs(Bundle args = null)
        {
            try
            {
                //var mapTab = Resources.GetDrawable(Resource.Drawable.map);
                var isAgent = ViewModel.CurrentUser?.IsAgent ?? false;

                if (isAgent)
                {
                    var leadsSpec = _tabHost.NewTabSpec(LeadsTabTag);
                    var leadsTab = ContextCompat.GetDrawable(this, Resource.Drawable.list);
                    leadsSpec.SetIndicator("Leads", leadsTab);

                    //var settingsSpec = _tabHost.NewTabSpec(SettingsTabTag);
                    //var settingsTab = ContextCompat.GetDrawable(this, Resource.Drawable.gear);
                    //settingsSpec.SetIndicator("Settings", settingsTab);

                    AddTab<LeadsView>(args, ViewModel.LeadsViewModel, leadsSpec);
                    //AddTab<SettingsView>(args, ViewModel.SettingsViewModel, settingsSpec);
                    _tabHost.SetCurrentTabByTag(LeadsTabTag);
                }
                else
                {
                    var homeSpec = _tabHost.NewTabSpec(HomeTabTag);
                    var homeTab = ContextCompat.GetDrawable(this, Resource.Drawable.home);
                    homeSpec.SetIndicator("Home", homeTab);

                    var mapSpec = _tabHost.NewTabSpec(MapTabTag);
                    var mapTab = ContextCompat.GetDrawable(this, Resource.Drawable.map);
                    mapSpec.SetIndicator("Map", mapTab);

                    //var settingsSpec = _tabHost.NewTabSpec(SettingsTabTag);
                    //var settingsTab = ContextCompat.GetDrawable(this, Resource.Drawable.gear);
                    //settingsSpec.SetIndicator("Settings", settingsTab);

                    AddTab<HomeView>(args, ViewModel.HomeViewModel, homeSpec);
                    AddTab<MapView>(args, ViewModel.MapViewModel, mapSpec);

                    //AddTab<SettingsView>(args, vm.SettingsViewModel, settingsSpec);

                    _tabHost.SetCurrentTabByTag(HomeTabTag);
                }

                if (ViewModel.CurrentUser != null)
                {
                    var accountSpec = _tabHost.NewTabSpec(AccountTabTag);
                    var accountTab = ContextCompat.GetDrawable(this, Resource.Drawable.user);
                    accountSpec.SetIndicator("Account", accountTab);

                    AddTab<AccountView>(args, ViewModel.AccountViewModel, accountSpec);
                }
                else
                {
                    var loginSpec = _tabHost.NewTabSpec(LoginTabTag);
                    var loginTab = ContextCompat.GetDrawable(this, Resource.Drawable.user);
                    loginSpec.SetIndicator("Login", loginTab);

                    AddTab<LoginView>(args, ViewModel.LoginViewModel, loginSpec);
                }
            }
            catch (Exception e)
            {
                var a = e;
            }
        }

        public void SetCurrentTabByTag(string tabTag)
        {
            _tabHost.SetCurrentTabByTag(tabTag);
        }

        public override void OnTabChanged(string tag)
        {
            CurrentTab = tag;

            if (ActionBar != null)
            {
                ActionBar.Title = tag.Remove(tag.IndexOf("Tab", StringComparison.InvariantCultureIgnoreCase));
            }

            if (CurrentFocus?.WindowToken != null)
            {
                var inputMethod = (InputMethodManager)GetSystemService(InputMethodService);
                inputMethod.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);
            }

            if(tag == MapTabTag)
            {
                var googleApiAvailability = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
                if(googleApiAvailability != ConnectionResult.Success)
                {
                    Task.Factory.StartNew(async () =>
                    {
                        await Mvx.Resolve<IDialogService>().Neutral(new AlertNeutralConfig
                        {
                            Button = SharedStrings.Ok,
                            Title = SharedStrings.Warning,
                            Message = "Your device does not support Google Map. please install Google Play Services App to support it",
                        }).ConfigureAwait(false);
                    });
                    return;
                }
            }

            InvalidateOptionsMenu();
            base.OnTabChanged(tag);
        }

        public TabView(int layoutId, int tabContentId) : base(layoutId, tabContentId)
        {
        }

        protected override void OnDestroy()
        {
            //TabViewModel.OnDestroy();
            base.OnDestroy();
        }

        #region MapViewModel

        private void CreateMenuForMapView(ref IMenu menu)
        {
            var viewModel = ViewModel?.MapViewModel;
        }

        #endregion
        
    }
}