﻿using Android.App;
using Android.Content.PM;
using Imow.Core.Utils;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using MvvmCross.Droid.Views;

namespace Imow.Droid
{
    [Activity(
        Label = "Imow"
        , MainLauncher = true
        , Icon = "@mipmap/icon"
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
            SharedPush.Initialize();

            AppCenter.Start("d9b66dc4-efb7-4bd0-984d-d4507fa880e5", typeof(Analytics), typeof(Crashes));
        }
    }
}
