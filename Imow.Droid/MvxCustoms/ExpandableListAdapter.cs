﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using Imow.Core.Models.Lists;
using Imow.Droid.Utils;
using Java.Lang;

namespace Imow.Droid.MvxCustoms
{
    public class ExpandableListAdapter : BaseExpandableListAdapter
    {
        private Activity _context;

        private IEnumerable<FilterSection> _expandableTableModel;
        public IEnumerable<FilterSection> ExpandableTableModel
        {
            get
            {
                return _expandableTableModel;
            }
            set
            {
                try
                {
                    _expandableTableModel = value;
                    NotifyDataSetChanged();
                }
                catch(Exception e)
                {
                    var a = e;
                }
            }
        }
        
        public ExpandableListAdapter(Activity context, IEnumerable<FilterSection> expandableTableModel)
        {
            _context = context;
            ExpandableTableModel = expandableTableModel;
        }

        #region ChildView

        public override Object GetChild(int groupPosition, int childPosition)
        {
            return GetFilterChild(groupPosition, childPosition)?.Title;
        }

        public FilterChild GetFilterChild(int groupPosition, int childPosition)
        {
            return ExpandableTableModel?.ElementAt(groupPosition)?[childPosition];
        }

        public override long GetChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
        {
            var filterChild = GetFilterChild(groupPosition, childPosition);
            if (convertView == null)
            {
                convertView = _context.LayoutInflater.Inflate(Resource.Layout.ListItemCustomLayout, null);
            }

            switch(groupPosition)
            {
                case 0:
                {
                    convertView.SetBackgroundColor((filterChild?.IsSelected ?? false)? Color.DarkGreen : DroidContants.GreenColor);
                    break;
                }
                case 1:
                {
                    convertView.SetBackgroundColor((filterChild?.IsSelected ?? false) ? Color.DarkRed : DroidContants.RedColor);
                    break;
                }
                case 2:
                {
                    convertView.SetBackgroundColor((filterChild?.IsSelected ?? false) ? Color.DarkBlue : DroidContants.BlueColor);
                    break;
                }
            }

            var txtListChild = convertView.FindViewById<TextView>(Resource.Id.lblListItem);
            txtListChild.Text = filterChild?.Title ;
            txtListChild.SetTextColor(Color.White);

            return convertView;
        }
        public override int GetChildrenCount(int groupPosition)
        {
            return ExpandableTableModel?.ElementAt(groupPosition)?.Count ?? 0;
        }

        #endregion

        #region HeaderView

        public override Object GetGroup(int groupPosition)
        {
            return ExpandableTableModel?.ElementAt(groupPosition)?.Title;
        }
        public override int GroupCount
        {
            get
            {
                return ExpandableTableModel?.Count() ?? 0;
            }
        }
        public override long GetGroupId(int groupPosition)
        {
            return groupPosition;
        }
        public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
        {
            var headerTitle = (string)GetGroup(groupPosition);

            convertView = convertView ?? _context.LayoutInflater.Inflate(Resource.Layout.HeaderCustomLayout, null);

            var lblListHeader = convertView.FindViewById<TextView>(Resource.Id.lblListHeader);

            switch (groupPosition)
            {
                case 0:
                {
                    lblListHeader.SetBackgroundResource(Resource.Drawable.home_green_button);
                    break;
                }
                case 1:
                {
                    lblListHeader.SetBackgroundResource(Resource.Drawable.home_red_button);
                    break;
                }
                case 2:
                {
                    lblListHeader.SetBackgroundResource(Resource.Drawable.home_blue_button);
                    break;
                }
            }
            lblListHeader.Text = headerTitle;

            return convertView;
        }
        public override bool HasStableIds
        {
            get
            {
                return false;
            }
        }
        public override bool IsChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }

        #endregion
    }
}