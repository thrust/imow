﻿using System.Collections.Generic;
using System.Reflection;
using Imow.Core.Models.PresentationHints;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using AndroidContent = Android.Content;

namespace Imow.Droid.Custom.MvxDroidPresentation
{
    public class MvxDroidPresenter : MvxAndroidViewPresenter
	{
        public MvxDroidPresenter (IEnumerable<Assembly> viewAssemblies) : base(viewAssemblies)
        {
            
        }

        public override void Show(MvxViewModelRequest request)
        {
            base.Show(request);
        }

        public override void ChangePresentation(MvxPresentationHint hint)
		{
			if (hint is MvxRootPresentationHint)
			{
				var rootHint = hint as MvxRootPresentationHint;
				var requestTranslator = Mvx.Resolve<IMvxAndroidViewModelRequestTranslator>();
                var newRootIntent = requestTranslator.GetIntentFor(new MvxViewModelRequest(rootHint.PresentationType, null, null));

				newRootIntent.AddFlags(AndroidContent.ActivityFlags.NewTask);
				newRootIntent.AddFlags(AndroidContent.ActivityFlags.ClearTask);
                CurrentActivity.StartActivity(newRootIntent);
			}
			else
			{
				base.ChangePresentation(hint);
			}
		}
	}
}