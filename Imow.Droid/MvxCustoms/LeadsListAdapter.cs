﻿using Android.App;
using Android.Views;
using Android.Widget;
using Imow.Core;
using Imow.Core.ViewModels;
using Imow.Droid.Utils;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;

namespace Imow.Droid.MvxCustoms
{
    public class LeadsListAdapter : MvxAdapter
    {
        private LeadsViewModel _viewModel;
        
        public LeadsListAdapter(LeadsViewModel viewModel, Activity context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext)
        {
            _viewModel = viewModel;
        }

        public void NotifyChange()
        {
            NotifyDataSetChanged();
        }

		protected override View GetBindableView(View convertView, object dataContext, ViewGroup parent, int templateId)
        {
            var bindedView = base.GetBindableView(convertView, dataContext, parent, templateId);

            //if(dataContext is Lead lead)
            //{
                //var buttonsView = bindedView?.FindViewById<LinearLayout>(Resource.Id.buttonsView);
                //if(lead.Status != LeadStatus.Pending && buttonsView != null)
                //{
                //    buttonsView.Visibility = ViewStates.Gone;
                //}
                //else
                //{
            var acceptButton = bindedView?.FindViewById<Button>(Resource.Id.acceptButton);
            var rejectButton = bindedView?.FindViewById<Button>(Resource.Id.rejectButton);

            acceptButton?.SetBackgroundColor(DroidContants.GreenColor);
            rejectButton?.SetBackgroundColor(DroidContants.RedColor);

                //acceptButton.Click += (sender, args) => _viewModel.MatchLeadCommand.Execute(dataContext);
                //rejectButton.Click += (sender, args) => _viewModel.RejectLeadCommand.Execute(dataContext);
                //}

                //var customerName = bindedView?.FindViewById<TextView>(Resource.Id.customerName);
                //if(customerName != null)
                //{
                //    customerName.Text = lead.CustomerName;
                //}

                //var apartmentName = bindedView?.FindViewById<TextView>(Resource.Id.apartmentName);
                //if (apartmentName != null)
                //{
                //    apartmentName.Text = lead.ApartmentId;
                //}
            //}

            return bindedView;
        }
    }
}