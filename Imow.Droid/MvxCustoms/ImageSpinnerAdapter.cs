﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;

namespace Imow.Droid.MvxCustoms
{
    public class ImageSliderAdapter : PagerAdapter
    {
        Context _context;

        private IEnumerable<string> _imageUrls;
        public IEnumerable<string> ImageUrls
        {
            get
            {
                return _imageUrls;
            }
            set
            {
                try
                {
                    _imageUrls = value;
                    NotifyDataSetChanged();
                }
                catch (Exception e)
                { }
            }
        }

        public ImageSliderAdapter(Context context, IEnumerable<string> imageUrls)
        {
            ImageUrls = imageUrls;
            _context = context;
        }

        public override bool IsViewFromObject(View view, Java.Lang.Object @object)
        {
            return view == @object;
        }

        public override int Count
        {
            get
            {
                return Math.Max(_imageUrls?.Count() ?? 0, 1);
            }
        }

		public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object)
        {
            container.RemoveView(@object as View);          
        }

		public override int GetItemPosition(Java.Lang.Object @object)
		{
            return PositionNone;
		}

		public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
		{
            return new Java.Lang.String($"{position + 1} of {this.Count}".ToArray());
		}

		public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
        {
            ImageView imageView;
            if(_imageUrls?.Any() ?? false)
            {
                imageView = new MvxImageView(_context);

                (imageView as MvxImageView).ImageUrl = _imageUrls?.ElementAt(position);
                (imageView as MvxImageView).DrawingCacheEnabled = false;
                (imageView as MvxImageView).DefaultImagePath = "res:loading1";
            }
            else
            {
                imageView = new ImageView(_context);
                imageView.SetImageResource(Resource.Drawable.no_image);
            }

            container.AddView(imageView);

            return imageView;
        }
        
    }
}