-- Table: imow.apartment

-- DROP TABLE imow.apartment;

CREATE TABLE imow.apartment
(
    id uuid NOT NULL,
    name character varying(60) COLLATE pg_catalog."default" NOT NULL,
    description character varying(60) COLLATE pg_catalog."default" NOT NULL,
    latitude numeric NOT NULL,
    longitude numeric NOT NULL,
    neighborhood character varying(40) COLLATE pg_catalog."default" NOT NULL,
    agent_status character varying(10) COLLATE pg_catalog."default" NOT NULL,
    property_type character varying(15) COLLATE pg_catalog."default" NOT NULL,
    habitation_option character varying(10) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT apartment_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.apartment
    OWNER to postgres;


    -- Table: imow.apartment_interest

-- DROP TABLE imow.apartment_interest;

CREATE TABLE imow.apartment_interest
(
    client_id uuid NOT NULL,
    apartment_id uuid NOT NULL,
    time_clicked timestamp without time zone NOT NULL,
    CONSTRAINT apartment_interest_pk PRIMARY KEY (client_id, time_clicked, apartment_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.apartment_interest
    OWNER to postgres;



    -- Table: imow.location

-- DROP TABLE imow.location;

CREATE TABLE imow.location
(
    neighborhood text COLLATE pg_catalog."default" NOT NULL,
    city text COLLATE pg_catalog."default" NOT NULL,
    state character varying(2) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT location_pk PRIMARY KEY (neighborhood, state, city)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.location
    OWNER to postgres;




    -- Table: imow.property_type

-- DROP TABLE imow.property_type;

CREATE TABLE imow.property_type
(
    type_name text COLLATE pg_catalog."default" NOT NULL,
    id uuid NOT NULL,
    CONSTRAINT property_type_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.property_type
    OWNER to postgres;



    -- Table: imow.rating

-- DROP TABLE imow.rating;

CREATE TABLE imow.rating
(
    id uuid NOT NULL,
    agent_id uuid NOT NULL,
    client_id uuid NOT NULL,
    rating integer NOT NULL,
    observation text COLLATE pg_catalog."default",
    CONSTRAINT rating_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.rating
    OWNER to postgres;




    -- Table: imow."user"

-- DROP TABLE imow."user";

CREATE TABLE imow."user"
(
    id uuid NOT NULL,
    phone character varying(20) COLLATE pg_catalog."default" NOT NULL,
    password character varying(30) COLLATE pg_catalog."default" NOT NULL,
    lastname character varying(30) COLLATE pg_catalog."default" NOT NULL,
    firstname character varying(30) COLLATE pg_catalog."default" NOT NULL,
    email character varying(30) COLLATE pg_catalog."default" NOT NULL,
    lat numeric,
    lng numeric,
    is_agent boolean,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT email_con UNIQUE (email)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow."user"
    OWNER to postgres;




    -- Table: imow.user_connection

-- DROP TABLE imow.user_connection;

CREATE TABLE imow.user_connection
(
    user_id uuid NOT NULL,
    connection_id text COLLATE pg_catalog."default" NOT NULL,
    type smallint NOT NULL,
    device_id text COLLATE pg_catalog."default" NOT NULL,
    version text COLLATE pg_catalog."default" NOT NULL,
    last_updated timestamp without time zone NOT NULL,
    free boolean NOT NULL,
    CONSTRAINT user_connection_pk PRIMARY KEY (user_id, type, connection_id),
    CONSTRAINT user_connection_user_fk FOREIGN KEY (user_id)
        REFERENCES imow."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE imow.user_connection
    OWNER to postgres;