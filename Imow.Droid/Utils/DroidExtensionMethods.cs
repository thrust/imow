﻿using System;
using Android.Graphics;
using MvvmCross.Platform.UI;
using Imow.Core.Utils;
using Imow.Core.Models.Lists;

namespace Imow.Droid.Utils
{
    public static class DroidExtensionMethods
	{
		public static Color ToColor(this MvxColor color)
		{
			return new Color(color.R, color.G, color.B, color.A);
		}

		public static MvxColor ToMvxColor(this Color color)
		{
            return new MvxColor(Convert.ToInt32(color.R * 255.0f), Convert.ToInt32(color.G * 255.0f), Convert.ToInt32(color.B * 255.0f), Convert.ToInt32(color.A * 255.0f));
		}

		public static Color GetContrastColor(this Color color)
		{
			return color.ToMvxColor().GetContrastColor().ToColor();
        }

        public static Color GetFilterColor(this FilterChild filterChild, int index)
        {
            return filterChild.GetFilterMvxColor(index)?.ToColor() ?? Color.White;
        }

        public static string GetLocalFilePath(string filename)
        {
            // Use the SpecialFolder enum to get the Personal folder on the Android file system.
            // Storing the database here is a best practice.
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return System.IO.Path.Combine(path, filename);
        }
    }
}
