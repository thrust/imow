﻿using Android.Graphics;
using Imow.Core.Utils;

namespace Imow.Droid.Utils
{
    public static class DroidContants
	{
        public static Color GreenColor = Constants.GreenColor.ToColor();
		public static Color BlueColor = Constants.BlueColor.ToColor();
		public static Color RedColor = Constants.RedColor.ToColor();
        public static Color OrangeColor = Constants.OrangeColor.ToColor();
        public static Color DarkGreenColor = Constants.DarkGreenColor.ToColor();
        public static Color DarkBlueColor = Constants.DarkBlueColor.ToColor();
        public static Color DarkRedColor = Constants.DarkRedColor.ToColor();
        public static Color DarkOrangeColor = Constants.DarkOrangeColor.ToColor();
    }
}
