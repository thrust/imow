﻿using Android.Content;
using Imow.Core.Services.DatabaseServices;
using Imow.Droid.Custom.MvxDroidPresentation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using static Imow.Droid.Utils.DroidExtensionMethods;

namespace Imow.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            var dbConn = GetLocalFilePath("imow.db3");
            Mvx.LazyConstructAndRegisterSingleton<IDatabaseService>(() =>
            {
                return new DatabaseService(dbConn);
            });
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            var droidPresenter = new MvxDroidPresenter(AndroidViewAssemblies);
            Mvx.RegisterSingleton<IMvxAndroidViewPresenter>(droidPresenter);
            return droidPresenter;
        }
    }
}
